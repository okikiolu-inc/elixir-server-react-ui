defmodule HeliosServerWeb.V1.FileView do
  use HeliosServerWeb, :view

  def render("avatar.json", %{avatar: avatar}) do
    %{avatar: avatar}
  end

  def render("server_banner.json", %{server_banner: server_banner}) do
    %{server_banner: server_banner}
  end

  def render("server_icon.json", %{server_icon: server_icon}) do
    %{server_icon: server_icon}
  end

  def render("error.json", %{error: msg}) do
    %{error: msg}
  end
end
