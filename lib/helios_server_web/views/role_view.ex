defmodule HeliosServerWeb.V1.RoleView do
  use HeliosServerWeb, :view
  alias HeliosServerWeb.V1.RoleView
  # alias HeliosServer.Core.GlobalCache

  def render("index.json", %{roles: _ = []}) do
    %{roles: []}
  end

  def render("index.json", %{roles: roles}) do
    %{roles: render_many(roles, RoleView, "role.json")}
  end

  def render("show.json", %{role: role}) do
    %{role: render_one(role, RoleView, "role.json")}
  end

  def render("role.json", %{role: role}) do
    %{
      server_id: "#{role.server_id}",
      role_id: "#{role.role_id}",
      name: role.name,
      color: role.color,
      permissions: get_perms_list(role.permissions),
    }
  end

  defp get_perms_list(perm_list) when is_list(perm_list) do perm_list end
  defp get_perms_list(perm_list) do MapSet.to_list(perm_list) end
end
