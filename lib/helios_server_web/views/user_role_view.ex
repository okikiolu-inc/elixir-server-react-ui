defmodule HeliosServerWeb.V1.UserRoleView do
  use HeliosServerWeb, :view
  alias HeliosServerWeb.V1.UserRoleView
  # alias HeliosServer.Core.GlobalCache

  def render("index.json", %{user_roles: _ = []}) do
    %{user_role: []}
  end

  def render("index.json", %{user_roles: user_roles}) do
    %{user_roles: render_many(user_roles, UserRoleView, "user_role.json")}
  end

  def render("show.json", %{user_role: user_role}) do
    %{user_roles: render_one(user_role, UserRoleView, "user_role.json")}
  end

  def render("user_role.json", %{user_role: user_role}) do
    %{
      role_id: user_role.role_id,
      name: user_role.name,
      color: user_role.color,
      permissions: MapSet.to_list(user_role.permissions),
    }
  end

  def render("user_permissions.json", %{user_permissions: _ = []}) do
    %{user_permissions: []}
  end

  def render("user_permissions.json", %{user_permissions: user_permissions}) do
    %{user_permissions: user_permissions}
  end
end
