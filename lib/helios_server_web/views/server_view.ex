defmodule HeliosServerWeb.V1.ServerView do
  use HeliosServerWeb, :view
  alias HeliosServerWeb.V1.ServerView
  alias HeliosServer.Core.GlobalCache
  alias HeliosServer.Uploader.ServerBanner
  alias HeliosServer.Uploader.ServerIcon

  def render("index.json", %{servers: _servers = [], owned_servers: _owned_servers = []}) do
    %{servers: [], owned_servers: []}
  end

  def render("index.json", %{servers: _servers = [], owned_servers: owned_servers}) do
    %{servers: [], owned_servers: render_many(owned_servers, ServerView, "server.json")}
  end

  def render("index.json", %{servers: servers, owned_servers: _owned_servers = []}) do
    %{servers: render_many(servers, ServerView, "server.json"), owned_servers: []}
  end

  def render("index.json", %{servers: servers, owned_servers: owned_servers}) do
    %{
      servers: render_many(servers, ServerView, "server.json"),
      owned_servers: render_many(owned_servers, ServerView, "server.json")
    }
  end

  def render("show.json", %{server: server}) do
    %{server: render_one(server, ServerView, "server.json")}
  end
  
  def render("server.json", %{server: server}) do
    %{server_id: "#{server.server_id}",
      name: server.name,
      icon: server_icon_url(server.icon, server.server_id),
      banner: server_banner_url(server.banner, server.server_id),
      description: server.description}
  end

  def render("member_list.json", %{server_members: server_members}) do
    %{server_members: render_many(server_members, ServerView, "member.json")}
  end

  def render("member.json", %{server: member}) do
      user = GlobalCache.get_user(member.user_id)
      %{
          username: user.username,
          avatar: user.avatar,
          userhash: user.userhash,
      }
  end

  defp server_icon_url(icon, _) when is_nil(icon) or icon == "" do ServerIcon.url(nil) end
  defp server_icon_url(icon, server_id) do ServerIcon.url({icon, %{server_id: server_id}}) end

  defp server_banner_url(banner, _) when is_nil(banner) or banner == "" do ServerBanner.url(nil) end
  defp server_banner_url(banner, server_id) do ServerBanner.url({banner, %{server_id: server_id}}) end
end
