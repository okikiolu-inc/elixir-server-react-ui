defmodule HeliosServerWeb.V1.ChannelView do
  use HeliosServerWeb, :view
  alias HeliosServerWeb.V1.ChannelView

  def render("index.json", %{channels: _channels = []}) do
    %{channels: []}
  end

  def render("index.json", %{channels: channels}) do
    %{channels: render_many(channels, ChannelView, "channel.json")}
  end

  def render("show.json", %{channel: channel}) do
    %{channel: render_one(channel, ChannelView, "channel.json")}
  end

  def render("channel.json", %{channel: channel}) do
    %{channel_id: "#{channel.channel_id}",
      server_id: "#{channel.server_id}",
      name: channel.name,
      description: channel.description}
  end
end
