defmodule HeliosServerWeb.V1.UserView do
  use HeliosServerWeb, :view

  alias HeliosServer.Uploader.Avatar
  alias HeliosServer.Core.GlobalCache

  def render("show.json", %{user: user}) do
    user_json = %{username: user.username,
      userhash: user.userhash,
      status: user.status,
      email: user.email}
    if user.avatar == nil || user.avatar == "" do
      Map.put(user_json, :avatar, Avatar.url(nil))
    else
      Map.put(user_json, :avatar, Avatar.url({user.avatar, user}))
    end
  end

  def render("user.json", %{user: user, token: token}) do
    %{id: "#{user.id}",
      username: user.username,
      userhash: user.userhash,
      email: user.email,
      status: user.status,
      avatar: user_avatar_url(user.avatar, user.id),
      token: token}
  end

  def render("shallow.json", %{id: id}) do
    user = GlobalCache.get_user(id)
    %{
      status: user.status,
    }
  end

  def render("error.json", %{error: msg}) do
    %{error: msg}
  end

  defp user_avatar_url(avatar, _) when is_nil(avatar) or avatar == "" do Avatar.url(nil) end
  defp user_avatar_url(avatar, user_id) do Avatar.url({avatar, %{id: user_id}}) end
end
