defmodule HeliosServerWeb.V1.MessageView do
  use HeliosServerWeb, :view
  alias HeliosServerWeb.V1.MessageView
  alias HeliosServer.Core.GlobalCache

  def render("index.json", %{messages: _messages = []}) do
    %{messages: []}
  end

  def render("index.json", %{messages: messages}) do
    %{messages: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{message: render_one(message, MessageView, "message.json")}
  end

  def render("message.json", %{message: message}) do
    author = GlobalCache.get_user(message.author_id)
    %{
      channel_id: "#{message.channel_id}",
      message_id: "#{message.message_id}",
      avatar: author.avatar,
      username: author.username,
      userhash: author.userhash,
      content: message.content,
      updated: message.updated,
    }
  end
end
