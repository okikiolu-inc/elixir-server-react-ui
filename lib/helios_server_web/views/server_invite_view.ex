defmodule HeliosServerWeb.V1.ServerInviteView do
  use HeliosServerWeb, :view
  alias HeliosServerWeb.V1.ServerInviteView

  def render("index.json", %{server_invites: _invites = []}) do
    %{server_invites: []}
  end

  def render("index.json", %{server_invites: server_invites}) do
    %{server_invites: render_many(server_invites, ServerInviteView, "invite.json")}
  end

  def render("show.json", %{server_invite: server_invite}) do
    %{server_invite: render_one(server_invite, ServerInviteView, "invite.json")}
  end
  
  def render("invite.json", %{server_invite: server_invite}) do
    %{
      server_id: "#{server_invite.server_id}",
      invite_id: "#{server_invite.invite_id}",
      join_count: server_invite.join_count,
      duration: server_invite.duration,
      created: server_invite.created,
    }
  end
end
