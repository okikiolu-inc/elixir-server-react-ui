defmodule HeliosServerWeb.V1.AuthView do
  use HeliosServerWeb, :view

  alias HeliosServer.Uploader.Avatar

  def render("show.json", %{user: user}) do
    user_json = %{id: "#{user.id}",
      username: user.username,
      userhash: user.userhash,
      email: user.email}
    if user.avatar == nil || user.avatar == "" do
      Map.put(user_json, :avatar, Avatar.url(nil))
    else
      Map.put(user_json, :avatar, Avatar.url({user.avatar, user}))
    end
  end

  def render("user.json", %{user: user, token: token}) do
    user_json = %{id: "#{user.id}",
      username: user.username,
      userhash: user.userhash,
      email: user.email,
      token: token}
    if user.avatar == nil || user.avatar == "" do
      Map.put(user_json, :avatar, Avatar.url(nil))
    else
      Map.put(user_json, :avatar, Avatar.url({user.avatar, user}))
    end
  end
end
