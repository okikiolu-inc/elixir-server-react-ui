defmodule HeliosServer.Uploader.ServerIcon do
  use Arc.Definition

  # Include ecto support (requires package arc_ecto installed):
  # use Arc.Ecto.Definition

  # Url prefixes
  # server icon - https://#{cdn_url}/server_icons/#{server.server_id}/#{random_hash}.#{original_file_extention}


  @acl :public_read

  @versions [:original]

  # To add a thumbnail version:
  # @versions [:original, :thumb]

  # Override the bucket on a per definition basis:
  # def bucket do
  #   :custom_bucket_name
  # end

  @doc """
  Whitelist file extensions

  ## Examples

      iex> validate({file, %Server{}})
      :ok

      iex> validate(user, "abx")
      {:error, msg}

  """
  def validate({file, _scope}) do
    ~w(.jpg .jpeg .png) |> Enum.member?(String.downcase(Path.extname(file.file_name)))
    # error
    # {:error, :invalid_file}
  end

  # Define a thumbnail transformation:
  # def transform(:thumb, _) do
  #   {:convert, "-strip -thumbnail 250x250^ -gravity center -extent 250x250 -format png", :png}
  # end

  @doc """
  Override the persisted filenames

  ## Examples

      iex> filename(version, {file, %Server{}})
      :ok

      iex> filename(version, {file, nil})
      {:error, msg}

  """
  def filename(_version, {file, _scope}) do
    Path.basename("icon", Path.extname(file.file_name))
  end

  @doc """
  Override the storage directory

  ## Examples

      iex> storage_dir(version, {file, %Server{}})
      :ok

      iex> storage_dir(version, {file, nil})
      {:error, msg}
  """
  def storage_dir(_version, {_file, scope}) do
    if Mix.env() == :prod do
      "server_icons/#{scope.server_id}"
    else
      "../uploads/server_icons/#{scope.server_id}"
    end
  end

  @doc """
  Provide a default URL if there hasn't been a file uploaded

  ## Examples

      iex> default_url(version, %Server{})
      :ok

      iex> default_url(version, nil)
      {:error, msg}

  """
  def default_url(_version, _scope) do
    HeliosServerWeb.Endpoint.url <> "/images/server_icons/default.png"
  end

  # Specify custom headers for s3 objects
  # Available options are [:cache_control, :content_disposition,
  #    :content_encoding, :content_length, :content_type,
  #    :expect, :expires, :storage_class, :website_redirect_location]
  #
  # def s3_object_headers(version, {file, scope}) do
  #   [content_type: MIME.from_path(file.file_name)]
  # end
end
