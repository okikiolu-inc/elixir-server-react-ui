defmodule HeliosServerWeb.Auth.Guardian do
  use Guardian, otp_app: :helios_server

  alias HeliosServer.Schema

  def subject_for_token(user, _claims) do
    sub = to_string(user.id)
    {:ok, sub}
  end

  def resource_from_claims(claims) do
    id = claims["sub"]
    resource = Schema.get_user(id)
    {:ok,  resource}
  end

  def authenticate(email, password) do
    with {:ok, user} <- Schema.get_by_email(email) do
      case validate_password(password, user.password) do
        true ->
          create_token(user)
        false ->
          {:error, :unauthorized}
      end
    else
      _ ->
        {:error, :unauthorized}
    end
  end

  def logout(conn) do
    userid_list = Plug.Conn.get_req_header(conn, "userid")
    case userid_list do
      [user_id | _tail = []] ->
        with {:ok, _} <- Redix.command(:redix, ["DEL", "token.#{user_id}"]) do
          {:ok, ""}
        end
      _ ->
        {:ok, ""}
    end
  end

  def get_token(conn) do
    g_token = Guardian.Plug.current_token(conn)
    if g_token != nil do
      {:ok, g_token}
    else
      {:error, nil}
    end
  end

  defp validate_password(password, encrypted_password) do
    Comeonin.Bcrypt.checkpw(password, encrypted_password)
  end

  defp create_token(user) do
    {:ok, token, _claims} = encode_and_sign(user)
    # store token in redis
    case Redix.command(:redix, ["SET", "token.#{user.id}", token]) do
      {:ok, _} ->
        {:ok, user, "#{token}"}
      {:error, msg} ->
        {:error, msg}
    end
  end
end