defmodule HeliosServerWeb.Auth.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :helios_server,
    module: HeliosServerWeb.Auth.Guardian,
    error_handler: HeliosServerWeb.Auth.ErrorHandler

  plug Guardian.Plug.VerifyHeader
  plug Guardian.Plug.EnsureAuthenticated
  # plug Guardian.Plug.LoadResource
end
