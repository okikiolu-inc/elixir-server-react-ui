defmodule HeliosServerWeb.ServerChannel do
  use HeliosServerWeb, :channel

  alias HeliosServer.Worker.User
  alias HeliosServer.Worker.Server


  # We can use permissions to do stuff here
  # e.g admins only or other stuff
  @impl true
  def join("server:admin:" <> ids, payload, _socket) do
    [raw_server_id, raw_channel_id | _tail = []] = String.split(ids, ":")
    server_id = String.to_integer(raw_server_id)
    channel_id = String.to_integer(raw_channel_id)
    if authorized?(server_id, channel_id, payload) do
      {:error, %{reason: "unauthorized"}}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  @impl true
  def join("server:" <> ids, %{"user_id" => raw_user_id}=payload, socket) do
    [raw_server_id, raw_channel_id | _tail = []] = String.split(ids, ":")
    server_id = String.to_integer(raw_server_id)
    channel_id = String.to_integer(raw_channel_id)
    if authorized?(server_id, channel_id, payload) do
      user_id = String.to_integer(raw_user_id)
      send(self(), {:after_join, user_id, server_id})
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  @impl true
  def handle_in("message:" <> ids, payload, socket) do
    [raw_server_id, raw_channel_id | _tail = []] = String.split(ids, ":")
    server_id = String.to_integer(raw_server_id)
    channel_id = String.to_integer(raw_channel_id)
    with {:ok, server} <- GenRegistry.lookup_or_start(Server, server_id, [server_id]) do
      payload_up = Map.put(payload, :now_time, DateTime.utc_now)
      Server.broadcast_message(server, channel_id, payload_up)
      Server.save_message(server, channel_id, payload_up)
      {:reply, :ok, socket}
    else
      _ ->
        {:reply, {:error, %{reason: "server error"}}, socket}
    end
  end

  @impl true
  def terminate(_reason, _socket) do
    # will need to do process cleanup on exit, on disconnect etc
    nil
  end

  @impl true
  def handle_info({:after_join, user_id, server_id}, socket) do
      with {:ok, user_pid} <- GenRegistry.lookup_or_start(User, user_id, [user_id]),
        {:ok, server} <- GenRegistry.lookup_or_start(Server, server_id, [server_id]) do
        User.set_socket(user_pid, socket)
        Server.connect_user(server, user_id, user_pid)
        {:noreply, socket} # :noreply
      else
        _ ->
          {:error, %{reason: "server error"}}
      end
  end

  # Add authorization logic here as required.
  defp authorized?(_server_id, _channel_id, _payload) do
    true
  end
end
