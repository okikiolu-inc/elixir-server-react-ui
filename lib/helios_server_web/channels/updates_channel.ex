defmodule HeliosServerWeb.UpdatesChannel do
  use HeliosServerWeb, :channel

  # alias HeliosServer.Worker.User
  alias HeliosServer.Worker.Server


  @impl true
  def join("updates", %{"user_id" => _raw_user_id}=payload, socket) do
    if authorized?(payload) do
      # user_id = String.to_integer(raw_user_id)
      send(self(), {})
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  @impl true
  def handle_in("update:" <> raw_server_id, payload, socket) do
    server_id = String.to_integer(raw_server_id)
    with {:ok, server} <- GenRegistry.lookup_or_start(Server, server_id, [server_id]) do
      Server.broadcast_update(server, payload)
      {:reply, :ok, socket}
    else
      _ ->
        {:reply, {:error, %{reason: "server error"}}, socket}
    end
  end

  @impl true
  def terminate(_reason, _socket) do
    # will need to do process cleanup on exit, on disconnect etc
    nil
  end

  @impl true
  def handle_info(_, socket) do
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
