defmodule HeliosServerWeb.Router do
  use HeliosServerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_auth_checks do
    plug HeliosServerWeb.Auth.Pipeline
    plug HeliosServerWeb.Plug.RedisJWT
  end

  # Other scopes may use custom stacks.
  # Unprotected API ROUTES (No auth needed)
  scope "/api", HeliosServerWeb do
    pipe_through :api

    scope "/v1", V1, as: :v1 do
      post "/auth/signup", UserController, :create
      post "/auth/signin", AuthController, :signin
    end
  end

  # Protected API ROUTES (Auth needed)
  scope "/api", HeliosServerWeb do
    pipe_through [:api, :api_auth_checks]

    scope "/v1", V1, as: :v1 do
      get "/auth/logout", AuthController, :logout
      
      resources "/users", UserController, except: [:index, :new, :edit, :create]
      get "/shallow-user/:username/:userhash", UserController, :shallow
      
      resources "/servers", ServerController, param: "server_id", except: [:index, :new, :edit]
      get "/user-servers", ServerController, :index
      get "/server-members/:server_id", ServerController, :members
      post "/server-join", ServerController, :join
      get "/server-leave/:server_id", ServerController, :leave
      
      resources "/server-invites", ServerInviteController, param: "invite_id", except: [:index, :show, :update, :new, :edit, :delete]
      get "/server-invites/:server_id/:invite_id", ServerInviteController, :show
      post "/server-invites/:server_id/:invite_id", ServerInviteController, :update
      delete "/server-invites/:server_id/:invite_id", ServerInviteController, :delete
      get "/list-invites/:server_id", ServerInviteController, :index
      
      resources "/channels", ChannelController, param: "channel_id", except: [:index, :show, :new, :edit, :update, :delete]
      get "/channels/:server_id/:channel_id", ChannelController, :show
      put "/channels/:server_id/:channel_id", ChannelController, :update
      delete "/channels/:server_id/:channel_id", ChannelController, :delete
      get "/server-channels/:server_id", ChannelController, :index
      
      get "/messages/:channel_id/:message_id", MessageController, :index
      get "/messages/:channel_id", MessageController, :index
      
      post "/image-upload/avatar", FileController, :user_avatar
      post "/image-upload/server-banner/:server_id", FileController, :server_banner
      post "/image-upload/server-icon/:server_id", FileController, :server_icon

      get "/roles/:server_id", RoleController, :index
      post "/roles/:server_id", RoleController, :create
      put "/roles/:server_id/:role_id", RoleController, :update
      delete "/roles/:server_id/:role_id", RoleController, :delete
      
      get "/user-roles/:server_id/:user_id", UserRoleController, :index
      post "/user-roles/:server_id/:user_id", UserRoleController, :create
      get "/user-permissions/:server_id/:user_id", UserRoleController, :show
    end
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: HeliosServerWeb.Telemetry
    end
  end

  # MUST BE LAST ROUTE
  # EVERYTHING AFTER HERE WILL GO TO THE APP!
  scope "/", HeliosServerWeb do
    pipe_through :browser

    get "/invite/:server_id/:invite_id", ServerInvitePageController, :index
    get "/*path", PageController, :index
  end
end
