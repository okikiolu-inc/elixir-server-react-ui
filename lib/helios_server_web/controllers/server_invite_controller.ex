defmodule HeliosServerWeb.V1.ServerInviteController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema
  alias HeliosServer.Schema.ServerInvite

  action_fallback HeliosServerWeb.FallbackController

  def index(conn, %{"server_id" => server_id}) do
    with {:ok, server_invites} <- Schema.get_server_invites(server_id) do
      render(conn, "index.json", %{server_invites: server_invites})
    end
  end

  def create(conn, %{"server_invite" => invite_params}) do
    with {:ok, %ServerInvite{} = server_invite} <- Schema.create_server_invite(invite_params) do
      conn
      |> put_status(:created)
      |> render("show.json", server_invite: server_invite)
    end
  end

  def show(conn, %{"invite_id" => invite_id, "server_id" => server_id}) do
    with {:ok, %ServerInvite{} = server_invite} <- Schema.get_server_invite(server_id, invite_id) do
      render(conn, "show.json", server_invite: server_invite)
    end
  end

  def update(conn, %{"invite_id" => invite_id, "server_id" => server_id, "server_invite" => invite_updates}) do
    with {:ok, %ServerInvite{} = invite} <- Schema.get_server_invite(server_id, invite_id) do
      with {:ok, %ServerInvite{} = updated_invite} <- Schema.update_server_invite(invite, invite_updates) do
        render(conn, "show.json", invite: updated_invite)
      end
    end
  end

  def delete(conn, %{"invite_id" => invite_id, "server_id" => server_id}) do
    with {:ok, _} <- Schema.delete_server_invite(server_id, invite_id) do
      send_resp(conn, :no_content, "")
    end
  end
end
