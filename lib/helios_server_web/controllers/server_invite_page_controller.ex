defmodule HeliosServerWeb.ServerInvitePageController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema
  alias HeliosServer.Schema.Server
  alias HeliosServer.Schema.ServerInvite
  alias HeliosServer.Uploader.ServerBanner
  alias HeliosServer.Uploader.ServerIcon

  action_fallback HeliosServerWeb.FallbackController

  def index(conn, %{"invite_id" => invite_id, "server_id" => server_id}) do
    with {:ok, %ServerInvite{} = server_invite} <- Schema.get_server_invite(server_id, invite_id),
      {:ok, %Server{} = server} <- Schema.get_server(server_id) do
      icon = server_icon_url(server.icon, server.server_id)
      banner = server_banner_url(server.banner, server.server_id)
      params = %{server_invite: server_invite, server: server, icon: icon, banner: banner}
      conn
      |> put_layout("invite.html")
      |> render("invite.html", params: params)
    else
      _ ->
        conn
        |> put_layout("invite.html")
        |> render("no-invite.html", params: %{})
    end
  end

  defp server_icon_url(icon, _) when is_nil(icon) or icon == "" do ServerIcon.url(nil) end
  defp server_icon_url(icon, server_id) do ServerIcon.url({icon, %{server_id: server_id}}) end

  defp server_banner_url(banner, _) when is_nil(banner) or banner == "" do ServerBanner.url(nil) end
  defp server_banner_url(banner, server_id) do ServerBanner.url({banner, %{server_id: server_id}}) end
end
