defmodule HeliosServerWeb.V1.RoleController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema.Role

  action_fallback HeliosServerWeb.FallbackController

  def index(conn, %{"server_id" => server_id}) do
    with {:ok, roles} <- Role.get_roles(server_id) do
      render(conn, "index.json", roles: roles)
    end
  end

  def create(conn, %{"server_id" => server_id, "role" => role_params}) do
    with {:ok, role} <- Role.create_role(server_id, role_params) do
      conn
      |> put_status(:created)
      |> render("show.json", role: role)
    end
  end

  def update(conn, %{"server_id" => server_id, "role_id" => role_id, "role" => role_params}) do
    with :ok <- Role.update_role(server_id, role_id, role_params) do
      send_resp(conn, :no_content, "")
    end
  end

  def delete(conn, %{"server_id" => server_id, "role_id" => role_id}) do
    with :ok <- Role.delete_role(server_id, role_id) do
      send_resp(conn, :no_content, "")
    end
  end
end
