defmodule HeliosServerWeb.V1.UserRoleController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema.UserRole

  action_fallback HeliosServerWeb.FallbackController

  def index(conn, %{"server_id" => server_id, "user_id" => user_id}) do
    with {:ok, user_roles} <- UserRole.get_user_roles(server_id, user_id) do
      render(conn, "index.json", user_roles: user_roles)
    end
  end

  def show(conn, %{"server_id" => server_id, "user_id" => user_id}) do
    with {:ok, user_permissions} <- UserRole.get_user_permissions(server_id, user_id) do
      render(conn, "user_permissions.json", user_permissions: user_permissions)
    end
  end

  def create(conn, %{"server_id" => server_id, "user_id" => user_id, "roles" => user_roles}) do
    with :ok <- UserRole.assign_user_roles(server_id, user_id, user_roles) do
      send_resp(conn, :created, "")
    end
  end
end
