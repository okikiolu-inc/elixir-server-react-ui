defmodule HeliosServerWeb.V1.FileController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema
  alias HeliosServer.Uploader.Avatar
  alias HeliosServer.Uploader.ServerBanner
  alias HeliosServer.Uploader.ServerIcon

  action_fallback HeliosServerWeb.FallbackController

  # use multipart/form-data when your form includes any <input type="file">
  # example file tag
  # <input class="form-control" id="user_photo" name="user[photo]" type="file">

  # Url prefixes
  # avatar - https://#{cdn_url}/avatars/#{user.id}/#{random_hash}.#{original_file_extention}
  # server icon - https://#{cdn_url}/icons/#{server.server_id}/#{random_hash}.#{original_file_extention}
  # server banner - https://#{cdn_url}/banners/#{server.server_id}/#{random_hash}.#{original_file_extention}
  # message attachment - https://#{cdn_url}/attachments/#{channel.channel_id}/#{message.message_id}/#{random_hash}.#{original_file_extention}
  # Also
  # - They resize their images as they come in
  # - and can query the url by size (maybe this is cdn specific)

  def user_avatar(conn, %{"avatar" => avatar}) do
    # Url prefixes
    # avatar - https://#{cdn_url}/avatars/#{user.id}/#{random_hash}.#{original_file_extention}
    with {:ok, user_id} <- get_user_id(conn) do
      with {:ok, user} <- Schema.get_user(user_id) do
        # add new avatar to server
        with {:ok, filename} <- Avatar.store({avatar, user}) do
          # delete old avatar if any
          IO.puts("filename")
          IO.puts(filename)
          # Avatar.delete({user.avatar, user})
          # update DB records
          Schema.update_user_avatar(user, filename)
          avatar = Avatar.url({filename, %{id: user.id}})
          IO.puts("url")
          IO.puts(avatar)
          conn
            |> put_status(:ok)
            |> render("avatar.json", avatar: avatar)
        end
      else
        _ ->
        conn |> send_resp(400, "Bad Request")
      end
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  def server_banner(conn, %{"server_id" => server_id, "banner" => banner}) do
    # Url prefixes
    # server banner - https://#{cdn_url}/banners/#{server.server_id}/#{random_hash}.#{original_file_extention}
    with {:ok, _user_id} <- get_user_id(conn) do
      with {:ok, server} <- Schema.get_server(server_id) do
        # add new avatar to server
        with {:ok, filename} <- ServerBanner.store({banner, server}) do
          # update DB records
          Schema.update_server_banner(server, filename)
          server_banner = ServerBanner.url({filename, %{server_id: server.server_id}})
          conn
            |> put_status(:ok)
            |> render("server_banner.json", server_banner: server_banner)
        end
      else
        _ ->
        conn |> send_resp(400, "Bad Request")
      end
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  def server_icon(conn, %{"server_id" => server_id, "icon" => icon}) do
    # Url prefixes
    # server icon - https://#{cdn_url}/icons/#{server.server_id}/#{random_hash}.#{original_file_extention}
    with {:ok, _user_id} <- get_user_id(conn) do
      with {:ok, server} <- Schema.get_server(server_id) do
        # add new avatar to server
        with {:ok, filename} <- ServerIcon.store({icon, server}) do
          # update DB records
          Schema.update_server_icon(server, filename)
          server_icon = ServerIcon.url({filename, %{server_id: server.server_id}})
          conn
          |> put_status(:ok)
          |> render("server_icon.json", server_icon: server_icon)
        end
      else
        _ ->
        conn |> send_resp(400, "Bad Request")
      end
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  def message_attachment(_conn, %{"message" => _message_params}) do
    # Url prefixes
    # message attachment - https://#{cdn_url}/attachments/#{channel.channel_id}/#{message.message_id}/#{random_hash}.#{original_file_extention}
    # with {:ok, user_id} <- get_user_id(conn),
    #   {:ok, server} <- Schema.create_server(message_params, user_id) do
    #   conn
    #   |> put_status(:created)
    #   |> render("show.json", server: server)
    # else
    #   _ ->
    #     conn |> send_resp(401, "Unauthorized")
    # end
    nil
  end

  defp get_user_id(conn) do
    userid_list = get_req_header(conn, "userid")
    case userid_list do
      [user_id | _tail = []] ->
        {:ok, user_id}
      _ ->
        {:error, "unauthorized"}
    end
  end
end
