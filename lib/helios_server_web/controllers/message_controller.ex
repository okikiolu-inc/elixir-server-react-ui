defmodule HeliosServerWeb.V1.MessageController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema
  # alias HeliosServer.Schema.ChannelMessage
  # alias HeliosServer.Schema.DirectMessage

  action_fallback HeliosServerWeb.FallbackController

  def index(conn, %{"channel_id" => channel_id, "message_id" => message_id}) do
    with {:ok, messages} <- Schema.get_channel_messages(channel_id, message_id) do
      render(conn, "index.json", %{messages: messages})
    end
  end

  def index(conn, %{"channel_id" => channel_id}) do
    with {:ok, messages} <- Schema.get_channel_messages(channel_id) do
      render(conn, "index.json", %{messages: messages})
    end
  end

  # def create(conn, %{"message" => message_params}) do
  #   with {:ok, %ChannelMessage{} = channel_message} <- Schema.create_channel(message_params) do
  #     conn
  #     |> put_status(:created)
  #     |> render("show.json", channel_message: channel_message)
  #   end
  # end

  # def show(conn, %{"message_id" => message_id}) do
  #   with {:ok, %ChannelMessage{} = channel_message} <- Schema.get_channel(message_id) do
  #     render(conn, "show.json", channel_message: channel_message)
  #   end
  # end

  # def update(conn, %{"message_id" => message_id, "message" => message_params}) do
  #   with {:ok, %ChannelMessage{} = message} <- Schema.get_channel(message_id) do
  #     with {:ok, %ChannelMessage{} = updated_channel_message} <- Schema.update_channel(message, message_params) do
  #       render(conn, "show.json", channel_message: updated_channel_message)
  #     end
  #   end
  # end

  # def delete(conn, %{"message_id" => message_id}) do
  #   with {:ok, _} <- Schema.delete_channel(message_id) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
