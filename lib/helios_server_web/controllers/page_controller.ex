defmodule HeliosServerWeb.PageController do
  use HeliosServerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
