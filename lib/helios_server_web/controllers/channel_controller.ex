defmodule HeliosServerWeb.V1.ChannelController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema
  alias HeliosServer.Schema.Channel

  action_fallback HeliosServerWeb.FallbackController

  def index(conn, %{"server_id" => server_id}) do
    with {:ok, channels} <- Schema.get_server_channels(server_id) do
      render(conn, "index.json", %{channels: channels})
    end
  end

  def create(conn, %{"channel" => channel_params}) do
    with {:ok, %Channel{} = channel} <- Schema.create_channel(channel_params) do
      conn
      |> put_status(:created)
      |> render("show.json", channel: channel)
    end
  end

  def show(conn, %{"server_id" => server_id, "channel_id" => channel_id}) do
    with {:ok, %Channel{} = channel} <- Schema.get_channel(server_id, channel_id) do
      render(conn, "show.json", channel: channel)
    end
  end

  def update(conn, %{"server_id" => server_id, "channel_id" => channel_id, "channel" => channel_params}) do
    with {:ok, %Channel{} = channel} <- Schema.get_channel(server_id, channel_id) do
      with {:ok, %Channel{} = updated_channel} <- Schema.update_channel(channel, channel_params) do
        render(conn, "show.json", channel: updated_channel)
      end
    end
  end

  def delete(conn, %{"server_id" => server_id, "channel_id" => channel_id}) do
    with {:ok, _} <- Schema.delete_channel(server_id, channel_id) do
      send_resp(conn, :no_content, "")
    end
  end
end
