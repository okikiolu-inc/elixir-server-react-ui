defmodule HeliosServerWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use HeliosServerWeb, :controller

  # This clause handles errors returned by Ecto's insert/update/delete.
  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(HeliosServerWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_status(:unauthorized)
    |> put_view(HeliosServerWeb.ErrorView)
    |> render(:"401")
  end

  # This clause is an example of how to handle resources that cannot be found.
  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(HeliosServerWeb.ErrorView)
    |> render(:"404")
  end

    # This clause is an example of how to handle resources that cannot be found.
  def call(conn, {:error, :internal_server_error}) do
    conn
    |> put_status(:not_found)
    |> put_view(HeliosServerWeb.ErrorView)
    |> render(:"500")
  end

  # Catch all clause for generic messages
  # must be last in the list
  def call(conn, {:error, msg}) do
    IO.puts "fallback_controller #{msg}"
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(HeliosServerWeb.ErrorView)
    |> render("error.json", msg)
  end

  # Catch all clause for generic messages
  # must be last in the list
  def call(conn, msg) do
    IO.puts "fallback_controller #{msg}"
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(HeliosServerWeb.ErrorView)
    |> render("error.json", msg)
  end
end
