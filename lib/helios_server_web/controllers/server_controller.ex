defmodule HeliosServerWeb.V1.ServerController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema
  alias HeliosServer.Schema.Server

  action_fallback HeliosServerWeb.FallbackController

  def index(conn, _params) do
    with {:ok, user_id} <- get_user_id(conn),
    {:ok, servers} <- Schema.get_user_servers(user_id),
    {:ok, owned_servers} <- Schema.get_owned_servers(user_id) do
      render(conn, "index.json", %{servers: servers, owned_servers: owned_servers})
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  def members(conn, %{"server_id" => server_id}) do
    with {:ok, server_members} <- Schema.get_server_members(server_id) do
      render(conn, "member_list.json", %{server_members: server_members})
    end
  end

  def join(conn, %{"server" => server_params}) do
    with {:ok, user_id} <- get_user_id(conn),
      {:ok, %Server{} = server} <- Schema.join_server(server_params, user_id) do
      conn
      |> put_status(:created)
      |> render("show.json", server: server)
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  def leave(conn, %{"server_id" => server_id}) do
    with {:ok, user_id} <- get_user_id(conn),
      :ok <- Schema.leave_server(user_id, server_id) do
        conn |> send_resp(:no_content, "")
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  def create(conn, %{"server" => server_params}) do
    with {:ok, user_id} <- get_user_id(conn),
      {:ok, %Server{} = server} <- Schema.create_server(server_params, user_id) do
      conn
      |> put_status(:created)
      |> render("show.json", server: server)
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  def show(conn, %{"server_id" => server_id}) do
    with {:ok, %Server{} = server} <- Schema.get_server(server_id) do
      render(conn, "show.json", server: server)
    end
  end

  def update(conn, %{"server_id" => server_id, "server" => server_params}) do
    with {:ok, %Server{} = server} <- Schema.get_server(server_id) do
      with {:ok, %Server{} = updated_server} <- Schema.update_server(server, server_params) do
        render(conn, "show.json", server: updated_server)
      end
    end
  end

  def delete(conn, %{"server_id" => server_id}) do
    with {:ok, user_id} <- get_user_id(conn),
      {:ok, %Server{} = server} <- Schema.get_server(server_id) do
      if server.owner_id == String.to_integer(user_id) do
        with {:ok, _} <- Schema.delete_server(server_id) do
          send_resp(conn, :no_content, "")
        end
      end
    else
      _ ->
        conn |> send_resp(401, "Unauthorized")
    end
  end

  defp get_user_id(conn) do
    userid_list = get_req_header(conn, "userid")
    case userid_list do
      [user_id | _tail = []] ->
        {:ok, user_id}
      _ ->
        {:error, "unauthorized"}
    end
  end
end
