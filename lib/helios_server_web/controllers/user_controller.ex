defmodule HeliosServerWeb.V1.UserController do
  use HeliosServerWeb, :controller

  alias HeliosServer.Schema
  alias HeliosServer.Schema.User
  alias HeliosServerWeb.Auth.Guardian

  action_fallback HeliosServerWeb.FallbackController

  def create(conn, %{"username" => username, "email" => email, "password" => password}) do
    user_params = %{
      username: username,
      email: email,
      password: password
    }
    with {:ok, %User{} = user} <- Schema.create_user(user_params),
    {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      case Redix.command(:redix, ["SET", "token.#{user.id}", token]) do
        {:ok, _} ->
          conn
            |> put_status(:created)
            |> render("user.json", %{user: user, token: "#{token}"})
        {:error, msg} ->
          conn
            |> put_status(:unauthorized)
            |> render("error.json", %{error: msg})
      end
    else
      {:error, msg} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", %{error: msg})
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, %User{} = user} <- Schema.get_user(id) do
      render(conn, "show.json", user: user)
    end
  end

  def shallow(conn, %{"username" => username, "userhash" => userhash}) do
    with {:ok, user_cred} <- Schema.get_by_username_and_userhash(username, userhash) do
      render(conn, "shallow.json", id: user_cred.user_id)
    end
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    with {:ok, %User{} = user} <- Schema.get_user(id) do
      with {:ok, %User{} = updated_user} <- Schema.update_user(user, user_params) do
        render(conn, "show.json", user: updated_user)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %User{} = user} <- Schema.get_user(id) do
      with {:ok, _} <- Schema.delete_user(user) do
        send_resp(conn, :no_content, "")
      end
    end
  end
end
