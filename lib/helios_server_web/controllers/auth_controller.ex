defmodule HeliosServerWeb.V1.AuthController do
  use HeliosServerWeb, :controller

  alias HeliosServerWeb.Auth.Guardian

  action_fallback HeliosServerWeb.FallbackController

  def signin(conn, %{"email" => email, "password" => password}) do
    with {:ok, user, token} <- Guardian.authenticate(email, password) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  def logout(conn, _) do
    with {:ok, _} <- Guardian.logout(conn) do
      send_resp(conn, :ok, "")
    end
  end
end
