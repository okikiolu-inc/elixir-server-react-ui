defimpl Canada.Can, for: HeliosServer.Schema.User do
  alias HeliosServer.Schema.User

  def can?(%User{id: user_id}, action)
    when action in [:update, :read, :destroy, :touch], do: true

  def can?(%User{id: id}, action, _)
    when action in [:update, :read, :destroy, :touch], do: true

  def can?(%User{}, :create), do: true
end