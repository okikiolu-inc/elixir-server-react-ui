defmodule HeliosServer.Schema.ServerByMember do
  require HeliosServer.Schema.ServerMember  # if you want to auto-create after compile
  use Triton.MaterializedView

  materialized_view :server_by_member, from: HeliosServer.Schema.ServerMember do
    fields :all
    partition_key [:user_id]
    cluster_columns [:server_id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        server_id: :desc
      ]
    ]
  end
end

