defmodule HeliosServer.Schema.UserCred do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  defstruct userhash: "", username: "", user_id: 0

  table :usercreds, keyspace: HeliosServer.Schema.Keyspace do
    field :userhash, :text, validators: [presence: true, length: [is: 4]]
    field :username, :text, validators: [presence: true]
    field :user_id, :bigint, validators: [presence: true]
    partition_key [:username]
    cluster_columns [:userhash]
    with_options [
      gc_grace_seconds: 172_800,
    ]
  end
end
