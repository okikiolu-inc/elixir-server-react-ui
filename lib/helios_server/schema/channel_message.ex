defmodule HeliosServer.Schema.ChannelMessage do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  defstruct channel_id: 0, message_id: 0, author_id: 0, content: "", updated: ""

  table :channel_messages, keyspace: HeliosServer.Schema.Keyspace do
    field :channel_id, :bigint, validators: [presence: true]
    field :bucket, :int, validators: [presence: true]
    field :message_id, :bigint, validators: [presence: true] # unique_constraint
    field :author_id, :bigint, validators: [presence: true]
    field :content, :text, validators: [presence: true]
    field :updated, :timestamp
    field :created, :timestamp
    partition_key [:channel_id, :bucket]
    cluster_columns [:message_id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        message_id: :desc
      ]
    ]
  end
end

# CREATE TABLE channel_messages (
#   channel_id bigint,
#   bucket int,
#   message_id bigint,
#   author_id bigint,
#   content text,
#   PRIMARY KEY ((channel_id, bucket), message_id)
# ) WITH CLUSTERING ORDER BY (message_id DESC);