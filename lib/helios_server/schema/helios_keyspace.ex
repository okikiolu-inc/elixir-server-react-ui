defmodule HeliosServer.Schema.Keyspace do
  use Triton.Keyspace

  keyspace :helios, conn: Triton.Conn do
    with_options [
      replication: "{'class' : 'SimpleStrategy', 'replication_factor': 1}"
      # For production do 3
      # replication: "{'class' : 'SimpleStrategy', 'replication_factor': 3}"
    ]
  end
end