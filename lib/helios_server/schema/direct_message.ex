defmodule HeliosServer.Schema.DirectMessage do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  table :direct_messages, keyspace: HeliosServer.Schema.Keyspace do
    field :channel_id, :bigint, validators: [presence: true]
    field :bucket, :int, validators: [presence: true]
    field :message_id, :bigint, validators: [presence: true] # unique_constraint
    field :author_id, :bigint, validators: [presence: true]
    field :content, :text, validators: [presence: true]
    field :updated, :timestamp
    field :created, :timestamp
    partition_key [:channel_id, :bucket]
    cluster_columns [:message_id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        message_id: :desc
      ]
    ]
  end
end

# a direct message is a message to an impromptu created channel (DM)
# CREATE TABLE direct_messages (
#   channel_id bigint,
#   bucket int,
#   message_id bigint,
#   author_id bigint,
#   content text,
#   PRIMARY KEY ((recipient_id, bucket), message_id)
# ) WITH CLUSTERING ORDER BY (message_id DESC);