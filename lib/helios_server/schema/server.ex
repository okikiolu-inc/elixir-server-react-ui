defmodule HeliosServer.Schema.Server do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  defstruct server_id: 0, description: "", icon: "", banner: "", name: "", owner_id: 0

  table :servers, keyspace: HeliosServer.Schema.Keyspace do
    field :server_id, :bigint, validators: [presence: true]  # validators using vex
    field :owner_id, :bigint, validators: [presence: true]
    field :name, :text, validators: [presence: true] # unique_constraint
    field :description, :text
    field :icon, :text
    field :banner, :text
    field :updated, :timestamp
    field :created, :timestamp
    partition_key [:server_id]
    with_options [
      gc_grace_seconds: 172_800,
    ]
  end
end
