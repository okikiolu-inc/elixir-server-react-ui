defmodule HeliosServer.Schema.Permission do
  @moduledoc """
    Permissions are read-only and are defined once and read everywhere
  """

  @permissions %{
    "SOL_INVICTUS" => %{},
    "administrator" => %{},
    "manage_server" => %{},
    "manage_roles" => %{},
    "manage_channels" => %{},
    "create_invite" => %{},
    "send_messages" => %{},
    "embed_links" => %{},
    "attach_files" => %{},
    "ban_members" => %{},
  }
  
  def get_permission(id) do
    Map.get(@permissions, id)
  end
end
