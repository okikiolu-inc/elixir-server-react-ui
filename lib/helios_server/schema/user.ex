defmodule HeliosServer.Schema.User do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  defstruct id: 0, avatar: "", email: "", password: "", userhash: "", username: "", status: ""

  table :users, keyspace: HeliosServer.Schema.Keyspace do
    field :id, :bigint, validators: [presence: true]  # validators using vex
    field :avatar, :text
    field :email, :text, validators: [presence: true, format: ~r/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/]
    field :password, :text, validators: [presence: true, length: [min: 6]]
    field :userhash, :text, validators: [presence: true, length: [is: 4]]
    field :username, :text, validators: [presence: true]
    field :status, :text
    field :updated, :timestamp
    field :created, :timestamp
    partition_key [:id]
    with_options [
      gc_grace_seconds: 172_800,
    ]
  end


  # @doc false
  # def changeset(%User{} = user, attrs \\ %{}) do
  #   user
  #   |> cast(attrs, [:username, :userhash, :avatar, :email, :password])
  #   |> validate_required([:username, :userhash, :email, :password])
  #   |> validate_format(:email, ~r/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)
  #   |> validate_length(:password, min: 6)
  #   |> validate_length(:userhash, is: 4)
  #   |> put_hashed_password
  # end

  # defp put_hashed_password(changeset) do
  #   case changeset do
  #     %Ecto.Changeset{valid?: true, changes: %{password: password}}
  #     ->
  #       put_change(changeset, :password, Comeonin.Bcrypt.hashpwsalt(password))
  #   _ ->
  #       changeset
  #   end
  # end
end
