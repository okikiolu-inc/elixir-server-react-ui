defmodule HeliosServer.Schema.UserRole do
  require HeliosServer.Schema.Keyspace
  use Triton.Table
  alias HeliosServer.Schema.UserRole

  defstruct user_id: 0, server_id: 0, roles: []

  table :user_roles, keyspace: HeliosServer.Schema.Keyspace do
    field :user_id, :bigint, validators: [presence: true]
    field :server_id, :bigint, validators: [presence: true]
    field :roles, {:set, "<bigint>"}
    partition_key [:user_id, :server_id]
    with_options [
      gc_grace_seconds: 172_800,
    ]
  end

  import Triton.Query

  @doc """
  Get a list of roles for a user

  ## Examples

      iex> get_user_roles(123, 456)
      {:ok, [%UserRole{}]}

      iex> get_user_roles(0, 0)
      {:error, msg}

  """
  def get_user_roles(s_id, u_id) do
    server_id = String.to_integer(s_id)
    user_id = String.to_integer(u_id)
    case UserRole
    |> prepared(server_id: server_id, user_id: user_id)
    |> select(:all)
    |> where(server_id: :server_id, user_id: :user_id)
    |> UserRole.all do
    {:ok, nil} ->
      {:ok, []}
    {:ok, []} ->
      {:ok, []}
    {:ok, user_roles} ->
      {:ok, user_roles}
    {:error, msg} ->
      {:error, msg}
    end
  end

  @doc """
  Gets a list of permissions for a user

  ## Examples

      iex> get_user_permissions(server_id, user_id)
      {:ok, %Role{}}

      iex> get_user_permissions(server_id, user_id)
      {:error, msg}

  """
  def get_user_permissions(s_id, u_id) do
    server_id = String.to_integer(s_id)
    user_id = String.to_integer(u_id)
    {:ok, []}
  end

  @doc """
  Assigns (add or remove) roles to a user.

  ## Examples

      iex> assign_user_roles(123, 456, [1,2,3,4,5])
      :ok

      iex> assign_user_roles(0, 0, [])
      {:error, msg}

  """
  def assign_user_roles(s_id, u_id, role_ids) do
    server_id = String.to_integer(s_id)
    user_id = String.to_integer(u_id)
    # role_ids = Enum.map(r_ids, &(String.to_integer(&1)))
    with {:ok, :success} <- UserRole
    |> prepared(server_id: server_id, user_id: user_id, roles: "{#{Enum.join(role_ids, ",")}}")
    |> insert(server_id: :server_id, user_id: :user_id, roles: :roles)
    |> if_not_exists
    |> UserRole.save do
      :ok
    end
  end
end

# Update the roles set
# UserRole
# |> update(roles: "{'1233446677878', '1233446677899', '1233449997878'}")
# |> where(user_id: 10, server_id: 10)
# |> UserRole.save

# Add a role_id to roles set
# UserRole
# |> update(roles: "roles + {'1233446677878'}")
# |> where(user_id: 10, server_id: 10)
# |> UserRole.save


# Remove role_id from set
# UserRole
# |> update(roles: "roles - {'1233446677878'}")
# |> where(user_id: 10, server_id: 10)
# |> UserRole.save