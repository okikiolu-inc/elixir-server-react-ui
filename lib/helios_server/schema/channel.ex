defmodule HeliosServer.Schema.Channel do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  defstruct channel_id: 0, server_id: 0, name: "", description: ""

  table :channels, keyspace: HeliosServer.Schema.Keyspace do
    field :server_id, :bigint, validators: [presence: true] # unique_constraint
    field :channel_id, :bigint, validators: [presence: true]
    field :name, :text, validators: [presence: true]
    field :description, :text
    partition_key [:server_id]
    cluster_columns [:channel_id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        channel_id: :desc
      ]
    ]
  end
end

# CREATE TABLE channel_messages (
#   server_id bigint,
#   channel_id bigint,
#   name text,
#   PRIMARY KEY ((server_id), channel_id)
# ) WITH CLUSTERING ORDER BY (channel_id DESC);