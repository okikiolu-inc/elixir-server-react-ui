defmodule HeliosServer.Schema.Role do
  require HeliosServer.Schema.Keyspace
  use Triton.Table
  alias HeliosServer.Schema.Role

  defstruct server_id: 0, role_id: 0, permissions: nil, name: "", color: ""

  table :roles, keyspace: HeliosServer.Schema.Keyspace do
    field :server_id, :bigint, validators: [presence: true]
    field :role_id, :bigint, validators: [presence: true] # unique_constraint
    field :permissions, {:set, "<text>"}
    field :name, :text, validators: [presence: true]
    field :color, :text, validators: [presence: true]
    partition_key [:server_id]
    cluster_columns [:role_id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        role_id: :desc
      ]
    ]
  end

  import Triton.Query

  @doc """
  Gets a list of roles by server_id.

  Returns an error message if the User does not exist.

  ## Examples

      iex> get_user(123)
      {:ok, [%Role{}]}


      iex> get_user(456)
      {:error, "Message"}
  
  """
  def get_roles(server_id) when is_integer(server_id) do
    with {:ok, roles} <- Role
    |> prepared(server_id: server_id)
    |> select(:all)
    |> where(server_id: :server_id)
    |> Role.all do
      if roles != nil do
        {:ok, roles}
      else
        {:ok, []}
      end
    else
      {:error, msg} ->
        {:error, msg}
    end
  end

  def get_roles(id) do
    get_roles(String.to_integer(id))
  end

  @doc """
  Creates a server.

  ## Examples

      iex> create_role(server_id, role_params)
      {:ok, %Role{}}

      iex> create_role(server_id, bad_role_params)
      {:error, msg}

  """
  def create_role(s_id, %{"name" => name, "permissions" => permissions, "color" => color}) do
    server_id = String.to_integer(s_id)
    case Snowflake.next_id() do
      {:ok, role_id} ->
        with {:ok, :success} <- Role
        |> prepared(role_id: role_id, server_id: server_id, name: name, color: color)
        |> insert(role_id: :role_id, server_id: :server_id, name: :name, color: :color, permissions: "{#{Enum.join(Enum.map(permissions, &("'#{&1}'")), ",")}}")
        |> if_not_exists
        |> Role.save do
          created_role = %Role{
            server_id: server_id,
            role_id: role_id,
            name: name,
            color: color,
            permissions: permissions,
          }
          {:ok, created_role}
      else
        {:error, msg} ->
          {:error, msg}
      end
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Updates a role.

  ## Examples

      iex> update_role(server_id, role_id, updates)
      {:ok, update_role}

      iex> update_role(server_id, role_id, updates)
      {:error, msg} # msg is a string
  """
  def update_role(s_id, r_id, %{"name" => name, "color" => color, "permissions" => permissions}) do
    server_id = String.to_integer(s_id)
    role_id = String.to_integer(r_id)

    with {:ok, :success} <- Role
    |> prepared(name: name, color: color)
    |> update(name: :name, color: :color, permissions: "{#{Enum.join(Enum.map(permissions, &("'#{&1}'")), ",")}}")
    |> where(server_id: server_id, role_id: role_id)
    |> Role.save do
      :ok
    else
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Deletes a role.

  ## Examples

      iex> delete_role(server_id, role_id)
      {:ok, :success}

      iex> delete_role(server_id, role_id)
      {:error, msg} # msg is a string

  """
  def delete_role(server_id, role_id) do
    Role
    |> prepared(server_id: String.to_integer(server_id), role_id: String.to_integer(role_id))
    |> delete(:all)  # here :all refers to all fields
    |> where(server_id: :server_id, role_id: :role_id)
    |> Role.del
    :ok
  end
end

# Update the entire permissions set
# Role
# |> update(permissions: "{'1233446677878', '1233446677899', '1233449997878'}")
# |> where(user_id: 10)
# |> Role.save

# Add a role_id to permissions set
# Role
# |> update(permissions: "permissions + {'1233446677878'}")
# |> where(user_id: 10)
# |> UserRole.save


# Remove role_id from set
# Role
# |> update(permissions: "permissions - {'1233446677878'}")
# |> where(user_id: 10)
# |> Role.save