defmodule HeliosServer.Schema.UserByEmail do
  require HeliosServer.Schema.User  # if you want to auto-create after compile
  use Triton.MaterializedView

  materialized_view :users_by_email, from: HeliosServer.Schema.User do
    fields :all
    partition_key [:email]
    cluster_columns [:id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        email: :asc,
        id: :desc
      ]
    ]
  end
end
