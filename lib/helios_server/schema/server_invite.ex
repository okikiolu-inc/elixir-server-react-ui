defmodule HeliosServer.Schema.ServerInvite do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  defstruct server_id: 0, invite_id: 0, join_count: 0, duration: 0, created: ""

  table :server_invites, keyspace: HeliosServer.Schema.Keyspace do
    field :server_id, :bigint, validators: [presence: true]
    field :invite_id, :bigint, validators: [presence: true]
    field :join_count, :int, validators: [presence: true]
    field :duration, :int, validators: [presence: true]
    field :updated, :timestamp
    field :created, :timestamp
    partition_key [:server_id]
    cluster_columns [:invite_id]
    with_options [
      gc_grace_seconds: 172_800,
    ]
  end
end
