defmodule HeliosServer.Schema.ServerMember do
  require HeliosServer.Schema.Keyspace
  use Triton.Table

  table :server_members, keyspace: HeliosServer.Schema.Keyspace do
    field :server_id, :bigint, validators: [presence: true]
    field :user_id, :bigint, validators: [presence: true]
    field :created, :timestamp
    partition_key [:server_id]
    cluster_columns [:user_id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        user_id: :desc
      ]
    ]
  end
end

# CREATE TABLE channel_messages (
#   channel_id bigint,
#   bucket int,
# 
#   message_id bigint,
#   author_id bigint,
#   content text,
#   PRIMARY KEY ((channel_id, bucket), message_id)
# ) WITH CLUSTERING ORDER BY (message_id DESC);