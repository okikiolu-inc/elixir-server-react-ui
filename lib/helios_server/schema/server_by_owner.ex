defmodule HeliosServer.Schema.ServerByOwner do
  require HeliosServer.Schema.Server  # if you want to auto-create after compile
  use Triton.MaterializedView

  materialized_view :server_by_owner, from: HeliosServer.Schema.Server do
    fields :all
    partition_key [:owner_id]
    cluster_columns [:server_id]
    with_options [
      gc_grace_seconds: 172_800,
      clustering_order_by: [
        owner_id: :desc,
        id: :desc
      ]
    ]
  end
end

