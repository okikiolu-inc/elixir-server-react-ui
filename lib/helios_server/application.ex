defmodule HeliosServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # without curly braces '{}' means you are NOT passing args e.g App
      # with curly braces means you are passing args e.g {App, 0}
      # {App, param: val, param2: val2} is simply a keyword list passed to the App
      # Start the Ecto repository
      # HeliosServer.Repo,
      # Start the Telemetry supervisor
      HeliosServerWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: HeliosServer.PubSub},
      # Start the Endpoint (http/https)
      HeliosServerWeb.Endpoint,
      # Start a worker by calling: HeliosServer.Worker.start_link(arg)
      # {HeliosServer.Worker, arg}
      # redis cache connection, usage `Redix.command(:redix, ["PING"])` from anywhere
      {Redix, name: :redix, host: "127.0.0.1", port: 6379, password: "Xd0ShOwftW+GPf4hivv3rczBiT/hNalMzxWRd0MGar4774KmhIXzBj/Cv5o+5/pjFhrDNH1qfM2swQjJ"},
      # DynamicSupervisor allows us to add children dynamically.
      #  {DynamicSupervisor, name: KV.BucketSupervisor, strategy: :one_for_one},
      {GenRegistry, name: User.GenRegistry, worker_module: HeliosServer.Worker.User},
      {GenRegistry, name: Server.GenRegistry, worker_module: HeliosServer.Worker.Server},
    ]

    _isolated_children = []

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: HeliosServer.Supervisor]
    Supervisor.start_link(children, opts)
  end


  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    HeliosServerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
