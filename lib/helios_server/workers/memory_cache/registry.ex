defmodule HeliosServer.MemoryCache.Registry do
    use GenServer

    # Client (outside of the genserver process)
    @doc """
    Starts the registry.
    """
    def start_link(opts) do
        GenServer.start_link(__MODULE__, :ok, opts)
    end

    @doc """
    Looks up the bucket pid for `name` stored in `server`.

    Returns `{:ok, pid}` if the bucket exists, `:error` otherwise.
    """
    def lookup(server, name) do
        GenServer.call(server, {:lookup, name})
    end

    @doc """
    Ensures there is a bucket associated with the given `name` in `server`.
    """
    def create(server, name) do
        GenServer.cast(server, {:create, name})
    end

    # Server (Callbacks) inside of the genserver process
    @impl true
    def init(:ok) do
        names = %{}
        refs = %{}
        {:ok, {names, refs}}
    end

    @impl true
    def handle_call({:lookup, name}, _from, state) do
        {names, _} = state
        {:reply, Map.fetch(names, name), state}
    end

    @impl true
    def handle_cast({:create, name}, {names, refs}) do
        if Map.has_key?(names, name) do
            {:noreply, {names, refs}}
        else
            {:ok, bucket} = HeliosServer.MemoryCache.Bucket.start_link(%{})
            ref = Process.monitor(bucket)
            refs = Map.put(refs, ref, name)
            names = Map.put(names, name, bucket)
            {:noreply, {names, refs}}
        end
    end

    @impl true
    def handle_info({:DOWN, ref, :process, _pid, _reason}, {names, refs}) do
        {name, refs} = Map.pop(refs, ref)
        names = Map.delete(names, name)
        {:noreply, {names, refs}}
    end

    @impl true
    def handle_info(_msg, state) do
        {:noreply, state}
    end

    #  can use this construct to schedule work to run at a later time e.g expire keys in a cache
    # defp schedule_work do
    #     # In 2 hours
    #     Process.send_after(self(), :work, 2 * 60 * 60 * 1000)
    # end
end