defmodule HeliosServer.MemoryCache.Supervisor do
    # Automatically defines child_spec/1
    use Supervisor

    alias HeliosServer.MemoryCache.Registry

    def start_link(opts) do
        Supervisor.start_link(__MODULE__, :ok, opts)
    end

    @impl true
    def init(:ok) do
        children = [
            {Registry, name: MemoryCache.Registry},
        ]

        Supervisor.init(children, strategy: :one_for_one)
    end
end