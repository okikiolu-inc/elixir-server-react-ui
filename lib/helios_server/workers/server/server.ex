defmodule HeliosServer.Worker.Server do
    use GenServer

    alias HeliosServer.Schema.{ChannelMessage}
    import Triton.Query

    ## [Client] (outside of the genserver process)
    @doc """
    Starts the Server GenServer opts are [server_id].
    """
    def start_link(server_id) do
        GenServer.start_link(__MODULE__, [server_id])
    end

    @doc """
    Add a user to the server session list

    Returns `:ok` if the user was added, `:error` otherwise.
    """
    def connect_user(server, user_id, pid) do
        GenServer.call(server, {:connect, user_id, pid})
    end

    @doc """
    Remove a user to the server session list

    Returns `:ok` if the user was removed, `:error` otherwise.
    """
    def disconnect_user(server, user_id, pid) do
        GenServer.call(server, {:disconnect, user_id, pid})
    end

    @doc """
    Remove a user to the server session list

    Returns `:ok` if the user was removed, `:error` otherwise.
    """
    def save_message(server, channel_id, payload) do
        GenServer.cast(server, {:save_message, channel_id, payload})
    end


    @doc """
    Broadcast a message to the server

    Returns `:ok` if the user was removed, `:error` otherwise.
    """
    def broadcast_message(server, channel_id, payload) do
        GenServer.cast(server, {:publish, channel_id, payload})
    end

    @doc """
    Broadcast a message to the server

    Returns `:ok` if the user was removed, `:error` otherwise.
    """
    def broadcast_update(server, payload) do
        GenServer.cast(server, {:update, payload})
    end

    ## [Server] (Callbacks) inside of the genserver process
    @impl true
    def init([server_id | _tail = []]) do
        state = %{
            server_id: server_id,
            sessions: %{},
        }
        {:ok, state}
    end

    @impl true
    def handle_call({:connect, user_id, pid}, _from, %{sessions: sessions}=state) do
        session = %Session{
            user_id: user_id,
            pid: pid
        }
        {:reply, :ok, %{state | sessions: Map.put(sessions, user_id, session)}}
    end

    @impl true
    def handle_call({:disconnect, user_id}, _from, %{sessions: sessions}=state) do
        {:reply, :ok, %{state | sessions: Map.delete(sessions, user_id)}}
    end

    @impl true
    def handle_cast({:save_message, channel_id, %{"content" => content, "user_id" => raw_user_id, now_time: now_time}}, state) do
        # Get bucket int by doing
        bucket = Snowflake.Util.bucket(10, :days, channel_id) # assuming our bucket is 10 days
        with {:ok, message_id} <- Snowflake.next_id() do
            user_id = String.to_integer(raw_user_id)
            with {:ok, :success} <- ChannelMessage
            |> prepared(channel_id: channel_id, message_id: message_id, author_id: user_id, bucket: bucket, content: content, updated: now_time, created: now_time)
            |> insert(channel_id: :channel_id, message_id: :message_id,  author_id: :author_id, bucket: :bucket, content: :content, updated: :updated, created: :created)
            |> if_not_exists
            |> ChannelMessage.save do
                {:noreply, state}
            else
                _ ->
                    {:noreply, state}
            end
        else
            _ ->
                {:noreply, state}
        end
    end

    # for now, naive loops, when performance is needed, better data structures / sharding
    @impl true
    def handle_cast({:publish, _, payload}, %{sessions: sessions}=state) do
        Enum.each(sessions, fn {_, v} ->
            Manifold.send(v.pid, {:receive, payload})
        end)
        {:noreply, state}
    end

    # for now, naive loops, when performance is needed, better data structures / sharding
    @impl true
    def handle_cast({:update, payload}, %{sessions: sessions}=state) do
        Enum.each(sessions, fn {_, v} ->
            Manifold.send(v.pid, {:receive, payload})
        end)
        {:noreply, state}
    end
end

# call, is Synchronous
# cast, is Asynchronous

# use different data structure for PID list?
# e.g a hash ring?

# Using GenRegistry
# we can use IDs as process IDs for a GenServer process
# e.g using a user ID for our blocklist process, so we dont have to keep track of random ID's
# returns {:ok, pid} or {:error, :not_found}
# we could always use "#{user_id}.blacklist", for namespacing purposes
# def place_call(sender, recipient) do
#   # Get the recipients Blacklist GenServer
#   {:ok, blacklist} = GenRegistry.lookup_or_start(Blacklist, recipient_id, [recipient])
    # recipient_id, the unique id to map the process to
    # NOTE "[param1, param2, ...]" are your startlink arguments in an array, maps to
    # startlink(param1, param2, ...), for instance
#   if Blacklist.allow?(blacklist, sender) do
#     {:ok, :place_call}
#   else
#     {:error, :reject_call}
#   end 
# end

# Manifold
# Manifold.send(self(), :hello)
# Manifold.send([self(), self()], :hello)

# Semaphore
# if Semaphore.acquire(:test, 1) do
#   IO.puts "acquired"
#   Semaphore.release(:test)
# end

# case Semaphore.call(:test, 1, fn -> :ok end) do
#   :ok ->
#     IO.puts "success"
#   {:error, :max} ->
#     IO.puts "too many callers"
# end

# When users logon, they spin up a session process (a GenServer)
# which communicates with remote erlang nodes that contain guilds (i.e discord servers/rooms) (also GenServers)
# anything pushed to a guild GenServer is fanned out to every session GenServer