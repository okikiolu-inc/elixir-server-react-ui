defmodule HeliosServer.Worker.User do
    use GenServer

    alias Phoenix.Channel

    ## [Client] (outside of the genserver process)
    @doc """
    Starts the registry.
    """
    # def start_link(user_id, socket) do
    #     GenServer.start_link(__MODULE__, user_id, socket)
    # end
    def start_link(user_id) do
        GenServer.start_link(__MODULE__, [user_id])
    end
    
    def set_socket(user, socket) do
        GenServer.call(user, {:set_socket, socket})
    end

    ## [Server] (Callbacks) inside of the genserver process
    @impl true
    def init([user_id | _tail = []]) do
        {:ok, %{user_id: user_id, socket: nil}}
    end

    @impl true
    def handle_call({:set_socket, socket}, _from, state) do
        {:reply, :ok, %{state | socket: socket}}
    end


    # On receipt of a message 
    # Handles Manifold.send(...,...) messages
    @impl true
    def handle_info({:receive, payload}, %{socket: socket}=state) do
        # Send message to user using their socket
        Channel.push(
            socket,
            "message",
            payload
        )
        {:noreply, state}
    end

    #catch-all clause for the handle_info for handling unkown messages
    @impl true
    def handle_info(_msg, state) do
        IO.puts "unknown message"
        {:noreply, state}
    end
end

# Using GenRegistry
# we can use IDs as process IDs for a GenServer process
# e.g using a user ID for our blocklist process, so we dont have to keep track of random ID's
# returns {:ok, pid} or {:error, :not_found}
# we could always use "#{user_id}.blacklist", for namespacing purposes
# def place_call(sender, recipient) do
#   # Get the recipients Blacklist GenServer
#   {:ok, blacklist} = GenRegistry.lookup_or_start(Blacklist, recipient_id, [recipient])
    # recipient_id, the unique id to map the process to
    # NOTE "[param1, param2, ...]" are your startlink arguments in an array, maps to
    # startlink(param1, param2, ...), for instance
#   if Blacklist.allow?(blacklist, sender) do
#     {:ok, :place_call}
#   else
#     {:error, :reject_call}
#   end 
# end

# Manifold
# Manifold.send(self(), :hello)
# Manifold.send([self(), self()], :hello)

# Semaphore
# if Semaphore.acquire(:test, 1) do
#   IO.puts "acquired"
#   Semaphore.release(:test)
# end

# case Semaphore.call(:test, 1, fn -> :ok end) do
#   :ok ->
#     IO.puts "success"
#   {:error, :max} ->
#     IO.puts "too many callers"
# end

# When users logon, they spin up a session process (a GenServer)
# which communicates with remote erlang nodes that contain guilds (i.e discord servers/rooms) (also GenServers)
# anything pushed to a guild GenServer is fanned out to every session GenServer