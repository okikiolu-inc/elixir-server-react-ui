defmodule HeliosServerWeb.Plug.RedisJWT do
    import Plug.Conn
    alias HeliosServerWeb.Auth.Guardian

    def init(default), do: default

    def call(conn, _default) do
        userid_list = get_req_header(conn, "userid")
        case userid_list do
        [user_id | _tail = []] ->
            case Guardian.get_token(conn) do
            {:error, _} ->
                conn |> send_resp(401, "Unauthorized") |> halt()
            {:ok, g_token} ->
                case Redix.command(:redix, ["GET", "token.#{user_id}"]) do
                {:error, _} ->
                    conn |> send_resp(401, "Unauthorized") |> halt()
                {:ok, nil} ->
                    conn |> send_resp(401, "Unauthorized") |> halt()
                {:ok, token} ->
                    if token === g_token do
                        conn
                    else
                        conn |> send_resp(401, "Unauthorized") |> halt()
                    end
                end
            end
        _ ->
            conn |> send_resp(401, "Unauthorized") |> halt()
        end
    end
end