defmodule HeliosServer.Core.GlobalCache do
  alias HeliosServer.Schema
  alias HeliosServer.Uploader.Avatar

  def get_user(user_id) do
    {_, key} = FastGlobal.new("#{user_id}")
    case FastGlobal.get(key) do
    nil ->
        with {:ok, user} <- Schema.get_user(user_id) do
            FastGlobal.put(key, %{
                username: user.username,
                avatar: user.avatar,
                status: user.status,
                userhash: user.userhash,
            })

            %{
                username: user.username,
                avatar: user_avatar_url(user.avatar, user_id),
                status: user.status,
                userhash: user.userhash,
            }
        else
        _ ->
            FastGlobal.put(key, "unknown / deleted")
            %{
                username: "unknown / deleted",
                avatar: Avatar.url(nil),
                status: "",
                userhash: "0000",
            }
        end
    user ->
        %{
            username: user.username,
            userhash: user.userhash,
            avatar: user_avatar_url(user.avatar, user_id),
            status: user.status,
        }
    end
  end

  def update_user(_user_id) do
    :ok
  end

  defp user_avatar_url(avatar, _) when is_nil(avatar) or avatar == "" do Avatar.url(nil) end
  defp user_avatar_url(avatar, user_id) do Avatar.url({avatar, %{id: user_id}}) end
end