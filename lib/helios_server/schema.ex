defmodule HeliosServer.Schema do
  @moduledoc """
  The Schema context.
  """

  alias HeliosServer.Schema.{User, UserByEmail, UserCred, Server, ServerMember, ServerByMember, ServerByOwner, DirectMessage, ChannelMessage, Channel, ServerInvite}
  alias HeliosServer.Core
  import Triton.Query

  ####
  #### Note all ids must be strings as JavaScript Big Ints overflow
  ####  

  @doc """
  Gets a single user.

  Returns an error message if the User does not exist.

  ## Examples

      iex> get_user(123)
      {:ok, %User{}}


      iex> get_user(456)
      {:ok, nil} or {:error, "Message"}
  
  """
  def get_user(id) when is_integer(id) do
    with {:ok, raw_user} <- User
    |> prepared(id: id)
    |> select(:all)
    |> where(id: :id)
    |> User.one do
      if raw_user != nil do
      {:ok, struct(User, raw_user)}
      else
        {:error, "User does not exist"}
      end
    else
      {:error, msg} ->
        {:error, msg}
    end
  end

  def get_user(id) do
    with {:ok, raw_user} <- User
    |> prepared(id: String.to_integer(id))
    |> select(:all)
    |> where(id: :id)
    |> User.one do
      if raw_user != nil do
      {:ok, struct(User, raw_user)}
      else
        {:error, "User does not exist"}
      end
    else
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, msg}

  """
  def create_user(attrs \\ %{}) do
    # do uniqueness checks here (for email)
    case get_by_email(attrs.email) do
      {:ok, :not_found} ->
        # returns {:ok. :success} or {:error, "error message"}
        user_hash = Core.randomizer(4, :numeric)
        # do uniqueness checks here (for username + userhash)
        case get_by_username_and_userhash(attrs.username, user_hash) do
          {:ok, :not_found} ->
            hashed_pass = Comeonin.Bcrypt.hashpwsalt(attrs.password)
            now_time = DateTime.utc_now
            case Snowflake.next_id() do
              {:ok, id} ->

                with {:ok, :success} <- User
                |> prepared(id: id, email: attrs.email, password: hashed_pass, username: attrs.username, userhash: user_hash, avatar: "", updated: now_time, created: now_time)
                |> insert(id: :id, email: :email, password: :password, username: :username, userhash: :userhash, avatar: :avatar, updated: :updated, created: :created)
                |> if_not_exists
                |> User.save, 

                {:ok, :success} <- UserCred
                |> prepared(username: String.downcase(attrs.username), userhash: user_hash, user_id: id)
                |> insert(username: :username, userhash: :userhash, user_id: :user_id)
                |> if_not_exists
                |> UserCred.save do

                  created_user = %User{
                    id: id,
                    email: attrs.email,
                    username: attrs.username,
                    userhash: user_hash,
                    avatar: "",
                    status: ""
                  }
                  {:ok, created_user}
                else
                  {:error, msg} ->
                    {:error, msg}
                end
              {:error, msg} ->
                {:error, msg}
            end
          {:ok, _user} ->
            # if we have a username with the same hash, try again until the combo
            # is unique 
            create_user(attrs)
          {:error, msg} ->
            {:error, msg}
        end
      {:ok, _user} ->
        {:error, "User Exists"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, nil}

  """
  def update_user(%User{} = user, %{"status" => status}) do
    ### TO update email
    ### Select old values, nullify old row
    ### insert new row with previous id etc
    ### To update usercred, delete old, add new
    # if updating username or userhash
    # update UserCred
    with {:ok, :success} <- User
    |> update(status: status)
    |> where(id: user.id)
    |> User.save do
      created_user = %User{
        id: user.id,
        email: user.email,
        username: user.username,
        userhash: user.userhash,
        avatar: user.avatar,
        status: status
      }
      {:ok, created_user}
    else
      {:ok, nil} ->
        {:error, "User does not exist"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Updates a user's avatar.

  ## Examples

      iex> update_user(user, "avatar.png")
      :ok

      iex> update_user(user, "abx")
      {:error, msg}

  """
  def update_user_avatar(%User{} = user, avatar) do
    with {:ok, :success} <- User
    |> update(avatar: avatar)
    |> where(id: user.id)
    |> User.save do
      :ok
    else
      {:ok, nil} ->
        {:error, "User does not exist"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, :success}

      iex> delete_user(user)
      {:error, msg}

  """
  def delete_user(%User{} = user) do
    User
    |> prepared(id: String.to_integer(user.id))
    |> delete(:all)  # here :all refers to all fields
    |> where(id: :id)
    |> User.del
  end


  @doc """
  Gets a user by their email address.

  ## Examples

      iex> get_by_email(email)
      {:ok, %User{}}

      iex> get_by_email(email)
      {:error, msg}

  """
  def get_by_email(email) do
    case UserByEmail |> select(:all) |> where(email: email) |> User.one do
      {:ok, nil} ->
        {:error, :not_found}
      {:ok, user} ->
        {:ok, user}
      {:error, msg} ->
        {:error, msg}
    end
  end


  @doc """
  Gets a user by their username and userhash.

  ## Examples

      iex> get_by_username_and_userhash(username, userhash)
      {:ok, %User{}}

      iex> get_by_username_and_userhash(username, userhash)
      {:error, msg}

  """
  def get_by_username_and_userhash(username, userhash) do
    case UserCred |> select(:all) |> where(username: String.downcase(username), userhash: userhash) |> User.one do
      {:ok, nil} ->
        {:ok, :not_found}
      {:ok, user} ->
        {:ok, user}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Creates a server.

  ## Examples

      iex> create_server(%{field: value})
      {:ok, %Server{}}

      iex> create_server(%{field: bad_value})
      {:error, msg}

  """
  def create_server(%{"name" => name, "description" => description} \\ %{}, owner_id) do
    now_time = DateTime.utc_now
    oid = String.to_integer(owner_id)
    case Snowflake.next_id() do
      {:ok, id} ->
        with {:ok, :success} <- Server
        |> prepared(server_id: id, name: name, description: description, icon: "", banner: "", owner_id: oid, updated: now_time, created: now_time)
        |> insert(server_id: :server_id, name: :name, description: :description, icon: :icon, banner: :banner, owner_id: :owner_id, updated: :updated, created: :created)
        |> if_not_exists
        |> Server.save do
          created_server = %Server{
            server_id: id,
            name: name,
            icon: "",
            banner: "",
            description: description,
            owner_id: owner_id,
          }
          {:ok, created_server}
      else
        {:error, msg} ->
          {:error, msg}
      end
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Updates a server.

  ## Examples

      iex> update_server(server, updates)
      {:ok, updated_server}

      iex> update_server(server, updates)
      {:error, msg} # msg is a string
  """
  def update_server(%Server{} = server, %{"name" => name, "description" => description}) do
    # never update server_id or owner_id
    with {:ok, :success} <- Server
    |> update(name: name, description: description)
    |> where(server_id: server.server_id)
    |> Server.save do
      updated_server = %Server{
        server_id: server.server_id,
        name: name,
        icon: server.icon,
        banner: server.banner,
        description: description,
        owner_id: server.owner_id,
      }
      {:ok, updated_server}
    else
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
    update a servers banner

    ## Examples

      iex> update_server_banner(user, "avatar.png")
      :ok

      iex> update_server_banner(user, "abx")
      {:error, msg}

  """
  def update_server_banner(%Server{} = server, banner) do
    with {:ok, :success} <- Server
    |> update(banner: banner)
    |> where(server_id: server.server_id)
    |> Server.save do
      :ok
    else
      {:ok, nil} ->
        {:error, "Server does not exist"}
      {:error, msg} ->
        {:error, msg}
    end
  end


  @doc """
    update a servers icon

    ## Examples

      iex> update_server_banner(user, "avatar.png")
      :ok

      iex> update_server_banner(user, "abx")
      {:error, msg}

  """
  def update_server_icon(%Server{} = server, icon) do
    with {:ok, :success} <- Server
    |> update(icon: icon)
    |> where(server_id: server.server_id)
    |> Server.save do
      :ok
    else
      {:ok, nil} ->
        {:error, "Server does not exist"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Deletes a server.

  ## Examples

      iex> delete_server(server_id)
      {:ok, :success}

      iex> delete_server(server_id)
      {:error, msg} # msg is a string

  """
  def delete_server(server_id) do
    srv_id = String.to_integer(server_id)
    Server
    |> prepared(server_id: srv_id)
    |> delete(:all)  # here :all refers to all fields
    |> where(server_id: :server_id)
    |> Server.del
    # Delete server invites
    ServerInvite
    |> prepared(server_id: srv_id)
    |> delete(:all)
    |> where(server_id: :server_id)
    |> ServerInvite.del
    # Delete server members
    ServerMember
    |> prepared(server_id: srv_id)
    |> delete(:all)
    |> where(server_id: :server_id)
    |> ServerMember.all
    channels = get_server_channels(server_id)
    Enum.each(channels, fn channel ->
      delete_channel(srv_id, channel.channel_id)
    end)
    {:ok, :success}
  end


  @doc """
  Gets a single server.

  Returns {:error, msg} if the Server does not exist.

  ## Examples

      iex> get_server(123)
      %Server{}

      iex> get_server(456)
      ** {:error, msg}

  """
  def get_server(server_id) do
    case Server
    |> prepared(server_id: String.to_integer(server_id))
    |> select(:all)
    |> where(server_id: :server_id)
    |> Server.one do
    {:ok, nil} ->
      {:error, :not_found}
    {:ok, raw_server} ->
      {:ok, struct(Server, raw_server)}
    {:error, msg} ->
      {:error, msg}
    end
  end

  @doc """
  Gets the member list of single server.

  Returns {:error, msg} if the Server does not exist.

  ## Examples

      iex> get_server(123)
      %Server{}

      iex> get_server(456)
      ** {:error, msg}

  """
  def get_server_members(server_id) do
    with {:ok, server} <- get_server(server_id) do
      case ServerMember
      |> prepared(server_id: String.to_integer(server_id))
      |> select(:all)
      |> where(server_id: :server_id)
      |> ServerMember.all do
      {:ok, nil} ->
        {:ok, [%{user_id: server.owner_id}]}
      {:ok, members} ->
        {:ok, [%{user_id: server.owner_id} | members]}
      {:error, msg} ->
        {:error, msg}
      end
    end
  end

  @doc """
  Gets a list of servers a user belongs to.

  Return {:error, msg} if the Server does not exist.

  ## Examples

      iex> get_server(123)
      [%Server{}]

      iex> get_server(456)
      ** {:error, msg}

  """
  def get_user_servers(user_id) do
    case ServerByMember
    |> prepared(user_id: String.to_integer(user_id))
    |> select(:all)
    |> where(user_id: :user_id)
    |> ServerByMember.all do
    {:ok, nil} ->
      {:ok, []}
    {:ok, []} ->
      {:ok, []}
    {:ok, servers} ->
      server_ids = Enum.map(servers, &(&1.server_id))
      case Server
      |> select(:all)
      |> where(server_id: [in: server_ids])
      |> Server.all do
      {:ok, nil} ->
        {:ok, []}
      {:ok, servers_objs} ->
        {:ok, servers_objs}
      {:error, msg} ->
        {:error, msg}
      end
    {:error, msg} ->
      {:error, msg}
    end
  end

    @doc """
  Gets a list of servers a user owns

  Return {:error, msg} if the Servers do not exist.

  ## Examples

      iex> get_owned_servers(123)
      [%Server{}]

      iex> get_owned_servers(456)
      ** {:error, msg}

  """
  def get_owned_servers(user_id) do
    # ServerByOwner
    case ServerByOwner
    |> prepared(owner_id: String.to_integer(user_id))
    |> select(:all)
    |> where(owner_id: :owner_id)
    |> ServerByOwner.all do
    {:ok, nil} ->
      {:ok, []}
    {:ok, servers} ->
      {:ok, servers}
    {:error, msg} ->
      {:error, msg}
    end
  end

  @doc """
  Adds a user to the membership of a server

  Returns {:error, msg} where 'msg' is a string if anything goes wring

  ## Examples

      iex> join_server(%{1,2},3)
      {:ok, :success}

      iex> join_server(%{0,0},0)
      ** {:error, msg}

  """
  def join_server(%{"server_id" => server_id, "invite_id" => _invite_id}, user_id) do
    now_time = DateTime.utc_now
    server_id_int = String.to_integer(server_id)
    with {:ok, server} <- get_server(server_id) do
      # if owner tries to join their server, just return the server
      if server.owner_id != String.to_integer(user_id) do
        with {:ok, :success} <- ServerMember
          |> prepared(server_id: server_id_int, user_id: String.to_integer(user_id), created: now_time)
          |> insert(server_id: :server_id, user_id: :user_id, created: :created)
          |> if_not_exists
          |> ServerMember.save do
          {:ok, server}
        else
          {:error, "Your operation was not applied."} ->
            {:ok, server}
        end
      else
        {:ok, server}
      end
    end
  end

  @doc """
  Removes a user from the membership of a server

  Returns {:error, msg} where 'msg' is a string if anything goes wring

  ## Examples

      iex> leave_server(1,2)
      {:ok, :success}

      iex> leave_server(0,0)
      ** {:error, msg}

  """
  def leave_server(user_id, server_id) do
    server_id_int = String.to_integer(server_id)
    user_id_int = String.to_integer(user_id)
    with {:ok, server} <- get_server(server_id) do
      if server.owner_id != String.to_integer(user_id) do
        with {:ok, _} <- ServerMember
        |> prepared(server_id: server_id_int, user_id: user_id_int)
        |> delete(:all)  # here :all refers to all fields
        |> where(server_id: :server_id, user_id: :user_id)
        |> ServerMember.del do
          :ok
        end
      else
        :ok
      end
    end
  end

  @doc """
  Creates a channel in a server.

  ## Examples

      iex> create_channel(123, 456, "channel name")
      {:ok, %Channel}

      iex> create_channel(nil, nil, nil)
      {:error, msg} # msg is a string

  """
  def create_channel(%{"server_id" => server_id, "name" => name, "description" => description} \\ %{}) do
    case Snowflake.next_id() do
      {:ok, id} ->
        with {:ok, :success} <- Channel
        |> prepared(channel_id: id, server_id: String.to_integer(server_id), name: name, description: description)
        |> insert(channel_id: :channel_id, server_id: :server_id, name: :name, description: :description)
        |> if_not_exists
        |> Channel.save do
          created_channel = %Channel{
            server_id: server_id,
            channel_id: id,
            name: name,
            description: description,
          }
          {:ok, created_channel}
      else
        {:error, msg} ->
          {:error, msg}
      end
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Gets a channel by its ID.

  ## Examples

      iex> get_channel(123, 123)
      {:ok, %Channel}

      iex> create_channel(nil, nil)
      {:error, msg} # msg is a string

  """
  def get_channel(server_id, channel_id) do
    case Channel
    |> prepared(server_id: String.to_integer(server_id), channel_id: String.to_integer(channel_id))
    |> select(:all)
    |> where(server_id: :server_id, channel_id: :channel_id)
    |> Channel.one do
    {:ok, nil} ->
      {:error, :not_found}
    {:ok, raw_channel} ->
      {:ok, struct(Channel, raw_channel)}
    {:error, msg} ->
      {:error, msg}
    end
  end

  @doc """
  Get all the channels for a server.

  ## Examples

      iex> get_server_channels(123)
      {:ok, [%Channel]}

      iex> get_server_channels(nil)
      {:error, msg} # msg is a string

  """
  def get_server_channels(server_id) do
    case Channel
    |> prepared(server_id: String.to_integer(server_id))
    |> select(:all)
    |> where(server_id: :server_id)
    |> Channel.all do
    {:ok, nil} ->
      {:ok, []}
    {:ok, channels} ->
      {:ok, channels}
    {:error, msg} ->
      {:error, msg}
    end
  end

  @doc """
  Updates a channel

  ## Examples

      iex> update_channel(%Channel{}, updates)
      {:ok, :success}

      iex> update_channel(bad_channel, bad_updates)
      {:error, msg} # msg is a string

  """
  def update_channel(%Channel{} = channel, %{ "name" => name, "description" => description}) do
    # never update server_id
    with {:ok, :success} <- Channel
    |> update(name: name, description: description)
    |> where(server_id: channel.server_id, channel_id: channel.channel_id)
    |> Channel.save do
      updated_channel = %Channel{
        channel_id: channel.channel_id,
        server_id: channel.server_id,
        name: name,
        description: description,
      }
      {:ok, updated_channel}
    else
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Deletes a channel.

  ## Examples

      iex> delete_channel(123, 456)
      {:ok, :success}

      iex> delete_channel(999, 456)
      {:error, msg} # msg is a string

  """
  def delete_channel(server_id, channel_id) when is_integer(server_id) and is_integer(channel_id) do
    Channel
    |> prepared(server_id: server_id, channel_id: channel_id)
    |> delete(:all)  # here :all refers to all fields
    |> where(server_id: :server_id, channel_id: :channel_id)
    |> Channel.del
    # Delete all the channel messages, this might be dangerous
    # millions of messages etc.
    ChannelMessage
    |> prepared(channel_id: channel_id)
    |> delete(:all)
    |> where(channel_id: :channel_id)
    |> ChannelMessage.del
    {:ok, :success}
  end

  def delete_channel(server_id, channel_id) do
    ch_id_int = String.to_integer(channel_id)
    Channel
    |> prepared(server_id: String.to_integer(server_id), channel_id: ch_id_int)
    |> delete(:all)  # here :all refers to all fields
    |> where(server_id: :server_id, channel_id: :channel_id)
    |> Channel.del
    # Delete all the channel messages, this might be dangerous
    # millions of messages etc.
    ChannelMessage
    |> prepared(channel_id: ch_id_int)
    |> delete(:all)
    |> where(channel_id: :channel_id)
    |> ChannelMessage.del
    {:ok, :success}
  end

  @max_messages 25
  @max_buckets 10

  @doc """
  Gets the last @max_messages of a channel, using the channel_id

  ## Examples

      iex> get_channel_messages(1)
      {:ok, [%ChannelMessage{}]}

      iex> get_channel_messages(0)
      {:error, msg} # msg is a string

  """
  def get_channel_messages(channel_id) do
    # To query for recent messages in the channel we generate a bucket range from current time to channel_id i.e
    with {:ok, current_time} <- Snowflake.next_id() do # Generate current time
      start_bucket = Snowflake.Util.bucket(@max_buckets, :days, String.to_integer(channel_id))
      end_bucket = Snowflake.Util.bucket(@max_buckets, :days, current_time)
      get_channel_messages_(String.to_integer(channel_id), current_time, start_bucket, end_bucket, end_bucket, [])
    end
  end


  @doc """
  Gets the last @max_messages of a channel from a certain message, using the channel_id and message_id

  ## Examples

      iex> get_channel_messages(1,2)
      {:ok, [%ChannelMessage{}]}

      iex> get_channel_messages(0,0)
      {:error, msg} # msg is a string

  """
  def get_channel_messages(channel_id, message_id) do
    # To query for recent messages in the channel we generate a bucket range from current time to channel_id i.e
    start_bucket = Snowflake.Util.bucket(@max_buckets, :days, String.to_integer(channel_id))
    end_bucket = Snowflake.Util.bucket(@max_buckets, :days, String.to_integer(message_id))
    get_channel_messages_(String.to_integer(channel_id), String.to_integer(message_id), start_bucket, end_bucket, end_bucket, [])
  end


  defp get_channel_messages_(channel_id, end_id, start_bucket, end_bucket, current_bucket, acc) do
    if current_bucket < start_bucket do
      {:ok, acc}
    else
      if length(acc) >= @max_messages do
        {:ok, acc}
      else
        case ChannelMessage
        |> prepared(channel_id: channel_id, bucket: current_bucket, end_id: end_id)
        |> select(:all)
        |> where(channel_id: :channel_id, bucket: :bucket, message_id: [<: :end_id])
        |> limit(@max_messages)
        |> ChannelMessage.all do
        {:ok, nil} ->
          {:ok, acc}
        {:ok, messages} ->
          get_channel_messages_(channel_id, end_id, start_bucket, end_bucket, current_bucket - 1, acc ++ messages)
        {:error, msg} ->
          {:error, msg}
        end
      end
    end
  end

  @doc """
  Gets the last @max_messages of a direct message chain, using the channel_id

  ## Examples

      iex> get_direct_messages(1)
      {:ok, [%DirectMessage{}]}

      iex> get_direct_messages(0)
      {:error, msg} # msg is a string

  """
  def get_direct_messages(_channel_id) do
    # Get bucket int by doing
    # {:ok, channel_id} = Snowflake.next_id() # Done way before hand (e.g when creating a channel)
    # bucket = Snowflake.Util.bucket(10, :days, channel_id) # assuming our bucket is 10 days
    # {:ok, message_id} = Snowflake.next_id()
    # NOTE: we should already have the channel id, and use that to generate the bucket int
    # To query for recent messages in the channel we generate a bucket range from current time to channel_id i.e
    # {:ok, current_time} = Snowflake.next_id() # Generate current time
    # start_bucket = Snowflake.Util.bucket(10, :days, channel_id)
    # end_bucket = Snowflake.Util.bucket(10, :days, end_time)
    # max_messages = 25
    # Enum.each(end_bucket..start_bucket, fn(x) ->
    #   Query Cassandra until we are either out of
    #   buckets or we get at least 25 messages
    #   NOTE, this is inclusive so first idx is start_bucket and last is end_bucket
    # "SELECT * FROM channel_messages WHERE channel_id = '#{channel_id}' AND bucket = #{x} LIMIT #{message_max};"
    # end)
    nil
  end


  
  @doc """
  Get all the invites for a server

  ## Examples

      iex> get_server_invites(1)
      {:ok, [%DirectMessage{}, ....]}

      iex> get_server_invites(0)
      {:error, msg} # msg is a string

  """
  def get_server_invites(server_id) do
    case ServerInvite
      |> prepared(server_id: server_id)
      |> select(:all)
      |> where(server_id: :server_id)
      |> ServerInvite.all do
      {:ok, nil} ->
        {:ok, []}
      {:ok, invites} ->
        {:ok, invites}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Gets a server invite using its ID

  ## Examples

      iex> get_server_invite(1,1)
      {:ok, %ServerInvite{}}

      iex> get_server_invite(0,0)
      {:error, msg} # msg is a string

  """
  def get_server_invite(server_id, invite_id) do
    case ServerInvite
    |> prepared(invite_id: invite_id, server_id: server_id)
    |> select(:all)
    |> where(invite_id: :invite_id, server_id: :server_id)
    |> ServerInvite.one do
    {:ok, nil} ->
      {:error, :not_found}
    {:ok, invite} ->
      {:ok, struct(ServerInvite, invite)}
    {:error, msg} ->
      {:error, msg}
    end
  end

  @doc """
  Create a server invite using invite params

  ## Examples

      iex> create_server_invite(1)
      {:ok, %ServerInvite{}}

      iex> create_server_invite(0)
      {:error, msg} # msg is a string

  """
  def create_server_invite(%{"server_id" => server_id, "duration" => duration} \\ %{}) do
    now_time = DateTime.utc_now
    sid = String.to_integer(server_id)
    case Snowflake.next_id() do
      {:ok, id} ->
        with {:ok, :success} <- ServerInvite
        |> prepared(server_id: sid, invite_id: id, join_count: 0, duration: duration, updated: now_time, created: now_time)
        |> insert(server_id: :server_id, invite_id: :invite_id, join_count: :join_count, duration: duration, updated: :updated, created: :created)
        |> if_not_exists
        |> ServerInvite.save do
          created_invite = %ServerInvite{
            server_id: sid,
            invite_id: id,
            join_count: 0,
            duration: duration,
            created: now_time,
          }
          {:ok, created_invite}
      else
        {:error, msg} ->
          {:error, msg}
      end
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Update a server invite

  ## Examples

      iex> update_server_invite(%ServerInvite{}, updates)
      {:ok, %ServerInvite{}}

      iex> update_server_invite(0)
      {:error, msg} # msg is a string

  """
  def update_server_invite(%ServerInvite{} = server_invite, %{"duration" => duration}) do
    with {:ok, invite} <- ServerInvite
    |> update(duration: duration)
    |> where(invite_id: server_invite.invite_id)
    |> ServerInvite.save do
      {:ok, struct(ServerInvite, invite)}
    else
      {:ok, nil} ->
        {:error, "ServerInvite does not exist"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @doc """
  Delete a server invite using the invite_id

  ## Examples

      iex> delete_server_invite(1, 1)
      {:ok}

      iex> delete_server_invite(0, 0)
      {:error, msg} # msg is a string

  """
  def delete_server_invite(server_id, invite_id) do
    ServerInvite
    |> prepared(server_id: String.to_integer(server_id), invite_id: String.to_integer(invite_id))
    |> delete(:all)
    |> where(server_id: :server_id, invite_id: :invite_id)
    |> ServerInvite.del
  end
end
