# Elixir Server and React 16 UI

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Dependencies
- [Redis](https://redis.io/topics/quickstart)
- [Cassandra](https://cassandra.apache.org/download/)
  - Must install `Java 8`, not a version higher. [Example](https://askubuntu.com/questions/1133216/downgrading-java-11-to-java-8)
  - Then run `sudo service cassandra stop` and `sudo service cassandra start`
  - Make sure `cassandra` owns the appropriate directories
    - `sudo chown -R cassandra:cassandra /var/lib/cassandra`
- PostgreSQL
- If we have multiple nodes, we have to keep them in sync with [NTPD](https://en.wikipedia.org/wiki/Ntpd) for **snowflakes**


## Redis
- Redis does not support nested objects
- But you can just flatten an object and use dots for the keys
- e.g.
```json
{
  "obj": [
    "val":{
      "text": "Car"
    }
  ]
}
```
to
```
"obj.0.text": "Car"
```

## UI and settings
Alot of the settings are stored in the browsers local storage e.g
- Collapsed categories
- Collapsed channels
- Collapsed servers
- Favorite emojis
- User Layout
- Notification Settings
- User server folders

Alot of persistent stuff is stored in Local storage e.g
- Access Token
- User ID
- Last change Log (assume this is to display `x new messages`)
- Last change Log date
- Search History
- Seen Tutorials

## URL format
for all
- `/channels/<server:id>/<room:id>`
for user
- `/channels/@me/<dm_user:id>`
- all these id's are 20 character UUID`s

## Server Architecture
```
                              |--------->[Text Chat]
                              |--------->[Voice Chat]
[Client]--->[Gateway Server]------
                              |--------->[User Presence]
                              |--------->[Chat Room Information]
                              |--------->[Chat Channel Information]
```

## Debugging
- An **extremely powerful GUI** for getting a topology of our App, it is built in.
```sh
iex -S mix # for interative
iex(1)> :observer.start() # to start a gui presentation of your application
```
- Even more tools listed [Here](https://elixir-lang.org/getting-started/debugging.html#other-tools-and-community)

## Testing with coveralls
- To run the tests with coverage
```sh
# do
MIX_ENV=test mix do coveralls.json
```

- To view the coverage in a web browser run the following:
```sh
# do
MIX_ENV=test mix coveralls.html && open cover/excoveralls.html
```

- Look into CI [Here](https://github.com/dwyl/phoenix-chat-example#continuous-integration)

## Documentation
- To generate docs for codebase do
```sh
mix docs
```

## Deployment
- Guide [Here](https://github.com/dwyl/phoenix-chat-example#deployment)
- [Here Too](https://elixir-lang.org/getting-started/mix-otp/config-and-releases.html#releases)

### Distributed Mode
- Elixir is location transparent, to run in distributed mode we need to do some setup
  - see [here](https://learnyousomeerlang.com/distribunomicon)
  - and [here](http://erlang.org/doc/reference_manual/distributed.html)
- In order to run distributed code, we need to start the VM with a name. The name can be short (when in the same network) or long (requires the full computer address). Let�s start a new IEx session:
```sh
$ iex --sname foo
```
- You can spawn processes on foreign nodes! and elixir automatically takes care of things like routing