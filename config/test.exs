use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :triton,
  clusters: [
    [
      conn: Triton.Conn,
      nodes: ["127.0.0.1"],
      pool: Xandra.Cluster,
      underlying_pool: DBConnection.Poolboy,
      pool_size: 10,
      keyspace: "helios_test#{System.get_env("MIX_TEST_PARTITION")}",
      health_check_delay: 2500,  # optional: (default is 5000)
      health_check_interval: 500  # optional: (default is 1000)
    ]
  ]

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :helios_server, HeliosServerWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
