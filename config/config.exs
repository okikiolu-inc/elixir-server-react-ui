# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# config :helios_server,
#   ecto_repos: [HeliosServer.Repo]

# Configures the endpoint
config :helios_server, HeliosServerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "x3Oc7vP+pAJuKKQqlX6GVoTMS1qtW94WJPtpu7LHc26EjkpTvR7GzaOY028sD6s+",
  render_errors: [view: HeliosServerWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: HeliosServer.PubSub,
  live_view: [signing_salt: "635xqJqi"]

config :helios_server, HeliosServerWeb.Auth.Guardian,
  issuer: "helios_server",
  secret_key: "IXlfbR4Ej2jblqXIUy9Djbldc/JuItcLTx/tfHbP9QIvgQDmknglj8FYqyDQ4Yfp"

config :snowflake,
  # nodes: ["127.0.0.1", :'nonode@nohost'],   # up to 1023 nodes
  nodes: ["127.0.0.1"],   # up to 1023 nodes
  epoch: 1142974214000  # NEVER FUCKING CHANGE THIS, IT WILL DESTROY YOUR DB

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# manifold config
# config :manifold, gen_module: MyGenModule

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
