// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket,
// and connect at the socket path in "lib/web/endpoint.ex".
//
// Pass the token on params as below. Or remove it
// from the params if you are not using authentication.
import { Socket, Channel } from "phoenix";
import store from '../store';

let socket: Socket | null;
let channels: { [key: string]: Channel } = {};
let requestQueue: { (): void; }[] = [];

let _onError: () => void;
let _onClose: () => void;
let _onOpen: () => void;

interface InitParams {
    user_id: string,
    token: string,
}

function logout() {
    if (socket != null) {
        socket.disconnect(() => {
            socket = null;
            _onError = null;
            _onClose = null;
            _onOpen = null;
        });
    }
};

export function socketLogout() {
    logout();
};

export function onError(cb: () => void | null) {
    _onError = cb;
};

export function onClose(cb: () => void | null) {
    _onClose = cb;
};

export function onOpen(cb: () => void | null) {
    _onOpen = cb;
};

export function init(params: InitParams) {
    socket = new Socket("/socket", {
        params,
    });

    socket.onError(() => {
        if (_onError != null) _onError();
        console.info("the socket had an error");
        logout();
    });

    socket.onClose(() => {
        if (_onClose != null) _onClose();
        console.info("the socket was closed");
        // logout();
    });

    socket.onOpen(() => {
        if (_onOpen != null) _onOpen();
        // execute any connection attempts
        requestQueue.map((func) => {
            func();
        });
    });
};

export function connect() {
    if (socket != null) {
        socket.connect();
    }
    else {
        throw new Error('Did you call init()? the socket does not exist');
    }
};

export function disconnect() {
    if (socket != null) {
        socket.disconnect();
    }
    else {
        throw new Error('Did you call init()? the socket does not exist');
    }
};

export function joinChannel(
    server_id: string,
    channel_id: string,
    onSuccess?: (response: any) => void,
    onError?: (response: any) => void,
) {
    const state = store.getState();
    const userid: string = state.User.userid;

    if (socket == null || socket.isConnected() != true) {
        requestQueue.push(
            () => {
                joinChannel(server_id, channel_id, onSuccess, onError);
            }
        );
        // throw new Error('Did you call init()? the socket does not exist');
        return;
    }
    
    if (channels[`${server_id}:${channel_id}`] == null) {
        let channel = socket.channel(`server:${server_id}:${channel_id}`, {
            user_id: userid,
        });
        channels[`${server_id}:${channel_id}`] = channel;
        channels[`${server_id}:${channel_id}`].join()
        .receive("ok", (resp) => {
            if (onSuccess != null) {
                onSuccess(resp);
            }
        })
        .receive("error", (resp) => {
            if (onError != null) {
                onError(resp);
            }
        });
    }
};

export interface SocketMessage {
    user_id: string,
    username: string,
    userhash: string,
    server_id: string,
    channel_id: string,
    avatar: string,
    content: string,
}

export interface SocketMessageResponse {
    now_time: string,
    avatar: string,
    channel_id: string,
    content: string,
    server_id: string,
    username: string,
    userhash: string,
    message_id: string,
}

export function sendMessage(message: SocketMessage) {
    if (channels[`${message.server_id}:${message.channel_id}`] != null) {
        const chan = channels[`${message.server_id}:${message.channel_id}`];
        chan.push(
            `message:${message.server_id}:${message.channel_id}`,
            message,
        );
    }
};


export function onChannelReceive(
    server_id: string,
    channel_id: string,
    event: string,
    callback: (payload: any) => void
) {
    if (channels[`${server_id}:${channel_id}`] != null) {
        channels[`${server_id}:${channel_id}`].off(`${event}`);
        channels[`${server_id}:${channel_id}`].on(`${event}`, callback);
    }
};
