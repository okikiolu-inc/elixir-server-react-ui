import React, { useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import * as SocketConnection from './socket';
// import constants from './constants';

import MainSideNav from './components/MainSideNav';
import ProtectedRoute from './components/auth/ProtectedRoute';
import Home from './pages/Home';
import Auth from './pages/Auth';
import ServerHome from './pages/ServerHome';
import ServerSettings from './pages/ServerSettings';
import JoinServer from './pages/JoinServer';
import NotFound from './pages/NotFound';

import { UserState, getUser } from './reducers/User';

interface RootState {
    User: UserState,
};

const mapState = (state: RootState) => ({
    userid: state.User.userid,
    usertoken: state.User.usertoken,
});

const mapDispatch = {
    getUser,
}


const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
}

const Root: React.FC<Props> = ({
    userid,
    usertoken,
    getUser,
}) => {
    useEffect(() => {
        if (userid != null && userid.trim() !== ''
            || usertoken != null && usertoken.trim() !== '') {
            getUser(userid);

            SocketConnection.init({
                user_id: userid,
                token: usertoken,
            });
            SocketConnection.onOpen(() => {
            });
            SocketConnection.onError(() => {
                // localStorage.removeItem(constants.LOCAL_STORAGE_USER);
                // store.dispatch(logoutSuccess());
            });
            SocketConnection.onClose(() => {
                // localStorage.removeItem(constants.LOCAL_STORAGE_USER);
                // store.dispatch(logoutSuccess());
            });
            SocketConnection.connect();
            // It keeps the old socket on disconnect and tries to use the
            // the terminated socket
        }
    }, [userid, usertoken]);

    return (
        <>
            <BrowserRouter>
                <MainSideNav />
                <Switch>
                    <Route exact path="/login" component={Auth} />
                    <Route exact path="/register" component={Auth} />
                    <ProtectedRoute path="/@home/:friendID?" component={Home} />
                    <ProtectedRoute path="/@server/:serverID/:channelID?" component={ServerHome} />
                    <ProtectedRoute path="/@settings/:serverID" component={ServerSettings} />
                    <ProtectedRoute path="/@join-server/:serverID/:inviteID" component={JoinServer} />
                    <ProtectedRoute component={NotFound} />
                </Switch>
            </BrowserRouter>
        </>
    );
};

export default connector(Root);