import constants from '../constants';
import store from '../store';

export default function AuthFile(path: string, data: FormData): Promise<Response> {
    const url = `${constants.APP_URL}/${constants.APP_API_URL}/${path}`;
    const state = store.getState();
    const userid: string = state.User.userid;
    const usertoken: string = state.User.usertoken;

    if (userid == null || usertoken == null
        || userid.trim() === '' || usertoken.trim() === null) {
        return Promise.reject('Not authorized');
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${usertoken}`);
    headers.append('userid', userid);
    /**
     * DO NOT PUT CONTENT TYPE, it will fuck your day up!
     */
    const payload: RequestInit = {
        method: 'POST',
    };

    payload.headers = headers;

    payload.body = data;

    console.log('payload', payload);

    return fetch(url, payload);
};