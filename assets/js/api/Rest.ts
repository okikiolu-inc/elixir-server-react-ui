import constants from '../constants';

export default function REST(method: string, path: string, data?: any): Promise<Response> {
    const url = `${constants.APP_URL}/${constants.APP_API_URL}/${path}`;

    const headers = new Headers();

    if (data != null) {
        headers.append('Content-Type', 'application/json');
    }

    const payload: RequestInit = {
        method: method,
    };

    payload.headers = headers;

    if (data != null) {
        payload.body = JSON.stringify(data);
    }

    return fetch(url, payload);
};