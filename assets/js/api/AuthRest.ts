/*
SIGN UP EXAMPLE
===============
curl -X POST http://localhost:4000/api/v1/auth/signup -H "Content-Type: application/json" -d '{"user": {"email": "dickballs@business.com", "password": "password", "avatar": "", "userhash": "9999", "username": "ass ass" }}'

SIGN IN EXAMPLE
===============
curl -X POST http://localhost:4000/api/v1/auth/signin -H "Content-Type: application/json" -d '{"email": "user2@business.com", "password": "password"}'


GET DATA AFTER SIGN-IN
======================
curl -X GET http://localhost:4000/api/v1/users/1 -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJoZWxpb3Nfc2VydmVyIiwiZXhwIjoxNTk4ODM4MDYzLCJpYXQiOjE1OTY0MTg4NjMsImlzcyI6ImhlbGlvc19zZXJ2ZXIiLCJqdGkiOiJlNjcxYWE1ZC1jMWM3LTQxZWYtYjBiMC03MDZjMmEzMWI3MzAiLCJuYmYiOjE1OTY0MTg4NjIsInN1YiI6IjEiLCJ0eXAiOiJhY2Nlc3MifQ.4rTzLtH9Gw6T9sycrbDTgaEAPVve8gE7C8CiljTKTKyx9U3OuSvpUBGsH1tmYI4Bx-FetNpM8cp0Fhb65FPYqA"

LOGOUT
======
curl -X GET http://localhost:4000/api/v1/auth/logout -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJoZWxpb3Nfc2VydmVyIiwiZXhwIjoxNTk4ODM4MDYzLCJpYXQiOjE1OTY0MTg4NjMsImlzcyI6ImhlbGlvc19zZXJ2ZXIiLCJqdGkiOiJlNjcxYWE1ZC1jMWM3LTQxZWYtYjBiMC03MDZjMmEzMWI3MzAiLCJuYmYiOjE1OTY0MTg4NjIsInN1YiI6IjEiLCJ0eXAiOiJhY2Nlc3MifQ.4rTzLtH9Gw6T9sycrbDTgaEAPVve8gE7C8CiljTKTKyx9U3OuSvpUBGsH1tmYI4Bx-FetNpM8cp0Fhb65FPYqA"

Always add User ID and bearer to all auth headers header i.e
=================================================
"Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVC...."
"userid: 034b2fb7-ec74-4bb8-bd67-457b260d137d"

curl -X GET http://localhost:4000/api/v1/users/034b2fb7-ec74-4bb8-bd67-457b260d137d -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJoZWxpb3Nfc2VydmVyIiwiZXhwIjoxNTk4OTIwMzgyLCJpYXQiOjE1OTY1MDExODIsImlzcyI6ImhlbGlvc19zZXJ2ZXIiLCJqdGkiOiJkNjRlZThkYi1jZWQ3LTRmZDktYWI2Mi03NGIzMmM1MDc0ZmUiLCJuYmYiOjE1OTY1MDExODEsInN1YiI6IjAzNGIyZmI3LWVjNzQtNGJiOC1iZDY3LTQ1N2IyNjBkMTM3ZCIsInR5cCI6ImFjY2VzcyJ9.YLtOFZjzECe0Iu5HsHEHvK0jLUSokQvLD0_uPNxpcSKmBm1kEWsBQWv-1Zds_-2SXTbwKR5iyTJkwFZeBPiIMQ" -H "userid: 034b2fb7-ec74-4bb8-bd67-457b260d137d"

curl -X GET http://localhost:4000/api/v1/auth/logout -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJoZWxpb3Nfc2VydmVyIiwiZXhwIjoxNTk4OTIwMzgyLCJpYXQiOjE1OTY1MDExODIsImlzcyI6ImhlbGlvc19zZXJ2ZXIiLCJqdGkiOiJkNjRlZThkYi1jZWQ3LTRmZDktYWI2Mi03NGIzMmM1MDc0ZmUiLCJuYmYiOjE1OTY1MDExODEsInN1YiI6IjAzNGIyZmI3LWVjNzQtNGJiOC1iZDY3LTQ1N2IyNjBkMTM3ZCIsInR5cCI6ImFjY2VzcyJ9.YLtOFZjzECe0Iu5HsHEHvK0jLUSokQvLD0_uPNxpcSKmBm1kEWsBQWv-1Zds_-2SXTbwKR5iyTJkwFZeBPiIMQ" -H "userid: 034b2fb7-ec74-4bb8-bd67-457b260d137d"


Token is gotten from sign in result

Always check if they are logged in
If you get 401, redirect them to the login page

Store auth tokens and shit in localstorage

this.state = { languages: [], loading: true };

// Get the data from our API.
fetch('/api/languages')
    .then(response => response.json() as Promise<ApiResponse>)
    .then(data => {
        this.setState({ languages: data.data, loading: false });
    });


"avatar":null,"email":"dickballs@business.com","id":"034b2fb7-ec74-4bb8-bd67-457b260d137d","token":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJoZWxpb3Nfc2VydmVyIiwiZXhwIjoxNTk4OTE4MzY4LCJpYXQiOjE1OTY0OTkxNjgsImlzcyI6ImhlbGlvc19zZXJ2ZXIiLCJqdGkiOiJiZGI2MGE5ZC0wZWY2LTQ4ZWUtYmZjZi0yMzExNmVlOTFiZmEiLCJuYmYiOjE1OTY0OTkxNjcsInN1YiI6IjAzNGIyZmI3LWVjNzQtNGJiOC1iZDY3LTQ1N2IyNjBkMTM3ZCIsInR5cCI6ImFjY2VzcyJ9.3ZaXLFy5tqIGHoAqXWpEXAmPMC5XELPHvi20gbqiBCLxBKkZAWCUmVEfqhYyiRLTg_rl9ZjuS11k421dh9xzpQ","use

curl -X GET http://localhost:4000/api/v1/auth/logout -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJoZWxpb3Nfc2VydmVyIiwiZXhwIjoxNTk4OTE4MzY4LCJpYXQiOjE1OTY0OTkxNjgsImlzcyI6ImhlbGlvc19zZXJ2ZXIiLCJqdGkiOiJiZGI2MGE5ZC0wZWY2LTQ4ZWUtYmZjZi0yMzExNmVlOTFiZmEiLCJuYmYiOjE1OTY0OTkxNjcsInN1YiI6IjAzNGIyZmI3LWVjNzQtNGJiOC1iZDY3LTQ1N2IyNjBkMTM3ZCIsInR5cCI6ImFjY2VzcyJ9.3ZaXLFy5tqIGHoAqXWpEXAmPMC5XELPHvi20gbqiBCLxBKkZAWCUmVEfqhYyiRLTg_rl9ZjuS11k421dh9xzpQ"

curl -X POST http://localhost:4000/api/v1/auth/signin -H "Content-Type: application/json" -d '{"email": "dickballs@business.com", "password": "password"}'

{:ok, id} = Snowflake.next_id()

now_time = DateTime.to_unix(DateTime.utc_now)

User |> prepared(id: 1902984997644533760, email: "test@meail.com", password: "parseword", username: "cassandrrra", userhash: "1999", avatar: "", updated: 1596681208, created: 1596681208) |> insert(id: :id, email: :email, password: :password, username: :username, userhash: :userhash, avatar: :avatar, updated: :updated, created: :created) |> if_not_exists |> User.save

UserByUsername |> select(:all) |> where(username: "cassandrrra", userhash: "1999") |> User.one

UserCred |> prepared(username: "cassandrrra", userhash: "1999") |> insert(username: :username, userhash: :userhash) |> if_not_exists |> UserCred.save

UserCred |> select(:all) |> where(username: "cassandrrra", userhash: "1999") |> UserCred.one

dick@balls.balls
dick@borus.balls
mTvUHX32WweYzb@
*/

import constants from '../constants';
import store from '../store';

export default function AuthREST(method: string, path: string, data?: any): Promise<Response> {
    const url = `${constants.APP_URL}/${constants.APP_API_URL}/${path}`;
    const state = store.getState();
    const userid: string = state.User.userid;
    const usertoken: string = state.User.usertoken;

    if (userid == null || usertoken == null
        || userid.trim() === '' || usertoken.trim() === null) {
        return Promise.reject('Not authorized');
    }

    const headers = new Headers();
    headers.append('Authorization', `Bearer ${usertoken}`);
    headers.append('userid', userid);
    if (data != null) {
        headers.append('Content-Type', 'application/json');
    }

    const payload: RequestInit = {
        method: method,
    };

    payload.headers = headers;

    if (data != null) {
        payload.body = JSON.stringify(data);
    }

    return fetch(url, payload);
};