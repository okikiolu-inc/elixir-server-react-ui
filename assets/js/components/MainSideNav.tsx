import React, { useEffect, useState } from 'react';
import { useHistory, useLocation, matchPath } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { useForm } from 'react-hook-form';
import Popup from "reactjs-popup";

import { UserState } from '../reducers/User';
import { ServersState, createServer, fetchServers } from '../reducers/Servers';
import { ChannelsState } from '../reducers/Channels';

interface RootState {
    User: UserState,
    Servers: ServersState,
    Channels: ChannelsState,
};

const mapState = (state: RootState) => ({
    userid: state.User.userid,
    avatar: state.User.avatar,
    servers: state.Servers.servers,
    owned_servers: state.Servers.owned_servers,
    lastChannels: state.Channels.lastchannel,
});

const mapDispatch = {
    createServer,
    fetchServers,
}

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
}

type Inputs = {
    name: string,
    description: string,
};

const MainSideNav: React.FC<Props> = ({
    userid,
    avatar,
    createServer,
    fetchServers,
    servers,
    owned_servers,
    lastChannels,
}) => {

    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        if (userid != null && userid.trim() !== '') {
            fetchServers();
        }
    }, [userid]);

    const { register, handleSubmit, errors } = useForm<Inputs>();
    const onSubmit = (data: Inputs) => {
        setShowModal(false);
        createServer(data.name, data.description);
    };

    const contentStyle: React.CSSProperties = {
        borderRadius: '0.5rem',
        color: 'white',
        backgroundColor: 'rgba(26, 32, 44, 1)',
        padding: 10,
        userSelect: "none",
        width: 'fit-content',
        border: 'none',
        boxShadow: 'none',
    };
    const arrowStyle: React.CSSProperties = {
        backgroundColor: 'rgba(26, 32, 44, 1)',
        color: 'rgba(26, 32, 44, 1)',
    };

    const history = useHistory();
    const location = useLocation();
    let canEditServer = false;
    const match = matchPath<{serverID: string, channelID: string}>(location.pathname, {
        path: '/@server/:serverID/:channelID?',
        exact: false,
        strict: false,
    });
    let server_id = '';
    if (userid != null && userid.trim() != '' && match) {
        const params = match.params;
        const { serverID } = params;

        if (serverID != null) {
            server_id = serverID;
        }
        
        if (serverID != null && owned_servers[serverID] != null) {
            canEditServer = true;
        }
    }
    const pathname = location.pathname;

    return (
        <div className={`${(pathname === '/login' || pathname === '/register') ? 'hidden' : ''} w-1/12 lg:w-1/20 bg-gray-800 shadow`}>
            <Popup
                trigger={
                    <img
                        src={avatar}
                        className="h-12 w-12 rounded-full m-auto my-2 cursor-pointer hover:rounded-lg transition-all duration-100"
                        alt="Home"
                        onClick={() => {
                            history.push("/@home");
                        }}
                    />
                }
                position="right center"
                contentStyle={contentStyle}
                arrowStyle={arrowStyle}
                on="hover"
            >
                <span>Home</span>
            </Popup>
            <div className="border-solid border-t border-1 border-gray-600 w-2/3 m-auto" />
            {owned_servers && Object.keys(owned_servers).map((serverID, idx) => {
                const server = owned_servers[serverID];
                return (
                    <Popup
                        key={`server-browser-${idx}`}
                        trigger={
                            <img
                                id={`server-browser-${idx}`}
                                src={server.icon}
                                className={`h-12 w-12 rounded-full m-auto my-2 cursor-pointer hover:rounded-lg transition-all duration-100 ${server_id === server.server_id ? ' border-green-500 border-2' : ''}`}
                                alt={server.name}
                                onClick={() => {
                                    if (lastChannels && lastChannels[serverID] != null) {
                                        history.push(`/@server/${serverID}/${lastChannels[serverID]}`);
                                    }
                                    else {
                                        history.push(`/@server/${serverID}/`);                                        
                                    }
                                }}
                            />
                        }
                        position="right center"
                        contentStyle={contentStyle}
                        arrowStyle={arrowStyle}
                        on="hover"
                    >
                        <span>OWNED: {server.name}</span>
                    </Popup>
                );
            })}
            {owned_servers && Object.keys(owned_servers).length > 0 && <div className="border-solid border-t border-1 border-gray-600 w-2/3 m-auto" />}
            {servers && Object.keys(servers).map((serverID, idx) => {
                const server = servers[serverID];
                return (
                    <Popup
                    key={`server-browser-${idx}`}
                    trigger={
                            <img
                                id={`server-browser-${idx}`}
                                src={server.icon}
                            className={`h-12 w-12 rounded-full m-auto my-2 cursor-pointer hover:rounded-lg transition-all duration-100 ${server_id === server.server_id ? ' border-green-500 border-2' : ''}`}
                                alt={server.name}
                                onClick={() => {
                                    history.push(`/@server/${serverID}/`);
                                }}
                            />
                        }
                        position = "right center"
                        contentStyle={contentStyle}
                        arrowStyle={arrowStyle}
                        on = "hover"
                    >
                        <span>{server.name}</span>
                    </Popup>
                );
            })}
            {servers && Object.keys(servers).length > 0 && <div className="border-solid border-t border-1 border-gray-600 w-2/3 m-auto" />}

            <Popup
                trigger={
                    <div className="">
                        <div
                            className="bg-gray-700 h-12 w-12 rounded-full m-auto my-2 cursor-pointer flex justify-center hover:rounded-lg transition-all duration-100 text-green-500 hover:text-gray-200 hover:bg-green-500"
                            onClick={() => {
                                setShowModal(true);
                            }}
                        >
                            <svg className="h-6 w-6 self-center" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 4v16m8-8H4" />
                            </svg>
                        </div>
                        <Popup
                            contentStyle={{
                                height: '25%',
                                boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
                                backgroundColor: 'rgba(45, 55, 72, 1)',
                                border: 'none',
                                borderRadius: '0.5rem',
                            }}
                            overlayStyle={{
                                backdropFilter: 'blur(6px)',
                            }}
                            open={showModal}
                            onClose={() => {
                                setShowModal(false);
                            }}
                            modal
                            closeOnDocumentClick
                        >
                            <div className="h-full w-full select-none flex flex-col bg-gray-800 items-center justify-center">
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="mb-4">
                                        <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                                            Server Name
                                        </label>
                                        <input
                                            className={`shadow appearance-none border ${errors.name ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                            id="name"
                                            name="name"
                                            type="text"
                                            placeholder="Your Cool Server Name"
                                            defaultValue={""}
                                            ref={register({
                                                required: true,
                                                minLength: 4
                                            })}
                                            // readOnly={inflight}
                                        />
                                        {errors.name && <p className="text-red-500 text-xs italic">Please fill in a server name.</p>}
                                    </div>
                                    <div className="mb-4">
                                        <label className="block text-white text-sm font-bold mb-2" htmlFor="description">
                                            Server Description
                                        </label>
                                        <input
                                            className={`shadow appearance-none border ${errors.description ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                            id="description"
                                            name="description"
                                            type="text"
                                            placeholder="What the server about"
                                            defaultValue={""}
                                            ref={register({
                                                required: true,
                                            })}
                                            // readOnly={inflight}
                                        />
                                        {errors.description && <p className="text-red-500 text-xs italic">Please fill in a description.</p>}
                                    </div>
                                    <div className="flex items-center flex-col">
                                        <button
                                            className="bg-blue-500 hover:bg-green-700 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                            type="submit"
                                        // disabled={inflight}
                                        >
                                            Create Server
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </Popup>

                    </div>
                }
                position="right center"
                contentStyle={contentStyle}
                arrowStyle={arrowStyle}
                on="hover"
            >
                <span>New Server</span>
            </Popup>

            {canEditServer && <div className="border-solid border-t border-1 border-gray-600 w-2/3 m-auto" />}

            {canEditServer && <Popup
                trigger={
                    <div
                        className="bg-gray-700 h-12 w-12 rounded-full m-auto my-2 cursor-pointer flex justify-center hover:rounded-lg transition-all duration-100 text-green-500 hover:text-gray-200 hover:bg-green-500"
                        onClick={() => {
                            if (server_id.trim() !== '') {
                                history.push(`/@settings/${server_id}/`);
                            }
                        }}
                    >
                        <svg className="h-6 w-6 self-center" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                            <path fillRule="evenodd" d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z" clipRule="evenodd" />
                        </svg>
                    </div>
                }
                position="right center"
                contentStyle={contentStyle}
                arrowStyle={arrowStyle}
                on="hover"
            >
                <span>Server Settings</span>
            </Popup>
            }
        </div>
    );
};

export default connector(MainSideNav);