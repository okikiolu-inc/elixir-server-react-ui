import React, { useState } from 'react';
import Popup from "reactjs-popup";
import md from '../../tools/markdown-it';
import constants from '../../constants';
import { MemberInfo } from '../../reducers/Servers';

// Interface for the Counter component state
interface MessageProps {
    username: string;
    userhash: string;
    avatar: string;
    text: string;
    time: string;
    member_info: MemberInfo,
    onImageLoad: () => void,
    getUserInfo: (username:string, userhash:string) => void
}

const isImageURL = (url: string) => {
    return (url.match(/\.(jpeg|jpg|gif|png)$/) != null);
};

const Message: React.FC<MessageProps> = ({
    text,
    username,
    userhash,
    avatar,
    time,
    onImageLoad,
    getUserInfo,
    member_info,
}) => {

    const parsed_result = md.render(text);
    const match_link = md.linkify.match(text);
    let only_link = false;
    const images: JSX.Element[] = [];
    if (match_link != null) {
        // send link requests to the server to get previews
        console.log(match_link);
        match_link.map((match, idx) => {
            only_link = match.url === match.text;

            if (isImageURL(match.url)) {
                images.push(
                    <img
                        key={`${username}-${time}-${idx}`}
                        src={match.url}
                        onLoad={() => {
                            onImageLoad();
                        }}
                        alt="chat_image"
                        style={{
                            maxHeight: '30vh',
                            maxWidth: '30vw',
                        }}
                        className="mt-1"
                    />
                );
            }
        });
    }


    // today at 9:16pm, yesterday at 10:00am, 09/30/2020
    let cleaned_time = time;
    if (time != null) {
        const dateObj = new Date(time);
        const now = new Date(Date.now());
        const sameday = dateObj.getFullYear() === now.getFullYear() &&
            dateObj.getMonth() === now.getMonth() &&
            dateObj.getDate() === now.getDate();

        let min_num = dateObj.getMinutes();
        let min = `${min_num + 1}`;
        if (min_num < 10) {
            min = `0${min}`;
        }

        if (sameday) {
            cleaned_time = `Today at ${dateObj.getHours()}:${min}`;
        }
        else {
            const yesterday = dateObj.getFullYear() === now.getFullYear() &&
                dateObj.getMonth() === now.getMonth() &&
                dateObj.getDate() === now.getDate() - 1;
            if (yesterday) {
                cleaned_time = `Yesterday at ${dateObj.getHours() + 1}:${min}`;
            }
            else {
                cleaned_time = `${dateObj.getDate()}/${dateObj.getMonth() + 1}/${dateObj.getFullYear()}`
            }
        }
    }

    const contentStyle: React.CSSProperties = {
        borderRadius: '1rem',
        color: 'white',
        backgroundColor: 'rgba(74, 85, 104, 1)',
        padding: 0,
        userSelect: "none",
        width: 'fit-content',
        border: 'none',
        boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
    };
    const arrowStyle: React.CSSProperties = {
        backgroundColor: 'rgba(74, 85, 104, 1)',
        color: 'rgba(26, 32, 44, 1)',
    };

        const roles = [
            {
                name: 'Sol Invictus',
                color: 'orange'
            },
            {
                name: 'Founder',
                color: 'blue'
            },
            {
                name: 'CEO',
                color: 'yellow'
            },
            {
                name: 'Ass Lord',
                color: 'green'
            },
            {
                name: 'Monke',
                color: 'white'
            },
            {
                name: 'Pontifex Maximus',
                color: 'purple'
            },
            {
                name: 'Pontifex',
                color: 'purple'
            },
            {
                name: 'Flamen',
                color: 'purple'
            },
        ];


    return (
        <div
            className="flex flex-row pb-5 mb-3 pr-1 hover:bg-gray-900 select-none break-all"
        >
            <Popup
                trigger={
                    <img
                        src={avatar}
                        className="h-12 w-12 self-start rounded-full mx-2 mb-2 mt-1 cursor-pointer"
                        alt={`${username}-avatar`}
                    />
                }
                position="top left"
                contentStyle={contentStyle}
                arrowStyle={arrowStyle}
                onOpen={() => {
                    getUserInfo(username, userhash);
                }}
            >
                <div className="w-56 flex flex-col">
                    <div className="w-full flex flex-col items-center content-center bg-gray-900 px-4 pt-4 pb-2">
                        <div className="pb-2">
                            <img
                                src={avatar}
                                className="h-20 w-20 rounded-full"
                                alt={`${username}-avatar`}
                            />
                        </div>
                        <div className="pt-2 pb-1">
                            <span>{username}</span>
                            <span className="opacity-50">#{userhash}</span>
                        </div>
                        <div className="py-1 text-sm text-gray-500">{member_info && member_info.status}</div>
                    </div>
                    <div className="w-full flex flex-col bg-gray-700 px-4 py-2">
                        <div className="text-sm pb-2">Roles</div>
                        <div className="flex flex-wrap text-xs text-gray-400">
                            {roles.map((role, idx) => {
                                return (
                                    <div
                                        key={`roles-${idx}`}
                                        className={`border ${constants.BORDER_COLOR[role.color]} ${constants.TEXT_COLOR[role.color]} p-1 m-1 rounded-full flex items-center`}
                                    >
                                        <div
                                            className={`w-3 h-3 ${constants.PILL_COLOR[role.color]} rounded-full mr-1`} />
                                        {role.name}
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </Popup>
            <div className="flex flex-col mt-1">
                <div>
                    <span className="text-gray-200 pr-2 cursor-pointer">{username}</span>
                    <span className="text-gray-600 text-xs">{cleaned_time}</span>
                </div>
                {!only_link && <span className="text-gray-200 mt-1 chat-text" dangerouslySetInnerHTML={{
                    __html: parsed_result,
                }} />}
                {images}
            </div>
        </div>
    );
};

export default Message;