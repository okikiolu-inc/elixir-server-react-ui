import React from 'react';

const Tooltip: React.FC = ({
    children
}) => (
    <div className="bg-gray-300 rounded-lg text-grey-800">
        {children}
    </div>
);

export default Tooltip;