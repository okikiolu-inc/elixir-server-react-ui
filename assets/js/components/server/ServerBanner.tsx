import React, { useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import Popup from "reactjs-popup";

import { ServersState, leaveServer } from '../../reducers/Servers';

interface RootState {
    Servers: ServersState,
};

const mapState = (state: RootState) => ({
    servers: state.Servers.servers,
    owned_servers: state.Servers.owned_servers,
});

const mapDispatch = {
    leaveServer,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
}

const ServerBanner: React.FC<Props> = ({
    servers,
    owned_servers,
    leaveServer,
}) => {

    const [popupOpen, setPopupOpen] = useState(false);
    const [showModal, setShowModal] = useState(false);

    const { serverID } = useParams();

    const history = useHistory();

    let backgroundImageUrl: string = '';
    let bannerName: string = 'Server Name Error';

    if (serverID && (servers[serverID] || owned_servers[serverID])) {
        const server = servers[serverID] || owned_servers[serverID];
        if (server) {
            backgroundImageUrl = server.banner;
            bannerName = server.name;
        }
    }

    const contentStyle: React.CSSProperties = {
        borderRadius: '0.5rem',
        color: 'white',
        backgroundColor: 'rgba(74, 85, 104, 1)',
        padding: 5,
        userSelect: "none",
        width: 'fit-content',
        border: 'none',
        boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
    };
    const arrowStyle: React.CSSProperties = {
        backgroundColor: 'rgba(74, 85, 104, 1)',
        color: 'rgba(26, 32, 44, 1)',
    };


    return (
        <header
            className="border-solid border border-gray-900 h-32 bg-local"
            style={{
                backgroundImage: `url(${backgroundImageUrl})`,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'
            }}
        >
            <div
                className="cursor-pointer text-gray-200 py-2 px-1 flex items-center justify-between h-third bg-gray-800 shadow bg-opacity-25"
                onClick={() => {
                    setPopupOpen(!popupOpen);
                }}
            >
                <span className="text-sm select-none">{bannerName}</span>
                <Popup
                    trigger={
                        <svg className={`h-5 w-5 ${popupOpen ? 'transform rotate-180' : ''}`} xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
                        </svg>
                    }
                    position="bottom center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    open={popupOpen}
                >
                    <div
                        className="w-48 bg-gray-700 text-red-600 hover:bg-red-600 hover:text-gray-700 flex flex-row justify-between rounded-md p-1"
                        onClick={() => {
                            setPopupOpen(false);
                            setShowModal(true);
                        }}
                    >
                        <span className="text-md">Leave Server</span>
                        <svg fill="none" viewBox="0 0 24 24" stroke="currentColor" className="logout h-5 w-5"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"></path></svg>
                    </div>
                </Popup>
                <Popup
                    contentStyle={{
                        height: '25%',
                        boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
                        backgroundColor: 'rgba(45, 55, 72, 1)',
                        border: 'none',
                        borderRadius: '0.5rem',
                    }}
                    overlayStyle={{
                        backdropFilter: 'blur(6px)',
                    }}
                    open={showModal}
                    onClose={() => {
                        setShowModal(false);
                        setPopupOpen(false);
                    }}
                    modal
                    closeOnDocumentClick
                >
                    <div className="h-full w-full select-none flex flex-col bg-gray-800 items-center justify-center">
                        <span className="mb-4 text-lg">Are you sure you want to leave this Server?</span>
                        <div className="flex items-center w-full">
                            <button
                                className="bg-blue-700 hover:bg-blue-500 text-white m-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                onClick={() => {
                                    setShowModal(false);
                                    setPopupOpen(false);
                                }}
                            >
                                Cancel
                            </button>
                            <button
                                className="bg-red-700 hover:bg-red-500 text-white m-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                onClick={() => {
                                    leaveServer(serverID);
                                    history.push("/@home");
                                }}
                            >
                                Leave Server
                            </button>
                        </div>
                    </div>
                </Popup>
            </div>
        </header>
    );
};

export default connector(ServerBanner);