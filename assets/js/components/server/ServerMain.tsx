import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import ChatHeader from '../chat/ChatHeader';
import ChatMain from '../chat/ChatMain';
import ChatEmpty from '../chat/ChatEmpty';
import { ChatTypes } from '../chat/ChatTypes';
import ChatBox from '../chat/ChatBox';

import { fetchChannels } from '../../reducers/Channels';
import { ServerHomeState } from '../../reducers/ServerHome';

import { useParams } from 'react-router-dom';

interface RootState {
    ServerHome: ServerHomeState
};

const mapState = (state: RootState) => ({
    showMemberList: state.ServerHome.showMemberList
});

const mapDispatch = {
    fetchChannels
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface ServerMainProps {
}

type Props = PropsFromRedux & ServerMainProps;


const ServerMain: React.FC<Props> = ({
    showMemberList,
    fetchChannels,
}) => {

    const { serverID, channelID } = useParams();

    useEffect(() => {
        if (serverID != null) {
            fetchChannels(serverID);
        }
    }, [serverID]);

    return (
        <div className={`${showMemberList ? 'sm:w-2/4 md:w-2/4 lg:w-14/20' : 'w-3/4 lg:w-17/20'} flex flex-col`}>
            {(serverID != null && channelID != null) ? (
                <>
                    <ChatHeader />
                    <ChatMain type={ChatTypes.Channel} />
                    <ChatBox />
                </>
            ) : (
                <ChatEmpty />
            )}
        </div>
    );
};

export default connector(ServerMain);