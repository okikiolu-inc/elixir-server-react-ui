import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useParams } from 'react-router-dom';

import { ServersState, fetchServerMembers } from '../../reducers/Servers';
import { ServerHomeState } from '../../reducers/ServerHome';


interface RootState {
    Servers: ServersState,
    ServerHome: ServerHomeState
};

const mapState = (state: RootState) => ({
    server_members: state.Servers.server_members,
    showMemberList: state.ServerHome.showMemberList,
});

const mapDispatch = {
    fetchServerMembers,
};


const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
};


const ServerMembers: React.FC<Props> = ({
    server_members,
    showMemberList,
    fetchServerMembers,
}) => {

    const { serverID } = useParams();

    useEffect(() => {
        if (serverID != null && serverID.trim() !== '') {
            fetchServerMembers(serverID);
        }
    }, [serverID]);

    return (
        <div className={`md:w-1/4 lg:w-3/20 flex flex-col bg-gray-800 shadow-md overflow-y-auto overflow-x-hidden scrollbar-w-2 scrolling-touch ${showMemberList ? '' : 'hidden'}`}>
            <span
                className="py-2 pl-1 my-2 mx-1 rounded-md flex items-center justify-between text-gray-200 select-none"
            >
                Server Members
            </span>
            {server_members && server_members[serverID] && server_members[serverID].map((member, idx) => {
                return (
                    <div
                        key={`server-member-${idx}`}
                        className={`py-1 pl-1 my-2 mx-1 hover:bg-gray-600 cursor-pointer rounded-md flex items-center justify-between text-gray-200`}
                    >
                        <div className="flex flex-row items-center">
                            <img
                                src={member.avatar}
                                className="h-8 w-8 rounded-full cursor-pointer self-center mr-2"
                            />
                            <span className="text-sm">{member.username}</span>
                        </div>
                        {
                            idx === 0 && <svg fill="none" viewBox="0 0 24 24" stroke="currentColor" className="sparkles w-6 h-6 mr-2"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 3v4M3 5h4M6 17v4m-2-2h4m5-16l2.286 6.857L21 12l-5.714 2.143L13 21l-2.286-6.857L5 12l5.714-2.143L13 3z"></path></svg>
                        }
                    </div>
                );
            })}
        </div>
    );
};

export default connector(ServerMembers);