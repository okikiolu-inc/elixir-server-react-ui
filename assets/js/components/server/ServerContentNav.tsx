import React from 'react';

import ServerBanner from './ServerBanner';
import ServerRooms from './ServerRooms';
import ServerFooter from './ServerFooter';

const ServerContentNav: React.FC = () => {
    return (
        <div className="md:w-1/4 lg:w-3/20 bg-gray-500 flex flex-col shadow">
            <ServerBanner />
            <ServerRooms />
            <ServerFooter />
        </div>
    );
};

export default ServerContentNav;