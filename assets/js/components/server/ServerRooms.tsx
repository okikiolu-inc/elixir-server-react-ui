import React, { useState, useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import Popup from "reactjs-popup";

import { ChannelsState, Channel, createChannel, setLastChannel, editChannel } from '../../reducers/Channels';
import { ServersState } from '../../reducers/Servers';


interface RootState {
    Channels: ChannelsState,
    Servers: ServersState,
};

const mapState = (state: RootState) => ({
    channels: state.Channels.channels,
    owned_servers: state.Servers.owned_servers,
    inflight: state.Channels.inflight,
});

const mapDispatch = {
    createChannel,
    editChannel,
    setLastChannel,
};


const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
};

type Inputs = {
    name: string,
    description: string,
};

type EditModal = {
    server_id: string,
    channel_id: string,
    name: string,
    description: string,
};

const ServerRooms: React.FC<Props> = ({
    channels,
    owned_servers,
    createChannel,
    editChannel,
    setLastChannel,
    inflight,
}) => {

    const [showModal, setShowModal] = useState(false);
    const { serverID, channelID } = useParams();
    const history = useHistory();

    useEffect(() => {
        if (channelID != null && channelID.trim() !== '') {
            setLastChannel(serverID, channelID);
        }
    }, [channelID]);

    const { register, handleSubmit, errors } = useForm<Inputs>();
    const onSubmit = (data: Inputs) => {
        setShowModal(false);
        createChannel(data.name, data.description, serverID);
    };

    let chan: Channel[] = [];
    if (serverID != null && channels != null) {
        const rawChannels = channels[serverID];
        if (rawChannels != null) {
            Object.keys(rawChannels).map((channel_id) => {
                chan.push(rawChannels[channel_id]);
            });
        }
    }

    let canEditServer = false;
    if (serverID != null && owned_servers[serverID] != null) {
        canEditServer = true;
    }

    const contentStyle = {
        height: '33%',
        width: 'fit-content',
        boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
        backgroundColor: 'rgba(45, 55, 72, 1)',
        border: 'none',
        borderRadius: '0.5rem',
        padding: '2.5rem',
    };

    const overlayStyle = {
        backdropFilter: 'blur(6px)',
    }

    const [showEditModal, setShowEditModal] = useState(false);
    const [serverEdit, setServerEdit] = useState<EditModal>({
        server_id: '',
        channel_id: '',
        name: '',
        description: '',
    });
    const onEditSubmit = (data: Inputs) => {
        setShowEditModal(false);
        editChannel(serverEdit.server_id, serverEdit.channel_id, data.name, data.description);
        setServerEdit({
            server_id: '',
            channel_id: '',
            name: '',
            description: '',
        });
    };

    return (
        <main className="flex-1 overflow-y-auto scrollbar-w-2 scrolling-touch bg-gray-700">
            {canEditServer && 
            <>
                <div
                    className="py-1 pl-1 my-2 mx-1 text-gray-500 hover:text-gray-200 rounded-md flex items-center justify-between select-none cursor-pointer"
                    onClick={() => {
                        setShowModal(true);
                    }}
                >
                    <span className="text-sm select-none">Create a Channel</span>
                    <svg className="h-4 w-4 mr-2 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 4v16m8-8H4" />
                    </svg>
                </div>
                <Popup
                    contentStyle={contentStyle}
                    overlayStyle={overlayStyle}
                    onClose={() => {
                        setShowModal(false);
                    }}
                    open={showModal}
                    modal
                    closeOnDocumentClick
                >
                    <div className="h-full select-none flex flex-col bg-gray-800 items-center justify-center">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                                    Channel Name
                                </label>
                                <input
                                    className={`shadow appearance-none border ${errors.name ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                    id="name"
                                    name="name"
                                    type="text"
                                    placeholder="Your Cool Channel Name"
                                    defaultValue={""}
                                    ref={register({
                                        required: true,
                                        minLength: 1
                                    })}
                                    readOnly={inflight}
                                />
                                {errors.name && <p className="text-red-500 text-xs italic">Your channel name needs to be at least 1 character.</p>}
                            </div>
                            <div className="mb-4">
                                <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                                    Channel Description
                                </label>
                                <input
                                    className={`shadow appearance-none border ${errors.description ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                    id="description"
                                    name="description"
                                    type="text"
                                    placeholder="Your Channels Description"
                                    defaultValue={""}
                                    ref={register({
                                        required: true,
                                        minLength: 1
                                    })}
                                    readOnly={inflight}
                                />
                                {errors.description && <p className="text-red-500 text-xs italic">Your channel name needs to be at least 1 character.</p>}
                            </div>
                            <div className="flex items-center flex-col">
                                <button
                                    className="bg-blue-500 hover:bg-green-700 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                    type="submit"
                                    disabled={inflight}
                                >
                                    Create Channel
                                </button>
                            </div>
                        </form>
                    </div>
                </Popup>
            </>
            }
            {chan.map((room, idx) => {
                const isSelected = room.channel_id === channelID;
                return (
                    <div
                        key={`server-room-${idx}`}
                        className={`py-1 my-2 mx-1 hover:bg-gray-600 ${isSelected ? 'bg-gray-600' : ''} cursor-pointer rounded-md flex items-center select-none justify-between ${room ? 'text-gray-200' : 'text-gray-500'}`}
                        onClick={() => {
                            history.push(`/@server/${serverID}/${room.channel_id}`);
                        }}
                    >
                        <div className="flex items-center truncate">
                            <svg className="h-4 w-4 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M7 20l4-16m2 16l4-16M6 9h14M4 15h14" />
                            </svg>
                            <span className="truncate">{room.name}</span>
                        </div>
                        <div className="flex flex-row">
                            {room && (
                                <svg className="h-4 w-4 mr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z" />
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z" />
                                </svg>
                            )}
                            {canEditServer && (
                                <svg
                                    onClick={() => {
                                        setServerEdit(room);
                                        setShowEditModal(true);
                                    }}
                                    className="h-4 w-4 mr-1 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                                >
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" /><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                            )}
                        </div>
                    </div>
                );
            })}
            <Popup
                contentStyle={contentStyle}
                overlayStyle={overlayStyle}
                onClose={() => {
                    setShowEditModal(false);
                }}
                open={showEditModal}
                modal
                closeOnDocumentClick
            >
                <div className="h-full select-none flex flex-col bg-gray-800 items-center justify-center">
                    <form onSubmit={handleSubmit(onEditSubmit)}>
                        <div className="mb-4">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                                Updated Channel Name
                            </label>
                            <input
                                className={`shadow appearance-none border ${errors.name ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                id="name"
                                name="name"
                                type="text"
                                placeholder="Your Cool Channel Name"
                                defaultValue={serverEdit.name}
                                ref={register({
                                    required: true,
                                    minLength: 1
                                })}
                                readOnly={inflight}
                            />
                            {errors.name && <p className="text-red-500 text-xs italic">Your channel name needs to be at least 1 character.</p>}
                        </div>
                        <div className="mb-4">
                            <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                                Channel Description
                                </label>
                            <input
                                className={`shadow appearance-none border ${errors.description ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                id="description"
                                name="description"
                                type="text"
                                placeholder="Your Channels Description"
                                defaultValue={serverEdit.description}
                                ref={register({
                                    required: true,
                                    minLength: 1
                                })}
                                readOnly={inflight}
                            />
                            {errors.description && <p className="text-red-500 text-xs italic">Your channel name needs to be at least 1 character.</p>}
                        </div>
                        <div className="flex items-center flex-col">
                            <button
                                className="bg-blue-500 hover:bg-green-700 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                type="submit"
                                disabled={inflight}
                            >
                                Update Channel
                            </button>
                        </div>
                    </form>
                </div>
            </Popup>
            {chan.length == 0 && <div className="flex flex-col pt-10 justify-center align-middle items-center h-half">
                <svg className="w-12 h-12 text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M7 20l4-16m2 16l4-16M6 9h14M4 15h14" />
                </svg>
                <span className="text-lg text-gray-500 select-none">There are no Channels</span>
            </div>}
        </main>
    );
};

export default connector(ServerRooms);