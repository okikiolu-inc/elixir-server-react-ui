import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import Popup from "reactjs-popup";
import { useForm } from 'react-hook-form';


import { UserState, logout, updateUserStatus, updateUserAvatar } from '../../reducers/User';

interface RootState {
    User: UserState
};

const mapState = (state: RootState) => ({
    username: state.User.username,
    userhash: state.User.userhash,
    userid: state.User.userid,
    status: state.User.status,
    email: state.User.email,
    avatar: state.User.avatar,
    inflight: state.User.inflight,
});

const mapDispatch = {
    logout,
    updateUserStatus,
    updateUserAvatar,
}

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
}

type Inputs = {
    status: string,
};

const ServerFooter: React.FC<Props> = ({
    username,
    userhash,
    userid,
    avatar,
    logout,
    email,
    status,
    inflight,
    updateUserStatus,
    updateUserAvatar,
}) => {
    const contentStyle: React.CSSProperties = {
        borderRadius: '0.5rem',
        color: 'white',
        backgroundColor: 'rgba(26, 32, 44, 1)',
        padding: 10,
        userSelect: "none",
        width: 'fit-content',
        border: 'none',
        boxShadow: 'none',
    };
    const arrowStyle: React.CSSProperties = {
        backgroundColor: 'rgba(26, 32, 44, 1)',
        color: 'rgba(26, 32, 44, 1)',
    };

    const { register, handleSubmit, errors } = useForm<Inputs>();
    const onSubmit = (data: Inputs) => {
        updateUserStatus(userid, data.status);
    };

    const onFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files != null) {
            updateUserAvatar(e.target.files[0]);
        }
    };

    return (
        <footer className="h-12 bg-gray-900 flex flex-row shadow">
            <img
                src={avatar}
                className="h-8 w-8 rounded-full cursor-pointer self-center mx-2"
            />
            <div className="text-xs flex-1 flex truncate flex-col justify-center">
                <span className="text-gray-200">{username}&nbsp;<span className="text-gray-600">#{userhash}</span></span>
                <span className="text-gray-600">{status}</span>
            </div>
            <div className="self-center mx-1">
                <Popup
                    trigger={
                        <div>
                            <Popup
                                trigger={
                                    <svg className="h-6 w-6 text-gray-500 hover:text-gray-300 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                    </svg>
                                }
                                contentStyle={{
                                    display: 'flex',
                                    boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
                                    backgroundColor: 'rgba(45, 55, 72, 1)',
                                    border: 'none',
                                    borderRadius: '0.5rem',
                                }}
                                overlayStyle={{
                                    backdropFilter: 'blur(6px)',
                                }}
                                modal
                                closeOnDocumentClick
                                className="test test2"
                            >
                                <div className="w-full select-none">
                                    <div className="flex flex-col bg-gray-800 w-full items-center justify-between">
                                        <span className="font-bold text-white select-none">USER SETTINGS</span>
                                        <div
                                            className="flex flex-row bg-gray-900 select-none rounded-lg w-full p-4 my-4"
                                        >
                                            <label htmlFor="avatar">
                                                <input
                                                    type="file"
                                                    className="cursor-pointer absolute block h-0 w-0 opacity-0 top-0 right-0"
                                                    onChange={onFileChange}
                                                    name="avatar"
                                                    id="avatar"
                                                    accept="image/*"
                                                />
                                                <img
                                                    src={avatar}
                                                    className="h-24 w-24 self-center rounded-full cursor-pointer hover:bg-black hover:opacity-50"
                                                    alt={`${username}-avatar`}
                                                    title="Change Avatar"
                                                />
                                            </label>
                                            <div className="flex flex-col ml-4 justify-between text-gray-600">
                                                <div className="flex flex-col">
                                                    <span className="font-bold">Username</span>
                                                    <span className="text-gray-500 text-md">{username}&nbsp;<span className="text-gray-700">#{userhash}</span></span>
                                                </div>
                                                <div className="flex flex-col">
                                                    <span className="font-bold">Email</span>
                                                    <span className="text-gray-500 text-md">{email}</span>
                                                </div>
                                                <div className="flex flex-col">
                                                    <span className="font-bold">Status</span>
                                                    <form onSubmit={handleSubmit(onSubmit)} className="flex items-center">
                                                        <input
                                                            className={`shadow appearance-none border ${errors.status ? 'border-red-500' : ''} rounded w-5/6 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                                            id="status"
                                                            name="status"
                                                            type="text"
                                                            placeholder="How are you?"
                                                            defaultValue={status}
                                                            ref={register({
                                                                required: true,
                                                                maxLength: 100
                                                            })}
                                                            readOnly={inflight}
                                                        />
                                                        {errors.status && <p className="text-red-500 text-xs italic">Please fill in a server name.</p>}
                                                        <button
                                                            className="w-1/6 bg-green-700 hover:bg-green-500 text-white my-2 font-bold py-2 px-3 rounded focus:outline-none focus:shadow-outline"
                                                            type="submit"
                                                            disabled={inflight}
                                                        >
                                                            <svg fill="none" viewBox="0 0 24 24" stroke="currentColor" className="check w-6 h-6"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" /></svg>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <button
                                            className="bg-red-800 hover:bg-red-500 text-white my-2 w-3/4 font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                            onClick={() => {
                                                logout();
                                            }}
                                        >
                                        LOGOUT
                                        </button>
                                    </div>
                                </div>
                            </Popup>
                        </div>
                    }
                    position="top center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    on="hover"
                >
                    <span>Settings</span>
                </Popup>
            </div>
        </footer>
    );
};

export default connector(ServerFooter);