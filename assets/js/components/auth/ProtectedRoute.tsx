import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import { UserState } from '../../reducers/User';

interface RootState {
    User: UserState
};

const mapState = (state: RootState) => ({
    userid: state.User.userid,
    usertoken: state.User.usertoken
});

const connector = connect(mapState);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & RouteProps & {
    component: any,
};

const ProtectedRoute = (props: Props) => {
    const {
        userid,
        usertoken,
        component: Component,
        ...rest
    } = props;

    const valid =
        userid != null &&
        usertoken != null && usertoken.trim() !== '';

    const DESTINATION_URL = 'DESTINATION_URL';
    let destinationUrl = '';

    if (!valid) {
        if (rest.location && rest.location.pathname) {
            localStorage.setItem(DESTINATION_URL, rest.location.pathname);
        }
    }
    else {
        const tempDest = localStorage.getItem(DESTINATION_URL);
        if (tempDest != null && tempDest.trim() !== '') {
            destinationUrl = tempDest;
            localStorage.removeItem(DESTINATION_URL);
        }
    }

    return (
        <Route
            {...rest}
            render={(routeProps) =>
                valid ? (
                    destinationUrl !== '' ?
                    <Redirect
                        to={{
                            pathname: destinationUrl,
                            state: { from: routeProps.location }
                        }}
                    /> :
                    <Component {...routeProps} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {from: routeProps.location}
                        }}
                    />
                )
            }
        />
    )
}

export default connector(ProtectedRoute);