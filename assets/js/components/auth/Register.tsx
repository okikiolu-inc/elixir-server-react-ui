import React, { useState } from 'react';

import { connect, ConnectedProps } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Redirect } from 'react-router-dom';
import { register, UserState } from '../../reducers/User';


const mapDispatch = {
    register,
}

interface RootState {
    User: UserState
};

const mapState = (state: RootState) => ({
    userid: state.User.userid,
    usertoken: state.User.usertoken,
    inflight: state.User.inflight,
    error: state.User.error,
});

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
}

type Inputs = {
    email: string,
    username: string,
    password: string,
    confirmpassword: string,
};

const Register: React.FC<Props> = ({
    userid,
    usertoken,
    register,
    inflight,
    error,
}) => {

    const loggedIn =
        userid != null && usertoken.trim() !== '' &&
        usertoken != null && usertoken.trim() !== '';

    const { register: formRegister, handleSubmit, errors, watch } = useForm<Inputs>();
    const onSubmit = (data: Inputs) => {
        register(data.email, data.username, data.password);
    }


    const d = new Date();
    const year = d.getFullYear();

    return (
       loggedIn === true ?
       (
            <Redirect
                to={{
                    pathname: "/@home",
                }}
            />
       ) : (
            <div className="flex flex-col bg-gray-800 w-full items-center justify-center">
                <form
                    className="bg-gray-600 max-w-md shadow-md rounded px-8 pt-6 pb-8 mb-4"
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <div className="mb-4">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="email">
                            Email
                        </label>
                        <input
                            className={`shadow appearance-none border ${errors.email ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                            name="email"
                            id="email"
                            type="text"
                            placeholder="em@ail.com"
                            defaultValue=""
                            ref={formRegister({
                                required: true,
                                pattern: /^\S+@\S+$/i,
                            })}
                            readOnly={inflight}
                        />
                        {errors.email && <p className="text-red-500 text-xs italic">Please enter a valid email.</p>}
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="username">
                            Username
                        </label>
                        <input
                            className={`shadow appearance-none border ${errors.username ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline`}
                            name="username"
                            id="username"
                            type="text"
                            placeholder="something_cool"
                            defaultValue=""
                            ref={formRegister({
                                required: true,
                            })}
                            readOnly={inflight}
                        />
                        {errors.username && <p className="text-red-500 text-xs italic">Please enter a valid username.</p>}
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="password">
                            Password
                        </label>
                        <input
                            className={`shadow appearance-none border ${errors.password ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline`}
                            name="password"
                            id="password"
                            type="password"
                            placeholder="******************"
                            defaultValue=""
                            ref={formRegister({
                                required: true,
                                minLength: 8
                            })}
                            readOnly={inflight}
                        />
                            <p className="text-white text-xs italic">Must be at least 8 characters</p>
                            {errors.password && <p className="text-red-500 text-xs italic">Please enter a valid password</p>}
                    </div>
                    <div className="mb-6">
                        <label className="block text-white text-sm font-bold mb-2" htmlFor="confirmpassword">
                            Re-Type Password
                    </label>
                        <input
                            className={`shadow appearance-none border ${errors.confirmpassword ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline`}
                            name="confirmpassword"
                            id="confirmpassword"
                            type="password"
                            placeholder="******************"
                            defaultValue=""
                            ref={formRegister({
                                required: true,
                                validate: (value) => {
                                    return value === watch('password');
                                }
                            })}
                            readOnly={inflight}
                        />
                        {errors.confirmpassword && <p className="text-red-500 text-xs italic">Passwords must match.</p>}
                        {error && (
                            <>
                                <br />
                                <p className="text-red-500 text-xs italic">{error}</p>
                            </>
                        )}
                    </div>
                    <div className="flex items-center flex-col">
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" 
                            type="submit"
                            disabled={inflight}
                        >
                            Sign Up
                        </button>
                        <a className="inline-block align-baseline font-bold text-sm my-2 text-white hover:text-blue-800" href="/login">
                            or Sign In
                        </a>
                    </div>
                </form>
                <p className="text-center text-gray-500 text-xs">
                    &copy;{year} Helios Messaging Inc. All rights reserved.
                </p>
            </div>
        )
    );
};

export default connector(Register);