import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useParams } from 'react-router-dom';
import { toogleMemberList } from '../../reducers/ServerHome';
import Popup from "reactjs-popup";

import { ChannelsState } from '../../reducers/Channels';

interface RootState {
    Channels: ChannelsState,
};

const mapState = (state: RootState) => ({
    channels: state.Channels.channels
});

const mapDispatch = {
    toogleMemberList,
}

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
}

const ChatHeader: React.FC<Props> = ({
    toogleMemberList,
    channels,
}) => {

    const { serverID, channelID } = useParams();

    let channelname = '';
    let channel_descr = '';

    if (serverID && channelID && channels[serverID] && channels[serverID][channelID]) {
        const channel = channels[serverID][channelID];
        if (channel) {
            channelname = channel.name;
            channel_descr = channel.description;
        }
    }

    const contentStyle: React.CSSProperties = {
        borderRadius: '0.5rem',
        color: 'white',
        backgroundColor: 'rgba(26, 32, 44, 1)',
        padding: 10,
        userSelect: "none",
        width: 'fit-content',
        border: 'none',
        boxShadow: 'none',
    };
    const arrowStyle: React.CSSProperties = {
        backgroundColor: 'rgba(26, 32, 44, 1)',
        color: 'rgba(26, 32, 44, 1)',
    };

    const [query, setQuery] = useState('');
    const placeHolder = 'Search';
    
    return (
        <header className="h-12 bg-gray-800 shadow-lg flex flex-row select-none">
            <div
                className="py-1 pl-1 my-2 ml-2 rounded-md flex-none flex items-center"
            >
                <svg className="h-6 w-6 mr-1 text-gray-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M7 20l4-16m2 16l4-16M6 9h14M4 15h14" />
                </svg>
                <span className="text-gray-500">{channelname}</span>
            </div>
            <div className="border-solid border-l border-1 ml-2 h-6 border-gray-700 self-center" />
            <div
                className="py-1 pl-2 my-2 rounded-md text-gray-500 flex-auto flex items-center truncate"
            >
                {channel_descr}
            </div>
            <div className="border-solid border-l border-1 ml-2 h-6 border-gray-700 self-center" />
            <div
                className="py-1 pl-2 my-2 flex items-center"
            >
                <Popup
                    trigger={
                        <svg className="h-6 w-6 mr-1 text-gray-500 hover:text-gray-200 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                        </svg>
                    }
                    position="bottom center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    on="hover"
                >
                    <span>Alerts</span>
                </Popup>
                <Popup
                    trigger={
                        <svg className="h-6 w-6 mr-1 text-gray-500 hover:text-gray-200 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
                        </svg>
                    }
                    position="bottom center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    on="hover"
                >
                    <span>Pinned</span>
                </Popup>
                <Popup
                    trigger={
                        <svg
                            onClick={() => {
                                toogleMemberList();
                            }}
                            className="h-6 w-6 mr-1 text-gray-500 hover:text-gray-200 cursor-pointer"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    }
                    position="bottom center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    on="hover"
                >
                    <span>Server Members</span>
                </Popup>
                <input className="bg-gray-900 text-gray-200 focus:outline-none rounded-sm pl-2 my-2 mx-1 block appearance-none leading-normal text-xs" placeholder={placeHolder} defaultValue={query} type="text" />
                <Popup
                    trigger={
                        <svg className="h-6 w-6 mr-1 text-gray-500 hover:text-gray-200 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    }
                    position="bottom center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    on="hover"
                >
                    <span>Help</span>
                </Popup>
            </div>
        </header>
    );
};

export default connector(ChatHeader);