import React, { useEffect, useRef, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { ChatTypes } from './ChatTypes';
import { useParams } from 'react-router-dom';
import Message from '../message/Message';
import { ChannelsState } from '../../reducers/Channels';
import { MessagesState, fetchMessages, fetchPreviousMessages } from '../../reducers/Messages';
import { ServersState, getShallowUserInfo } from '../../reducers/Servers';

interface RootState {
    Channels: ChannelsState,
    Messages: MessagesState,
    Servers: ServersState,
};

const mapState = (state: RootState) => ({
    channels: state.Channels.channels,
    messages: state.Messages.messages,
    end: state.Messages.end,
    member_info: state.Servers.member_info
});

const mapDispatch = {
    fetchMessages,
    fetchPreviousMessages,
    getShallowUserInfo,
};


const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
    type: ChatTypes
};

const ChatMain: React.FC<Props> = ({
    type,
    messages,
    end,
    fetchMessages,
    fetchPreviousMessages,
    member_info,
    getShallowUserInfo
}) => {

    const { serverID, channelID } = useParams();

    useEffect(() => {
        if (serverID != null && serverID.trim() !== '' && channelID != null && channelID.trim() !== '') {
            fetchMessages(channelID);
        }
    }, [serverID, channelID]);

    const chatWindow = useRef(document.createElement('main'));

    const [firstLoad, setFirstLoad] = useState(true);
    const scrollToBottom = () => {
        const shouldScroll = chatWindow.current.scrollTop + chatWindow.current.clientHeight === chatWindow.current.scrollHeight;
        if (firstLoad) {
            console.log('first load');
            chatWindow.current.scrollTop = chatWindow.current.scrollHeight;
            setFirstLoad(false);
        }
        else if (shouldScroll) {
            chatWindow.current.scrollTop = chatWindow.current.scrollHeight;
        }
    }
    useEffect(() => {
        if (messages[channelID] != null) {
            scrollToBottom();
        }
    }, [messages[channelID]]);

    const showButton = (messages[channelID] && messages[channelID].length > 0) && end[channelID] !== true;


    return (
        <main
            className="flex-1 overflow-y-auto scrollbar-w-2 scrolling-touch bg-gray-800"
            ref={chatWindow}
        >
            {
                showButton && <div className="p-3 mt-2">
                    <button
                        className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded w-full"
                        onClick={() => {
                            /*
                                To load more messages, simply load since the earliest message ID in the Queue i.e messages[channelID][0].message_id,
                                in the schema.ex get_channel_messages(channel_id, message_id)
                            */
                            fetchPreviousMessages(channelID, messages[channelID][0].message_id);
                        }}
                    >
                        Load More Messages
                    </button>
                </div>
            }
            {
                end[channelID] === true && messages[channelID] != null && messages[channelID].length !== 0 && (
                    <div className="flex flex-row pb-5 my-3 pr-1 select-none justify-center">
                        <span className="text-gray-500">You have reached the end of the chat history. Well Done!</span>
                    </div>
                )
            }
            {
                end[channelID] === true && messages[channelID] != null && messages[channelID].length === 0 && (
                    <div className="flex flex-row pb-5 my-3 pr-1 select-none h-half justify-center">
                        <span className="text-gray-500 self-center">There is nothing here, why don't you post something</span>
                    </div>
                )
            }
            {messages[channelID] != null && messages[channelID].map((message, idx) => {
                return (
                    <Message
                        key={`chat-message-${idx}`}
                        username={message.username}
                        userhash={message.userhash}
                        text={message.content}
                        avatar={message.avatar}
                        time={message.updated}
                        member_info={member_info[`${message.username}#${message.userhash}`]}
                        onImageLoad={() => {
                            console.log('on image load');
                            scrollToBottom();
                        }}
                        getUserInfo={getShallowUserInfo}
                    />
                );
            })}
        </main>
    );
};

export default connector(ChatMain);