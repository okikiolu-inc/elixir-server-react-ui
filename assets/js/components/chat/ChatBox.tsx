import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';

import { sendMessage } from '../../socket';
import { ChannelsState } from '../../reducers/Channels';
import { UserState } from '../../reducers/User';


interface RootState {
    Channels: ChannelsState,
    User: UserState,
};

const mapState = (state: RootState) => ({
    userid: state.User.userid,
    username: state.User.username,
    userhash: state.User.userhash,
    avatar: state.User.avatar,
    channels: state.Channels.channels
});

const mapDispatch = {
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
}

const ChatBox: React.FC<Props> = ({
    userid,
    username,
    userhash,
    channels,
    avatar,
}) => {

    const { serverID, channelID } = useParams();

    let channelname = '';

    if (serverID && channelID && channels[serverID] && channels[serverID][channelID]) {
        const channel = channels[serverID][channelID];
        if (channel) {
            channelname = channel.name;
        }
    }

    const [message, setMessage] = useState('');

    return (
        <footer className="h-16 bg-gray-800 flex flex-row">
            <div className="text-blue-500 pl-2 flex justify-center cursor-pointer hover:text-blue-300">
                <svg className="h-8 w-8 self-center" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z" clipRule="evenodd" />
                </svg>
            </div>
            <input
                className="bg-gray-700 text-gray-200 focus:outline-none rounded-lg pl-2 my-2 mx-2 block w-full appearance-none leading-normal"
                placeholder={`Message #${channelname}`}
                value={message}
                onChange={(e) => {
                    setMessage(e.target.value);
                }}
                type="text"
                onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                        if (message.trim() !== '') {
                            e.preventDefault();
                            if (serverID != null && serverID.trim() !== '' && channelID != null && channelID.trim()) {
                                sendMessage({
                                    user_id: userid,
                                    server_id: serverID,
                                    channel_id: channelID,
                                    avatar: avatar,
                                    content: message,
                                    username: username,
                                    userhash: userhash,
                                });
                                setMessage('');
                            }
                        }
                    }
                }}
            />
            <div className="text-blue-500 pr-2 flex justify-center cursor-pointer hover:text-blue-300">
                <svg className="h-8 w-8 self-center" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-.464 5.535a1 1 0 10-1.415-1.414 3 3 0 01-4.242 0 1 1 0 00-1.415 1.414 5 5 0 007.072 0z" clipRule="evenodd" />
                </svg>
            </div>
        </footer>
    );
};

export default connector(ChatBox);