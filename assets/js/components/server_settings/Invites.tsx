import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux'
import { useForm } from 'react-hook-form';

import { ServerInvitesState, fetchServerInvites, deleteServerInvite, createServerInvite } from '../../reducers/ServerInvites';

interface RootState {
    ServerInvites: ServerInvitesState,
};

const mapState = (state: RootState) => ({
    server_invites: state.ServerInvites.server_invites,
});


const mapDispatch = {
    fetchServerInvites,
    deleteServerInvite,
    createServerInvite
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
    serverID: string,
};

type Inputs = {
};

const ServerSettingsInvites: React.FC<Props> = ({
    server_invites,
    fetchServerInvites,
    deleteServerInvite,
    createServerInvite,
    serverID,
}) => {

    const duration_map = [
        'NEVER',
        '1 Hour',
        '6 Hours',
        '12 Hours',
        '1 Day'
    ];

    const { register, handleSubmit, errors } = useForm<Inputs>();
    const onSubmit = (data: Inputs) => {
        createServerInvite(serverID);
    };

    useEffect(() => {
        if (serverID != null && serverID.trim() !== '') {
            fetchServerInvites(serverID);
        }
    }, [serverID]);

    const origin = window.location.origin;

    return (
        <div className="w-full select-none flex flex-col text-white bg-gray-800 items-center lg:items-start p-4">
            <span className="w-full text-left text-lg font-bold py-3">INVITES</span>
            <span className="w-full text-left text-gray-500 text-md py-2">Here are your active invites</span>
            <span className="border-b border-gray-600 my-4 w-full"></span>
            <div className="table w-full lg:w-3/4">
                <div className="table-row-group text-sm">
                    <div className="table-row">
                        <div className="table-cell px-4 py-2 font-bold">INVITE URL</div>
                        <div className="table-cell px-4 py-2 font-bold">USES</div>
                        <div className="table-cell px-4 py-2 font-bold">EXPIRES</div>
                    </div>
                    {server_invites[serverID] && server_invites[serverID].map((invite, idx) => {
                        return (
                            <div className="table-row" key={`invite-${idx}`}>
                                <div className="table-cell px-4 py-2 text-gray-500 text-sm select-text break-all">
                                    <a href={`${origin}/invite/${invite.server_id}/${invite.invite_id}`} target="_blank">
                                        {`${origin}/invite/${invite.server_id}/${invite.invite_id}`}
                                    </a>
                                </div>
                                <div className="table-cell px-4 py-2">{invite.join_count}</div>
                                <div className="table-cell px-4 py-2">{duration_map[invite.duration]}</div>
                                <div className="table-cell px-4 py-2">
                                    <button
                                        className="bg-red-700 hover:bg-red-500 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                        onClick={() => {
                                            deleteServerInvite(serverID, invite.invite_id);
                                        }}
                                    >
                                        DELETE
                                    </button>
                                </div>
                            </div>
                        );
                    })}
                    <form className="table-row" onSubmit={handleSubmit(onSubmit)}>
                        <div className="table-cell px-4 py-2 text-gray-500 text-sm"></div>
                        <div className="table-cell px-4 py-2"></div>
                        <div className="table-cell px-4 py-2"></div>
                        <div className="table-cell px-4 py-2">
                            <button
                                className="bg-green-700 hover:bg-green-500 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                type="submit"
                            >
                                CREATE
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default connector(ServerSettingsInvites);
