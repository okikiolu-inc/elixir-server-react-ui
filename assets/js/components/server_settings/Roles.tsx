import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux'
import { useForm } from 'react-hook-form';
import constants from '../../constants';
import Popup from "reactjs-popup";


import { ServerRolesState, fetchServerRoles, deleteServerRole, updateServerRole, createServerRole } from '../../reducers/ServerRoles';

interface RootState {
    ServerRoles: ServerRolesState,
};

const mapState = (state: RootState) => ({
    roles: state.ServerRoles.roles,
});


const mapDispatch = {
    fetchServerRoles,
    deleteServerRole,
    createServerRole,
    updateServerRole,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
    serverID: string,
};

type Inputs = {
    name: string,
    color: string,
    permission: (string | boolean)[],
};

const ServerSettingsRoles: React.FC<Props> = ({
    roles,
    fetchServerRoles,
    createServerRole,
    updateServerRole,
    deleteServerRole,
    serverID,
}) => {

    useEffect(() => {
        if (serverID != null && serverID.trim() !== '') {
            fetchServerRoles(serverID);
        }
    }, [serverID]);

    const [currentRole, setCurrentRole] = useState<string | number>('');
    const [currentColor, setCurrentColor] = useState('');

    const [permsSet, setPermsSet] = useState<Set<string>>();

    const [showModal, setShowModal] = useState(false);


    const { register, handleSubmit, errors } = useForm<Inputs>();
    const onSubmit = (data: Inputs) => {
        const cleanedPerms = data.permission.filter((perm) => {
            return perm !== false;
        });
        createServerRole(serverID, data.name, data.color, cleanedPerms);
    };

    const onUpdateSubmit = (data: Inputs) => {
        const cleanedPerms = data.permission.filter((perm) => {
            return perm !== false;
        });

        if (typeof currentRole === 'number' && roles[serverID][currentRole]) {
            updateServerRole(serverID, roles[serverID][currentRole].role_id, data.name, data.color, cleanedPerms);
        }
    };

    return (
        <>
        <div className="w-full select-none text-white bg-gray-800 p-2 flex">
            <div className="w-1/3 md:w-1/4 border-r border-gray-600 mr-2 flex flex-col">
                {roles[serverID] && roles[serverID].map((role, idx) => {
                    return (
                        <div
                            className="py-1 pl-1 my-2 mx-1 text-gray-500 hover:text-gray-200 rounded-md flex items-center justify-between select-none cursor-pointer"
                            key={`invite-${idx}`}
                            onClick={() => {
                                setPermsSet(new Set(role.permissions));
                                setCurrentColor(role.color);
                                setCurrentRole(idx);
                            }}
                        >
                            {role.name}
                        </div>
                    );
                })}
                <div
                    className="py-1 pl-1 my-2 mx-1 text-gray-500 hover:text-gray-200 rounded-md flex items-center justify-between select-none cursor-pointer"
                    onClick={() => {
                        setCurrentColor(constants.COLOR[0]);
                        setCurrentRole('new');
                    }}
                >
                    <span className="text-sm select-none">Create a Role</span>
                    <svg className="h-4 w-4 mr-2 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 4v16m8-8H4" />
                    </svg>
                </div>
            </div>
            <div className="w-2/3 md:w-3/4 flex flex-col">
                {currentRole === 'new' && (
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="mb-2">
                            <label className="block font-bold mb-2" htmlFor="name">
                                Role Name
                            </label>
                            <input
                                className={`shadow appearance-none border ${errors.name ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                name="name"
                                type="text"
                                placeholder="role name"
                                defaultValue=""
                                ref={register({
                                    required: true,
                                })}
                            />
                            {errors.name && <p className="text-red-500 text-xs italic">Please enter a role name.</p>}
                        </div>

                        <div className="mb-2">
                            <label className="block font-bold mb-2" htmlFor="color">
                                Color
                            </label>
                            <select
                                name="color"
                                ref={register({
                                    required: true,
                                })}
                                className={`shadow appearance-none border ${errors.color ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${constants.PILL_COLOR[currentColor]}`}
                                onChange={(e) => {
                                    setCurrentColor(e.target.value);
                                }}
                            >
                                {constants.COLOR.map((color) => {
                                    return (
                                    <option key={`select-color-${color}`} value={color}>{color}</option>
                                    );
                                })}
                            </select>
                            {errors.color && <p className="text-red-500 text-xs italic">Please enter a role name.</p>}
                        </div>

                        <div className="mb-2 flex flex-col">
                            {Object.keys(constants.PERMISSIONS).map((permission, idx) => {
                                return (
                                    <label key={`permission-checkbox-${idx}`} className="inline-flex items-center mt-3">
                                        <input
                                            type="checkbox"
                                            className={`form-checkbox h-5 w-5 ${constants.TEXT_COLOR[currentColor]}`}
                                            name={`permission.${idx}`}
                                            value={permission}
                                            ref={register}
                                        />
                                        <span className={`ml-2 ${constants.TEXT_COLOR[currentColor]}`}>{constants.PERMISSIONS[permission]}</span>
                                    </label>
                                );
                            })}
                            {errors.color && <p className="text-red-500 text-xs italic">Please enter a role name.</p>}
                        </div>

                        <button
                            className="bg-green-700 hover:bg-green-500 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            CREATE
                        </button>
                    </form>
                )}
                {typeof currentRole === 'number' && currentRole > -1 && (
                    <form onSubmit={handleSubmit(onUpdateSubmit)}>
                        <div className="mb-2">
                            <label className="block font-bold mb-2" htmlFor="name">
                                Role Name
                            </label>
                            <input
                                className={`shadow appearance-none border ${errors.name ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                                name="name"
                                type="text"
                                placeholder="role name"
                                defaultValue={roles[serverID][currentRole].name}
                                ref={register({
                                    required: true,
                                })}
                            />
                            {errors.name && <p className="text-red-500 text-xs italic">Please enter a role name.</p>}
                        </div>

                        <div className="mb-2">
                            <label className="block font-bold mb-2" htmlFor="color">
                                Color
                            </label>
                            <select
                                name="color"
                                ref={register({
                                    required: true,
                                })}
                                className={`shadow appearance-none border ${errors.color ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline ${constants.PILL_COLOR[currentColor]}`}
                                defaultValue={roles[serverID][currentRole].color}
                                onChange={(e) => {
                                    setCurrentColor(e.target.value);
                                }}
                            >
                                {constants.COLOR.map((color) => {
                                    return (
                                        <option key={`select-color-${color}`} value={color}>{color}</option>
                                    );
                                })}
                            </select>
                            {errors.color && <p className="text-red-500 text-xs italic">Please enter a role name.</p>}
                        </div>

                        <div className="mb-2 flex flex-col">
                            {Object.keys(constants.PERMISSIONS).map((permission, idx) => {
                                return (
                                    <label key={`permission-checkbox-${idx}`} className="inline-flex items-center mt-3">
                                        <input
                                            type="checkbox"
                                            className={`form-checkbox h-5 w-5 ${constants.TEXT_COLOR[currentColor]}`}
                                            name={`permission.${idx}`}
                                            value={permission}
                                            ref={register}
                                            defaultChecked={permsSet?.has(permission)}
                                        />
                                        <span className={`ml-2 ${constants.TEXT_COLOR[currentColor]}`}>{constants.PERMISSIONS[permission]}</span>
                                    </label>
                                );
                            })}
                            {errors.color && <p className="text-red-500 text-xs italic">Please enter a role name.</p>}
                        </div>

                        <button
                            className="bg-green-700 hover:bg-green-500 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            UPDATE
                        </button>
                    </form>
                )}
                {typeof currentRole === 'number' && currentRole > -1 && (
                    <button
                        className="bg-red-700 hover:bg-red-500 text-white mt-8 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={() => {
                            setShowModal(true);
                        }}
                    >
                        DELETE
                    </button>
                )}
            </div>
        </div>
        <Popup
            contentStyle={{
                height: '33%',
                boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
                backgroundColor: 'rgba(45, 55, 72, 1)',
                border: 'none',
                borderRadius: '0.5rem',
                padding: '2.5rem',
            }}
            overlayStyle={{
                backdropFilter: 'blur(6px)',
            }}
            open={showModal}
            onClose={() => {
                setShowModal(false);
            }}
            modal
            closeOnDocumentClick
        >
            <div className="h-full w-full select-none flex flex-col font-bold text-white bg-gray-800 items-center justify-center">
                <span className="mb-2 text-lg">Are you Sure?</span>
                <span className="mb-2 text-lg">This can NEVER be undone?</span>
                <span className="mb-6 text-lg">Are you Really Really Sure?</span>
                <div className="flex items-center w-full">
                    <button
                        className="bg-blue-700 hover:bg-blue-500 m-2 w-full py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={() => {
                            setShowModal(false);
                        }}
                    >
                        Cancel
                        </button>
                    <button
                        className="bg-red-700 hover:bg-red-500 m-2 w-full py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={() => {
                            if (typeof currentRole === 'number' && currentRole > -1) {
                                const role_id = roles[serverID][currentRole].role_id;
                                deleteServerRole(serverID, role_id);
                                setCurrentRole('');
                                setShowModal(false);
                            }
                        }}
                    >
                        DELETE!
                        </button>
                </div>
            </div>
        </Popup>
        </>
    );
};

export default connector(ServerSettingsRoles);
