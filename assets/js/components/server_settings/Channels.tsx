import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import Popup from "reactjs-popup";


import { ChannelsState, Channel, deleteChannel } from '../../reducers/Channels';


interface RootState {
    Channels: ChannelsState,
};

const mapState = (state: RootState) => ({
    channels: state.Channels.channels,
});

const mapDispatch = {
    deleteChannel,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
    serverID: string,
};

const ServerSettingsChannels: React.FC<Props> = ({
    channels,
    deleteChannel,
    serverID,
}) => {

    let chan: Channel[] = [];
    if (serverID != null && channels != null) {
        const rawChannels = channels[serverID];
        if (rawChannels != null) {
            Object.keys(rawChannels).map((channel_id) => {
                chan.push(rawChannels[channel_id]);
            });
        }
    }

    const [showModal, setShowModal] = useState(false);
    const [chanId, setChanId] = useState('');

    return (
        <>
        <div className="w-full select-none flex flex-col text-white bg-gray-800 items-center lg:items-start p-4">
            <span className="w-full text-left text-lg font-bold py-3">CHANNELS</span>
            <span className="w-full text-left text-gray-500 text-md py-2">Here are your Channels</span>
            <span className="border-b border-gray-600 my-4 w-full"></span>
            <div className="table w-full lg:w-3/4">
                <div className="table-row-group text-sm">
                    <div className="table-row">
                        <div className="table-cell px-4 py-2 font-bold">NAME</div>
                        <div className="table-cell px-4 py-2 font-bold">DESCRIPTION</div>
                    </div>
                    {chan && chan.map((channel, idx) => {
                        return (
                            <div className="table-row" key={`channel-${idx}`}>
                                <div className="table-cell px-4 py-2">{channel.name}</div>
                                <div className="table-cell px-4 py-2">{channel.description}</div>
                                <div className="table-cell px-4 py-2">
                                    <button
                                        className="bg-red-700 hover:bg-red-500 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                        onClick={() => {
                                            setChanId(channel.channel_id);
                                            setShowModal(true);
                                        }}
                                    >
                                        DELETE
                                    </button>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
        {channels[serverID] && channels[serverID][chanId] && <Popup
            contentStyle={{
                height: '33%',
                boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
                backgroundColor: 'rgba(45, 55, 72, 1)',
                border: 'none',
                borderRadius: '0.5rem',
                padding: '2.5rem',
            }}
            overlayStyle={{
                backdropFilter: 'blur(6px)',
            }}
            open={showModal}
            onClose={() => {
                setShowModal(false);
            }}
            modal
            closeOnDocumentClick
        >
            <div className="h-full w-full select-none flex flex-col font-bold text-white bg-gray-800 items-center justify-center">
                <span className="mb-2 text-lg">Are you Sure you want to delete</span>
                <span className="mb-2 text-lg text-gray-600">{channels[serverID][chanId].name}</span>
                <span className="mb-2 text-lg">This can NEVER be undone?</span>
                <div className="flex items-center w-full">
                    <button
                        className="bg-blue-700 hover:bg-blue-500 m-2 w-full py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={() => {
                            setShowModal(false);
                            setChanId('');
                        }}
                    >
                        Cancel
                        </button>
                    <button
                        className="bg-red-700 hover:bg-red-500 m-2 w-full py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={() => {
                            if (channels[serverID] && channels[serverID][chanId]) {
                                deleteChannel(channels[serverID][chanId].server_id, channels[serverID][chanId].channel_id);
                                setShowModal(true);
                                setChanId('');
                            }
                        }}
                    >
                        DELETE!
                    </button>
                </div>
            </div>
        </Popup>}
        </>
    );
};

export default connector(ServerSettingsChannels);
