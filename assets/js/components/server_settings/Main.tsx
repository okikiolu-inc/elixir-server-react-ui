import React from 'react';
import { connect, ConnectedProps } from 'react-redux'
import { useForm } from 'react-hook-form';

import { ServersState, editServer, updateServerIcon, updateServerBanner } from '../../reducers/Servers';

interface RootState {
    Servers: ServersState,
};

const mapState = (state: RootState) => ({
    owned_servers: state.Servers.owned_servers,
});


const mapDispatch = {
    editServer,
    updateServerIcon,
    updateServerBanner,
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
    serverID: string,
};

type Inputs = {
    name: string,
    description: string,
};

const ServerSettingsMain: React.FC<Props> = ({
    owned_servers,
    editServer,
    serverID,
    updateServerIcon,
    updateServerBanner,
}) => {


    const { register, handleSubmit, errors } = useForm<Inputs>();
    const onSubmit = (data: Inputs) => {
        editServer(serverID, data.name, data.description);
    };

    const onIconFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files != null) {
            updateServerIcon(serverID, e.target.files[0]);
        }
    };

    const onBannerFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files != null) {
            updateServerBanner(serverID, e.target.files[0]);
        }
    };


    const server = owned_servers[serverID];

    return (
        <div className="w-full bg-gray-800">
            <form className="select-none flex flex-col items-center p-4" onSubmit={handleSubmit(onSubmit)}>
                <div className="w-full mb-4">
                    <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                        Edit server name
                    </label>
                    <input
                        className={`shadow appearance-none border ${errors.name ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                        id="name"
                        name="name"
                        type="text"
                        placeholder="Change your server name"
                        defaultValue={server.name}
                        ref={register({
                            required: true,
                            minLength: 4
                        })}
                    />
                    {errors.name && <p className="text-red-500 text-xs italic">Please enter a valid server name.</p>}
                </div>
                <div className="w-full mb-4">
                    <label className="block text-white text-sm font-bold mb-2" htmlFor="name">
                        Edit server description
                    </label>
                    <input
                        className={`shadow appearance-none border ${errors.description ? 'border-red-500' : ''} rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
                        id="description"
                        name="description"
                        type="text"
                        placeholder="Change your server description"
                        defaultValue={server.description}
                        ref={register({
                            required: true,
                        })}
                    />
                    {errors.description && <p className="text-red-500 text-xs italic">Please enter a description.</p>}
                </div>
                <button
                    className="bg-blue-500 hover:bg-green-700 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="submit"
                >
                    Save Changes
                </button>
            </form>
            <div className="w-full mb-4 p-4">
                <div className="block text-white text-sm font-bold mb-2">
                    Change Server Image
                </div>
                <label htmlFor="server-icon">
                    <input
                        type="file"
                        className="cursor-pointer absolute block h-0 w-0 opacity-0 top-0 right-0"
                        onChange={onIconFileChange}
                        name="server-icon"
                        id="server-icon"
                        accept="image/*"
                    />
                    <img
                        src={server.icon}
                        className="h-24 w-24 self-center rounded-full cursor-pointer hover:bg-black hover:opacity-50"
                        alt={`${server.name}-server-icon`}
                        title="Click to change Server Icon"
                    />
                </label>
            </div>

            <div className="w-full mb-4 p-4">
                <div className="block text-white text-sm font-bold mb-2">
                    Change Server Banner
                </div>
                <label htmlFor="server-banner">
                    <input
                        type="file"
                        className="cursor-pointer absolute block h-0 w-0 opacity-0 top-0 right-0"
                        onChange={onBannerFileChange}
                        name="server-banner"
                        id="server-banner"
                        accept="image/*"
                    />
                    <img
                        src={server.banner}
                        className="self-center cursor-pointer max-w-1/2 hover:bg-black hover:opacity-50 border-gray-500 border-2"
                        alt={`${server.name}-server-banner`}
                        title="Click to change Server Banner"
                    />
                </label>
            </div>
        </div>
    );
};

export default connector(ServerSettingsMain);
