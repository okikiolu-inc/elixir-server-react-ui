import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import Popup from "reactjs-popup";

import { deleteServer } from '../../reducers/Servers';


const mapDispatch = {
    deleteServer,
};

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
    serverID: string,
};

const ServerSettingsDelete: React.FC<Props> = ({
    deleteServer,
    serverID,
}) => {

    const [showModal, setShowModal] = useState(false);

    const history = useHistory();

    return (
        <>
            <div className="w-full select-none flex flex-col text-white bg-gray-800 items-center lg:items-start p-4">
                <button
                    className="bg-red-700 hover:bg-red-500 text-white my-2 w-full font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    onClick={() => {
                        setShowModal(true);
                    }}
                >
                    Delete Server
                </button>
            </div>
            <Popup
                contentStyle={{
                    height: '33%',
                    boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
                    backgroundColor: 'rgba(45, 55, 72, 1)',
                    border: 'none',
                    borderRadius: '0.5rem',
                    padding: '2.5rem',
                }}
                overlayStyle={{
                    backdropFilter: 'blur(6px)',
                }}
                open={showModal}
                onClose={() => {
                    setShowModal(false);
                }}
                modal
                closeOnDocumentClick
            >
                <div className="h-full w-full select-none flex flex-col font-bold text-white bg-gray-800 items-center justify-center">
                    <span className="mb-2 text-lg">Are you Sure?</span>
                    <span className="mb-2 text-lg">This can NEVER be undone?</span>
                    <span className="mb-6 text-lg">Are you Really Really Sure?</span>
                    <div className="flex items-center w-full">
                        <button
                            className="bg-blue-700 hover:bg-blue-500 m-2 w-full py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            onClick={() => {
                                setShowModal(false);
                            }}
                        >
                            Cancel
                        </button>
                        <button
                            className="bg-red-700 hover:bg-red-500 m-2 w-full py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            onClick={() => {
                                deleteServer(serverID);
                                history.push("/@home");
                            }}
                        >
                            DELETE!
                        </button>
                    </div>
                </div>
            </Popup>
        </>
    );
};

export default connector(ServerSettingsDelete);
