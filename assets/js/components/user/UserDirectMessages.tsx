import React, { useState } from 'react';
import { useHistory, useParams } from "react-router-dom";
    

const UserDirectMessages: React.FC = () => {
    const senders = [];
    for (let index = 0; index < 10; index++) {
        senders.push({
            sendername: 'Test DM',
            userid: String(index),
            unread: true,
        });        
    }
    const senders_2 = new Array(10).fill({
        sendername: 'Test DM',
        userid: '24232343456-3224324-is11234',
        unread: false,
    });
    senders.push(...senders_2);

    const history = useHistory();
    let { friendID } = useParams();

    /*
        A direct message simply is a channel with...
    
    */

    return (
        <main className="flex-1 overflow-y-auto scrollbar-w-2 scrolling-touch bg-gray-700">
            <div
                className="py-1 pl-1 my-2 mx-1 rounded-md flex items-center justify-between"
            >
                <span className="text-sm text-gray-500 select-none">Direct Messages</span>
                <svg className="h-4 w-4 mr-2 cursor-pointer text-gray-500 hover:text-gray-200" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 4v16m8-8H4" />
                </svg>
            </div>
            {senders.map((sender, idx) => {
                return (
                    <div
                        key={`server-room-${idx}`}
                        className={`py-1 pl-1 my-2 mx-1 ${sender.userid === friendID ? 'bg-gray-600' : ''} hover:bg-gray-600 cursor-pointer rounded-md flex items-center justify-between ${sender.unread === true ? 'text-gray-200' : 'text-gray-500'}`}
                        onClick={() => {
                            history.push(`/@home/${sender.userid}`);
                        }}
                    >
                        <div className="flex flex-row items-center">
                            <img
                                src="/images/test/test_image.png"
                                className="h-8 w-8 rounded-full cursor-pointer self-center mr-2"
                            />
                            <span className="truncate text-sm">{sender.sendername}</span>
                        </div>
                        {sender.unread === true && (
                            <svg className="h-4 w-4 mr-2 justify-end" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                            </svg>
                        )}
                    </div>
                );
            })}
        </main>
    );
};

export default UserDirectMessages;