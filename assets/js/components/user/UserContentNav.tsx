import React, { useState } from 'react';

import UserDirectMessages from './UserDirectMessages';
import ServerFooter from '../server/ServerFooter';

const UserContentNav: React.FC = () => {

    return (
        <div className="md:w-1/4 lg:w-3/20 bg-gray-500 flex flex-col shadow">
            <UserDirectMessages />
            <ServerFooter />
        </div>
    );
};

export default UserContentNav;