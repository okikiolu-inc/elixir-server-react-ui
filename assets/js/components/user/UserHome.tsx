import React, { useState } from 'react';

const UserHome: React.FC = () => {

    const backgroundImageUrl: string = '/images/undraw_moonlight_5ksn.png';

    return (
        <main className="flex-1 overflow-y-auto scrollbar-w-2 scrolling-touch bg-gray-800" style={{
            backgroundImage: `url(${backgroundImageUrl})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
        }}>
            
        </main>
    );
};

export default UserHome;