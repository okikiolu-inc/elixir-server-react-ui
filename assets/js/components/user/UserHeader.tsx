import React from 'react';
import { useLocation } from 'react-router-dom';
import Popup from "reactjs-popup";


const UserHeader: React.FC = () => {
    const location = useLocation();
    const pathname = location.pathname;
    const sendername = pathname === '/@home' ? 'Home' : 'Test User';

    const contentStyle: React.CSSProperties = {
        borderRadius: '0.5rem',
        color: 'white',
        backgroundColor: 'rgba(26, 32, 44, 1)',
        padding: 10,
        userSelect: "none",
        width: 'fit-content',
        border: 'none',
        boxShadow: 'none',
    };
    const arrowStyle: React.CSSProperties = {
        backgroundColor: 'rgba(26, 32, 44, 1)',
        color: 'rgba(26, 32, 44, 1)',
    };

    return (
        <header className="h-12 bg-gray-800 select-none shadow-lg flex flex-row justify-between">
            <div
                className="py-1 pl-1 my-2 ml-2 rounded-md flex-none flex items-center"
            >
                <svg className="h-6 w-6 mr-1 text-gray-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207" />
                </svg>
                <span className="text-gray-500">{sendername}</span>
                <div className="border-solid border-l border-1 ml-2 h-6 border-gray-700 self-center" />
            </div>
            <div
                className="py-1 pl-2 my-2 flex items-center"
            >
                <div className="border-solid border-l border-1 mr-1 h-6 border-gray-700 self-center" />
                <Popup
                    trigger={
                        <svg className="h-6 w-6 mr-1 text-gray-500 hover:text-gray-200 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13" />
                        </svg>
                    }
                    position="bottom center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    on="hover"
                >
                    <span>Pinned</span>
                </Popup>
                <Popup
                    trigger={
                        <svg className="h-6 w-6 mr-3 text-gray-500 hover:text-gray-200 cursor-pointer" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    }
                    position="bottom center"
                    contentStyle={contentStyle}
                    arrowStyle={arrowStyle}
                    on="hover"
                >
                    <span>Help</span>
                </Popup>
            </div>
        </header>
    );
};

export default UserHeader;