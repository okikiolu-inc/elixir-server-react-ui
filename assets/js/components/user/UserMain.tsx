import React, { useState } from 'react';

import UserHeader from './UserHeader';
import UserHome from './UserHome';
import ChatMain from '../chat/ChatMain';
import { ChatTypes } from '../chat/ChatTypes';
import ChatBox from '../chat/ChatBox';


import {
    useParams
} from "react-router-dom";

const UserMain: React.FC = () => {
    let { friendID } = useParams();

    return (
        <div className="w-3/4 lg:w-17/20 flex flex-col">
            <UserHeader />
            {
                friendID == null ?
                <UserHome /> :
                <>
                    <ChatMain type={ChatTypes.Direct} />
                    <ChatBox />
                </>
            }
        </div>
    );
};

export default UserMain;