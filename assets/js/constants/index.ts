const isProduction = process.env.NODE_ENV === 'production';

interface constants {
    APP_URL: string,
    APP_API_URL: string,
    LOCAL_STORAGE_USER: string,
    LOCAL_STORAGE_SERVER: string,
    LOCAL_STORAGE_CHANNEL: string,
    LOCAL_STORAGE_MMESSAGE: string,
    LOCAL_STORAGE_SETTINGS: string,
    COLOR: string[],
    BORDER_COLOR: { [color: string]: string },
    PILL_COLOR: { [color: string]: string },
    TEXT_COLOR: { [color: string]: string },
    PERMISSIONS: { [permission: string]: string},
}

const prodConstants = {
    APP_URL: '',
};

const devConstants = {
    APP_URL: 'http://localhost:4000',
};

const allConstants = {
    APP_API_URL: 'api/v1',
    LOCAL_STORAGE_USER: 'USER',
    LOCAL_STORAGE_SERVER: 'SERVER',
    LOCAL_STORAGE_CHANNEL: 'CHANNEL',
    LOCAL_STORAGE_MMESSAGE: 'MESSAGE',
    LOCAL_STORAGE_SETTINGS: 'SETTINGS',
    COLOR: [ 'orange', 'yellow', 'blue', 'red', 'green', 'white'],
    BORDER_COLOR: {
        orange: 'border-orange-600',
        yellow: 'border-yellow-600',
        blue: 'border-blue-600',
        red: 'border-red-600',
        green: 'border-green-600',
        white: 'border-white',
        black: 'border-black', // special
        purple: 'border-purple-600' // special
    },
    PILL_COLOR: {
        orange: 'bg-orange-600',
        yellow: 'bg-yellow-600',
        blue: 'bg-blue-600',
        red: 'bg-red-600',
        green: 'bg-green-600',
        white: 'bg-white',
        black: 'bg-black', // special
        purple: 'bg-purple-600', // special
    },
    TEXT_COLOR: {
        orange: 'text-orange-600',
        yellow: 'text-yellow-600',
        blue: 'text-blue-600',
        red: 'text-red-600',
        green: 'text-green-600',
        white: 'text-white',
        black: 'text-black', // special
        purple: 'text-purple-600', // special
    },
    PERMISSIONS: {
        'administrator': 'Administrator',
        'manage_server': 'Manage Server',
        'manage_roles': 'Manage Roles',
        'manage_channels': 'Manage Channels',
        'create_invite': 'Create Invite',
        'send_messages': 'Send Messages',
        'embed_links': 'Embed Links',
        'attach_files': 'Attach Files',
        'ban_members': 'Ban Members',
    },
};

let exportConstants: constants;

if (isProduction) {
    exportConstants = {
        ...allConstants,
        ...prodConstants
    };
}
else {
    exportConstants = {
        ...allConstants,
        ...devConstants
    };
}

export default exportConstants;