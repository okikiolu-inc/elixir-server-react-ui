import { createStore, applyMiddleware, compose, Store } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';
import constants from '../constants';

const persistedState = {
    User: localStorage.getItem(constants.LOCAL_STORAGE_USER)
        ? JSON.parse(localStorage.getItem(constants.LOCAL_STORAGE_USER) || '{}')
        : {},
    Servers: localStorage.getItem(constants.LOCAL_STORAGE_SERVER)
        ? JSON.parse(localStorage.getItem(constants.LOCAL_STORAGE_SERVER) || '{}')
    : {},
    Channels: localStorage.getItem(constants.LOCAL_STORAGE_CHANNEL)
        ? JSON.parse(localStorage.getItem(constants.LOCAL_STORAGE_CHANNEL) || '{"channels": {}, "lastchannel": {}}')
        : { channels: {}, lastchannel: {}},
};

if (!persistedState.Servers.servers) persistedState.Servers.servers = {};
if (!persistedState.Servers.owned_servers) persistedState.Servers.owned_servers = {};
if (!persistedState.Servers.server_members) persistedState.Servers.server_members = {};
if (!persistedState.Servers.member_info) persistedState.Servers.member_info = {};


// Gotta JSON.stringify before you put in local storage

let store: Store;
if (process.env.NODE_ENV === 'development' && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
    const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    store = createStore(
        reducer,
        persistedState,
        composeEnhancers(
            applyMiddleware(thunk),
        ),
    );
}
else {
    store = createStore(
        reducer,
        persistedState,
        applyMiddleware(thunk),
    );
}
export default store;