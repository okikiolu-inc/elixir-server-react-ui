interface Friend {
    id: bigint,
    name: string,
};

export interface FriendsState {
    friends: Friend[]
};