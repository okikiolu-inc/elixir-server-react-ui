import AuthREST from '../api/AuthRest';
import AuthFile from '../api/AuthFile';
import { socketLogout } from '../socket';
import REST from '../api/Rest';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import constants from '../constants';

export interface UserState {
    usertoken: string,
    userid: string,
    email: string,
    avatar: string,
    username: string,
    userhash: string,
    status: string,
    error: string | null,
    inflight: boolean,
}

const REFRESH_TOKEN = 'REFRESH_TOKEN';
const AUTH_SUCCESS = 'AUTH_SUCCESS';
const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
const AUTH_ERROR = 'AUTH_ERROR';
const SET_INFLIGHT = 'SET_INFLIGHT';
const UPDATE_USER = 'UPDATE_USER';
const UPDATE_AVATAR = 'UPDATE_AVATAR';


interface AuthSuccess {
  type: typeof AUTH_SUCCESS
  id: string,
  email: string,
  avatar: string,
  userhash: string,
  username: string,
  status: string,
  token: string,
};

interface AuthSuccessMessage {
  id: string,
  email: string,
  avatar: string,
  userhash: string,
  username: string,
  token: string,
  status: string,
  error?: string,
};

interface GetUserSuccess {
  type: typeof GET_USER_SUCCESS
  email: string,
  avatar: string,
  userhash: string,
  username: string,
  status: string,
};


interface GetUserSuccessMessage {
  email: string,
  avatar: string,
  userhash: string,
  username: string,
  status: string,
  error?: string,
};

interface UpdateAvatar {
  type: typeof UPDATE_AVATAR,
  avatar: string,
};

interface UpdateAvatarMessage {
  avatar: string,
  error?: string,
};

interface RefreshToken {
  type: typeof REFRESH_TOKEN
};

interface LogoutSuccess {
  type: typeof LOGOUT_SUCCESS,
}

interface AuthError {
  type: typeof AUTH_ERROR,
  error: string,
}

interface SetInFlight {
  type: typeof SET_INFLIGHT,
  inflight: boolean,
}

export type UserActionTypes = AuthSuccess | AuthError | GetUserSuccess | RefreshToken | LogoutSuccess | SetInFlight | UpdateAvatar;

export function login(email: string, password: string): ThunkAction<any, any, any, Action> {
  const data = {
    email,
    password,
  };
  
  return (dispatch) => {
    dispatch(setInflight(true));
    REST('POST', 'auth/signin', data)
    .then((response) => {
      dispatch(setInflight(false));
      if (response.status >= 400) {
        return {
          error: 'Please enter a valid username or password',
        } as AuthSuccessMessage;
      }
      return response.json() as Promise<AuthSuccessMessage>;
    })
    .then((response) => {
      if (response.error != null) {
        dispatch(authError(
          response.error,
        ));
      }
      else {
        const prevState = JSON.parse(localStorage.getItem(
          constants.LOCAL_STORAGE_USER) || '{}'
        );
        prevState['usertoken'] = response.token;
        prevState['userid'] = response.id;
        localStorage.setItem(constants.LOCAL_STORAGE_USER, JSON.stringify(prevState));

        dispatch(authSuccess(
          response.id,
          response.token,
          response.email,
          response.username,
          response.userhash,
          response.avatar,
          response.status,
        ));
      }
    })
    .catch((data) => {
      dispatch(setInflight(false));
    });
  };
}

export function register(email: string, username: string, password: string): ThunkAction<any, any, any, Action> {
  const data = {
    email,
    username,
    password,
  };
  return (dispatch) => {
    dispatch(setInflight(true));
    REST('POST', 'auth/signup', data)
    .then((response) => {
      dispatch(setInflight(false));
      if (response.status >= 400) {
        return {
          error: 'Please fill out the form properly',
        } as AuthSuccessMessage;
      }
      return response.json() as Promise<AuthSuccessMessage>;
    })
    .then((response) => {
      if (response.error != null) {
        dispatch(authError(
          response.error,
        ));
      }
      else {
        const prevState = JSON.parse(localStorage.getItem(constants.LOCAL_STORAGE_USER) || '{}');
        prevState['usertoken'] = response.token;
        prevState['userid'] = response.id;
        localStorage.setItem(constants.LOCAL_STORAGE_USER, JSON.stringify(prevState));

        dispatch(authSuccess(
          response.id,
          response.token,
          response.email,
          response.username,
          response.userhash,
          response.avatar
        ));
      }
    })
    .catch((data) => {
      dispatch(setInflight(false));
    });
  };
};

export function getUser(userid: string): ThunkAction<any, any, any, Action> {
  return (dispatch) => {
    dispatch(setInflight(true));
    AuthREST('GET', `users/${userid}`)
    .then((response) => {
      dispatch(setInflight(false));
      if (response.status >= 400) {
        return {
          error: 'Please fill out the form properly',
        } as GetUserSuccessMessage;
      }
      return response.json() as Promise<GetUserSuccessMessage>;
    })
    .then((response) => {
      if (response.error != null) {
        dispatch(authError(
          response.error,
        ));
      }
      else {
        dispatch(getUserSuccess(
          response.email,
          response.username,
          response.userhash,
          response.avatar,
          response.status
        ));
      }
    })
    .catch(() => {
      dispatch(setInflight(false));
    });
  };
};

export function updateUserStatus(userid: string, status: string): ThunkAction<any, any, any, Action> {
  const data = {
    user: {
      status,
    },
  };

  console.log('updateUserStatus:::', data);

  return (dispatch) => {
    dispatch(setInflight(true));
    AuthREST('PUT', `users/${userid}`, data)
    .then((response) => {
      dispatch(setInflight(false));
      if (response.status >= 400) {
        return {
          error: 'Please fill out the form properly',
        } as GetUserSuccessMessage;
      }
      return response.json() as Promise<GetUserSuccessMessage>;
    })
    .then((response) => {
      if (response.error == null) {
        dispatch(getUserSuccess(
          response.email,
          response.username,
          response.userhash,
          response.avatar,
          response.status
        ));
      }
    })
    .catch(() => {
      dispatch(setInflight(false));
    });
  };
};

export function updateUserAvatar(file: File): ThunkAction<any, any, any, Action> {

  const formData = new FormData();
  formData.append('avatar', file);
 
  return (dispatch) => {
    if (file == null) return null;

    dispatch(setInflight(true));
    AuthFile(`image-upload/avatar`, formData)
      .then((response) => {
        dispatch(setInflight(false));
        if (response.status >= 400) {
          return {
            error: 'Could not upload image',
          } as UpdateAvatarMessage;
        }
        return response.json() as Promise<UpdateAvatarMessage>;
      })
      .then((response) => {
        if (response.error == null) {
          dispatch(updateAvatarSuccess(
            response.avatar,
          ));
        }
      })
      .catch(() => {
        dispatch(setInflight(false));
      });
  };
};

function authSuccess(id: string, token: string, email: string, username: string, userhash: string, avatar: string, status: string): UserActionTypes {
  return {
    type: AUTH_SUCCESS,
    id,
    email,
    avatar,
    userhash,
    username,
    token,
    status,
  }
};

function getUserSuccess(email: string, username: string, userhash: string, avatar: string, status: string): UserActionTypes {
  return {
    type: GET_USER_SUCCESS,
    email,
    avatar,
    userhash,
    username,
    status
  }
};

function authError(error: string): UserActionTypes {
  return {
    type: AUTH_ERROR,
    error,
  }
};

function updateAvatarSuccess(avatar: string): UserActionTypes {
  return {
    type: UPDATE_AVATAR,
    avatar,
  }
};

function setInflight(inflight: boolean): UserActionTypes {
  return {
    type: SET_INFLIGHT,
    inflight,
  }
}

export function refreshToken(): UserActionTypes {
  return {
    type: REFRESH_TOKEN,
  };
};

export function logout(): ThunkAction<any, any, any, Action> {
  return (dispatch) => {
    dispatch(setInflight(true));
    AuthREST('GET', 'auth/logout')
    .then(() => {
      localStorage.removeItem(constants.LOCAL_STORAGE_USER);
      socketLogout();
      dispatch(setInflight(false));
      dispatch(logoutSuccess());
    }).catch(() => {
      localStorage.removeItem(constants.LOCAL_STORAGE_USER);
      socketLogout();
      dispatch(setInflight(false));
      dispatch(logoutSuccess());
    });
  };
};

export function logoutSuccess (): UserActionTypes {
  return {
    type: LOGOUT_SUCCESS,
  };
};

const initialState: UserState = {
    usertoken: '',
    userid: '',
    email: '',
    avatar: '',
    username: '',
    userhash: '',
    status: '',
    error: null,
    inflight: false,
};

export default function UserReducer(
    state = initialState,
  action: UserActionTypes
): UserState {
    switch (action.type) {
        case AUTH_SUCCESS:
            return {
              ...state,
              usertoken: action.token,
              userid: action.id,
              email: action.email,
              avatar: action.avatar,
              username: action.username,
              userhash: action.userhash,
              status: action.status,
              inflight: false,
              error: null,
            };
        case GET_USER_SUCCESS:
            return {
              ...state,
              email: action.email,
              avatar: action.avatar,
              username: action.username,
              userhash: action.userhash,
              status: action.status,
              inflight: false,
              error: null,
            };
        case AUTH_ERROR:
          return {
            ...state,
            inflight: false,
            error: action.error,
          };
        case SET_INFLIGHT:
          return {
            ...state,
            inflight: action.inflight,
          };
        case UPDATE_AVATAR:
          return {
            ...state,
            avatar: action.avatar,
          }
        case LOGOUT_SUCCESS:
          return initialState;
        default:
            return state;
    }
};