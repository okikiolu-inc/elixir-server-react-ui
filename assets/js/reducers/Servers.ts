import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import constants from '../constants';
import AuthREST from '../api/AuthRest';
import AuthFile from '../api/AuthFile';


interface Server {
    server_id: string,
    icon: string,
    name: string,
    banner: string,
    description: string,
};

interface ServerMember {
    username: string,
    avatar: string,
    userhash: string,
};

export interface MemberInfo {
    status: string,
}

export interface ServersState {
    servers: { [server_id: string]: Server },
    owned_servers: { [server_id: string]: Server },
    server_members: { [server_id: string]: ServerMember[] },
    member_info: { [user: string]: MemberInfo} 
}

const SERVER_FETCH_SUCCESS = 'SERVER_FETCH_SUCCESS';
const SERVER_CREATE_SUCCESS = 'SERVER_CREATE_SUCCESS';
const SERVER_EDIT_SUCCESS = 'SERVER_EDIT_SUCCESS';
const SERVER_JOIN_SUCCESS = 'SERVER_JOIN_SUCCESS';
const SERVER_LEAVE_SUCCESS = 'SERVER_LEAVE_SUCCESS';
const SERVER_MEMBERS_SUCCESS = 'SERVER_MEMBERS_SUCCESS';
const SERVER_MEMBER_INFO_SUCCESS = 'SERVER_MEMBER_INFO_SUCCESS';


interface ServerFetchSuccess {
    type: typeof SERVER_FETCH_SUCCESS,
    servers: { [server_id: string]: Server },
    owned_servers: { [server_id: string]: Server },
};

interface ServerCreateSuccess {
    type: typeof SERVER_CREATE_SUCCESS,
    server: Server,
};

interface ServerEditSuccess {
    type: typeof SERVER_EDIT_SUCCESS,
    server: Server,
};

interface ServerJoinSuccess {
    type: typeof SERVER_JOIN_SUCCESS,
    server: Server,
};

interface ServerLeaveSuccess {
    type: typeof SERVER_LEAVE_SUCCESS,
    server_id: string,
};

interface ServerMembersSuccess {
    type: typeof SERVER_MEMBERS_SUCCESS,
    server_id: string,
    server_members: ServerMember[],
};

interface MemberInfoSuccess {
    type: typeof SERVER_MEMBER_INFO_SUCCESS,
    username: string,
    userhash: string,
    member_info: MemberInfo,
};

export type ServersActionTypes = ServerFetchSuccess | ServerCreateSuccess | ServerEditSuccess | ServerJoinSuccess | ServerMembersSuccess | ServerLeaveSuccess | MemberInfoSuccess;

interface ServerFetchSuccessMessage {
    servers: Server[],
    owned_servers: Server[],
};

export interface ServerSuccessMessage {
    server: Server,
    error?: string,
};

export interface UpdateBannerMessage {
    banner: string,
    error?: string,
};

export interface UpdateIconMessage {
    icon: string,
    error?: string,
};

interface ServerMemberSuccessMessage {
    server_members: ServerMember[],
}

interface ShallowUserMessage {
    status: string,
    error?: string,
};

function serverFetchSuccess(servers: { [server_id: string]: Server }, owned_servers: { [server_id: string]: Server }): ServersActionTypes {
    return {
        type: SERVER_FETCH_SUCCESS,
        servers,
        owned_servers,
    };
};

export function serverJoinSuccess(server: Server): ServersActionTypes {
    return {
        type: SERVER_JOIN_SUCCESS,
        server,
    };
};

function serverMemberSuccess(server_id:string, server_members: ServerMember[]): ServersActionTypes {
    return {
        type: SERVER_MEMBERS_SUCCESS,
        server_id,
        server_members,
    };
};

function memberInfoSuccess(username: string, userhash: string, member_info: MemberInfo): ServersActionTypes {
    return {
        type: SERVER_MEMBER_INFO_SUCCESS,
        userhash,
        username,
        member_info,
    };
};


export function fetchServers(): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', 'user-servers')
        .then((response) => {
            return response.json() as Promise<ServerFetchSuccessMessage>;
        })
        .then((data) => {
            const prevState = JSON.parse(localStorage.getItem(
                constants.LOCAL_STORAGE_SERVER) || '{}'
            );
            let servers: { [server_id: string]: Server } = {};
            let owned_servers: { [server_id: string]: Server } = {};
            data.servers.map((server) => {
                servers[server.server_id] = server;
            });
            prevState['servers'] = servers;
            data.owned_servers.map((server) => {
                owned_servers[server.server_id] = server;
            });
            prevState['owned_servers'] = owned_servers;
            localStorage.setItem(constants.LOCAL_STORAGE_SERVER, JSON.stringify(prevState));
            dispatch(serverFetchSuccess(servers, owned_servers));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function createServer(name: string, description: string): ThunkAction<any, any, any, Action> {
    const data = {
        server: {
            name,
            description, 
        },
    };

    return (dispatch) => {
        AuthREST('POST', 'servers', data)
        .then((response) => {
            return response.json() as Promise<ServerSuccessMessage>;
        })
        .then((data) => {
            dispatch(fetchServers());
            // dispatch(serverCreateSuccess(data.server));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function editServer(server_id: string, name: string, description: string): ThunkAction<any, any, any, Action> {
    const data = {
        server: {
            name,
            description, 
        },
    };

    return (dispatch) => {
        AuthREST('PUT', `servers/${server_id}`, data)
        .then((response) => {
            return response.json() as Promise<ServerSuccessMessage>;
        })
        .then((data) => {
            dispatch(fetchServers());
            // dispatch(serverEditSuccess(data.server));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function deleteServer(server_id: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('DELETE', `servers/${server_id}`)
        .then((response) => {
            console.log('response', response);
            dispatch(fetchServers());
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function leaveServer(server_id: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', `server-leave/${server_id}`)
        .then((response) => {
            console.log('server_leave data:', response);
            dispatch(fetchServers());
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function fetchServerMembers(server_id: string): ThunkAction<any, any, any, Action> {    
    return (dispatch) => {
        AuthREST('GET', `server-members/${server_id}`)
        .then((response) => {
            return response.json() as Promise<ServerMemberSuccessMessage>;
        })
        .then((data) => {
            console.log('data', data);
            dispatch(serverMemberSuccess(server_id, data.server_members));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function updateServerBanner(server_id: string, file: File): ThunkAction<any, any, any, Action> {
    const formData = new FormData();
    formData.append('banner', file);

    return (dispatch) => {
        if (file == null) return null;

        AuthFile(`image-upload/server-banner/${server_id}`, formData)
        .then((response) => {
            if (response.status >= 400) {
                return {
                    error: 'Could not upload image',
                } as UpdateBannerMessage;
            }
            return response.json() as Promise<UpdateBannerMessage>;
        })
        .then((response) => {
            if (response.error == null) {
                console.log('response', response);
            }
        })
        .catch(() => {
        });
    };
};

export function updateServerIcon(server_id: string, file: File): ThunkAction<any, any, any, Action> {

    const formData = new FormData();
    formData.append('icon', file);

    return (dispatch) => {
        if (file == null) return null;

        AuthFile(`image-upload/server-icon/${server_id}`, formData)
        .then((response) => {
            if (response.status >= 400) {
                return {
                    error: 'Could not upload image',
                } as UpdateIconMessage;
            }
            return response.json() as Promise<UpdateIconMessage>;
        })
        .then((response) => {
            if (response.error == null) {
                console.log('response', response);
            }
        })
        .catch(() => {
        });
    };
};

export function getShallowUserInfo(username: string, userhash: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', `shallow-user/${username}/${userhash}`)
            .then((response) => {
                if (response.status >= 400) {
                    return {
                        error: 'Could not upload image',
                    } as ShallowUserMessage;
                }
                return response.json() as Promise<ShallowUserMessage>;
            })
            .then((response) => {
                if (response.error == null) {
                    dispatch(memberInfoSuccess(
                        username,
                        userhash,
                        response,
                    ));
                }
            })
            .catch(() => {
                // dispatch(setInflight(false));
            });
    };
};


const initialState: ServersState = {
    servers: {},
    owned_servers: {},
    server_members: {},
    member_info: {}
};

export default function ServersReducer(
    state = initialState,
    action: ServersActionTypes
): ServersState {
    switch (action.type) {
        case SERVER_FETCH_SUCCESS:
            return {
                ...state,
                servers: action.servers,
                owned_servers: action.owned_servers,
            };
        case SERVER_CREATE_SUCCESS:
        case SERVER_EDIT_SUCCESS:
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.server.server_id]: action.server,
                },
            };
        case SERVER_JOIN_SUCCESS:
            // dont join a server you own
            if (state.owned_servers[action.server.server_id] != null) {
                return state;
            }
            return {
                ...state,
                servers: {
                    ...state.servers,
                    [action.server.server_id]: action.server,
                },
            };
        case SERVER_LEAVE_SUCCESS:
            return state;
        case SERVER_MEMBERS_SUCCESS:
            return {
                ...state,
                server_members: {
                    ...state.server_members,
                    [action.server_id]: action.server_members,
                },
            };
        case SERVER_MEMBER_INFO_SUCCESS:
            return {
                ...state,
                member_info: {
                    ...state.member_info,
                    [`${action.username}#${action.userhash}`]: action.member_info,
                },
            }
        default:
            return state;
    }
};