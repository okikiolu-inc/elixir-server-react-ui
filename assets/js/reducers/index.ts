import { combineReducers } from 'redux';
import ServerHomeReducer from './ServerHome';
import UserReducer from './User';
import ServerReducer from './Servers';
import ServerInvitesReducer from './ServerInvites';
import ChannelsReducer from './Channels';
import MessagesReducer from './Messages';
import ServerRolesReducer from './ServerRoles';

export default combineReducers({
    ServerHome: ServerHomeReducer,
    User: UserReducer,
    Servers: ServerReducer,
    Channels: ChannelsReducer,
    Messages: MessagesReducer,
    ServerInvites: ServerInvitesReducer,
    ServerRoles: ServerRolesReducer,
});