import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import constants from '../constants';
import AuthREST from '../api/AuthRest';


export interface Channel {
    channel_id: string,
    server_id: string,
    name: string,
    description: string,
}


export interface ChannelsState {
    channels: { [server_id: string]: { [channel_id: string]: Channel } },
    lastchannel: { [server_id: string]: string },
    inflight: boolean,
};

const CHANNEL_FETCH_SUCCESS = 'CHANNEL_FETCH_SUCCESS';
const CHANNEL_CREATE_SUCCESS = 'CHANNEL_CREATE_SUCCESS';
const CHANNEL_INFLIGHT = 'CHANNEL_INFLIGHT';
const CHANNEL_LAST_CHANNEL = 'CHANNEL_LAST_CHANNEL';

interface ChannelFetchSuccess {
    type: typeof CHANNEL_FETCH_SUCCESS,
    server_id: string,
    channels: { [channel_id: string]: Channel },
};

interface ChannelCreateSuccess {
    type: typeof CHANNEL_CREATE_SUCCESS,
    server_id: string,
    channel: Channel,
};

interface ChannelInflight {
    type: typeof CHANNEL_INFLIGHT,
    inflight: boolean,
};

interface LastChannel {
    type: typeof CHANNEL_LAST_CHANNEL,
    server_id: string,
    lastchannel: string,
};

export type ChannelsActionTypes = ChannelFetchSuccess | ChannelCreateSuccess | ChannelInflight | LastChannel;

interface ChannelFetchSuccessMessage {
    channels: Channel[],
};

interface ChannelSuccessMessage {
    channel: Channel,
};

function channelFetchSuccess(server_id: string, channels: { [channel_id: string]: Channel }): ChannelsActionTypes {
    return {
        type: CHANNEL_FETCH_SUCCESS,
        server_id,
        channels,
    };
};

function channelCreateSuccess(server_id: string, channel: Channel): ChannelsActionTypes {
    return {
        type: CHANNEL_CREATE_SUCCESS,
        server_id,
        channel,
    };
};

function setInflight(inflight: boolean): ChannelsActionTypes {
    return {
        type: CHANNEL_INFLIGHT,
        inflight,
    };
};

export function setLastChannel(server_id: string, lastchannel: string): ChannelsActionTypes {

    const prevState = JSON.parse(localStorage.getItem(
        constants.LOCAL_STORAGE_CHANNEL) || '{"channels": {}, "lastchannel": {}}'
    );
    if (!prevState['lastchannel']) {
        prevState['lastchannel'] = {};
    }
    prevState['lastchannel'][server_id] = lastchannel;
    localStorage.setItem(constants.LOCAL_STORAGE_CHANNEL, JSON.stringify(prevState));

    return {
        type: CHANNEL_LAST_CHANNEL,
        lastchannel,
        server_id,
    };
};


export function fetchChannels(serverid: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', `server-channels/${serverid}`)
        .then((response) => {
            return response.json() as Promise<ChannelFetchSuccessMessage>;
        })
        .then((data) => {
            const prevState = JSON.parse(localStorage.getItem(
                constants.LOCAL_STORAGE_CHANNEL) || '{"channels": {}}'
            );
            let channels: { [channel_id: string]: Channel } = {};
            data.channels.map((channel) => {
                channels[channel.channel_id] = channel;
            });
            prevState['channels'][serverid] = channels;
            localStorage.setItem(constants.LOCAL_STORAGE_CHANNEL, JSON.stringify(prevState));
            dispatch(channelFetchSuccess(serverid, channels));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};


export function createChannel(name: string, description: string, server_id: string): ThunkAction<any, any, any, Action> {
    const data = {
        channel: {
            name,
            server_id,
            description,
        },
    };

    return (dispatch) => {
        dispatch(setInflight(true));
        AuthREST('POST', 'channels', data)
        .then((response) => {
            dispatch(setInflight(false));
            return response.json() as Promise<ChannelSuccessMessage>;
        })
        .then((data) => {
            dispatch(fetchChannels(server_id));
        })
        .catch((data) => {
            dispatch(setInflight(false));
            console.error('data', data);
        });
    };
};


export function editChannel(server_id: string, channel_id: string, name: string, description: string): ThunkAction<any, any, any, Action> {
    const data = {
        channel: {
            name,
            description,
        },
    };

    return (dispatch) => {
        dispatch(setInflight(true));
        AuthREST('PUT', `channels/${server_id}/${channel_id}`, data)
        .then(() => {
            dispatch(setInflight(false));
            dispatch(fetchChannels(server_id));
        })
        .catch((data) => {
            dispatch(setInflight(false));
            console.error('data', data);
        });
    };
};

export function deleteChannel(server_id: string, channel_id: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        dispatch(setInflight(true));
        AuthREST('DELETE', `channels/${server_id}/${channel_id}`)
        .then(() => {
            dispatch(setInflight(false));
            dispatch(fetchChannels(server_id));
        })
        .catch((data) => {
            dispatch(setInflight(false));
            console.error('data', data);
        });
    };
};

const initialState: ChannelsState = {
    channels: {},
    lastchannel: {},
    inflight: false,
};

export default function ChannelsReducer(
    state = initialState,
    action: ChannelsActionTypes
): ChannelsState {
    switch (action.type) {
        case CHANNEL_FETCH_SUCCESS:
            return {
                ...state,
                channels: {
                    ...state.channels,
                    [action.server_id]: {
                        ...action.channels,
                    },
                },
            };
        case CHANNEL_CREATE_SUCCESS:
            return {
                ...state,
                channels: {
                    ...state.channels,
                    [action.server_id]: {
                        ...state.channels[action.server_id],
                        [action.channel.channel_id]: action.channel,
                    },
                },
            };
        case CHANNEL_INFLIGHT:
            return {
                ...state,
                inflight: action.inflight,
            };
        case CHANNEL_LAST_CHANNEL:
            return {
                ...state,
                lastchannel: {
                    ...state.lastchannel,
                    [action.server_id]: action.lastchannel,
                },
            };
        default:
            return state;
    }
};