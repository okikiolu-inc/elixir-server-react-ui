export interface ServerHomeState {
    showMemberList: boolean
}

const TOGGLE_MEMBER_LIST = 'TOGGLE_MEMBER_LIST';

interface ToggleMemberList {
    type: typeof TOGGLE_MEMBER_LIST
};

export type ServerHomeActionTypes = ToggleMemberList;

export function toogleMemberList(): ServerHomeActionTypes {
    return {
        type: TOGGLE_MEMBER_LIST,
    }
};

const initialState: ServerHomeState = {
    showMemberList: false,
};

export default function ServerHomeReducer(
    state = initialState,
    action: ServerHomeActionTypes
): ServerHomeState {
    switch (action.type) {
        case TOGGLE_MEMBER_LIST:
            return {
                showMemberList: !state.showMemberList,
            };
        default:
            return state;
    }
};