import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import constants from '../constants';
import AuthREST from '../api/AuthRest';


interface ServerInvite {
    server_id: string,
    invite_id: string,
    join_count: number,
    duration: number,
    created: string,
}


export interface ServerInvitesState {
    server_invites: { [server_id: string]: ServerInvite[] },
}

const SERVER_INVITE_FETCH_SUCCESS = 'SERVER_INVITE_FETCH_SUCCESS';
const SERVER_CREATE_INVITE_SUCCESS = 'SERVER_CREATE_INVITE_SUCCESS';
const SERVER_DELETE_INVITE_SUCCESS = 'SERVER_DELETE_INVITE_SUCCESS';


interface ServerInviteFetchSuccess {
    type: typeof SERVER_INVITE_FETCH_SUCCESS,
    server_id: string,
    server_invites: ServerInvite[],
};

interface ServerCreateInviteSuccess {
    type: typeof SERVER_CREATE_INVITE_SUCCESS,
    server_id: string,
    server_invite: ServerInvite,
};

interface ServerDeleteInviteSuccess {
    type: typeof SERVER_DELETE_INVITE_SUCCESS,
    server_id: string,
    invite_id: string,
};

export type ServersActionTypes = ServerCreateInviteSuccess | ServerDeleteInviteSuccess | ServerInviteFetchSuccess;


interface ServerInviteFetchMessage {
    server_invites: ServerInvite[],
};

interface ServerInviteCreateMessage {
    server_invite: ServerInvite,
};


function serverInviteFetchSuccess(server_id: string, server_invites: ServerInvite[]): ServersActionTypes {
    return {
        type: SERVER_INVITE_FETCH_SUCCESS,
        server_id,
        server_invites,
    };
};

function serverInviteCreateSuccess(server_id: string, server_invite: ServerInvite): ServersActionTypes {
    return {
        type: SERVER_CREATE_INVITE_SUCCESS,
        server_id,
        server_invite,
    };
};

function serverInviteDeleteSuccess(server_id: string, invite_id: string): ServersActionTypes {
    return {
        type: SERVER_DELETE_INVITE_SUCCESS,
        server_id,
        invite_id,
    };
};


export function fetchServerInvites(server_id: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', `list-invites/${server_id}`)
        .then((response) => {
            return response.json() as Promise<ServerInviteFetchMessage>;
        })
        .then((data) => {
            dispatch(serverInviteFetchSuccess(server_id, data.server_invites));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function createServerInvite(server_id: string): ThunkAction<any, any, any, Action> {
    const data = {
        server_invite: {
            server_id,
            duration: 0,
        },
    };

    return (dispatch) => {
        AuthREST('POST', 'server-invites', data)
        .then((response) => {
            return response.json() as Promise<ServerInviteCreateMessage>;
        })
        .then((data) => {
            dispatch(serverInviteCreateSuccess(server_id, data.server_invite));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function deleteServerInvite(server_id: string, invite_id: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('DELETE', `server-invites/${server_id}/${invite_id}`)
        .then(() => {
            dispatch(serverInviteDeleteSuccess(server_id, invite_id));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

const initialState: ServerInvitesState = {
    server_invites: {},
};

export default function ServersReducer(
    state = initialState,
    action: ServersActionTypes
): ServerInvitesState {
    switch (action.type) {
        case SERVER_INVITE_FETCH_SUCCESS:
        return {
            ...state,
            server_invites: {
                ...state.server_invites,
                [action.server_id]: action.server_invites,
            },
        };
        case SERVER_CREATE_INVITE_SUCCESS:
            return {
                ...state,
                server_invites: {
                    ...state.server_invites,
                    [action.server_id]: state.server_invites[action.server_id]
                        ? [...state.server_invites[action.server_id], action.server_invite]
                        : [action.server_invite],
                },
            };
        case SERVER_DELETE_INVITE_SUCCESS:
            if (state.server_invites[action.server_id] == null) {
                return state;
            }

            const invites = state.server_invites[action.server_id];
            for (let index = 0; index < invites.length; index++) {
                const element = invites[index];
                if (element.invite_id === action.invite_id) {
                    invites.splice(index, 1);
                    break;
                }
            }

            return {
                ...state,
                server_invites: {
                    ...state.server_invites,
                    [action.server_id]: invites,
                },
            };
        default:
            return state;
    }
};