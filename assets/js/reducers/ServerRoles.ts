import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import constants from '../constants';
import AuthREST from '../api/AuthRest';


interface ServerRole {
    server_id: string,
    role_id: string,
    name: string,
    color: string,
    permissions: string[],
}


export interface ServerRolesState {
    roles: { [server_id: string]: ServerRole[] },
}

const SERVER_ROLE_FETCH_SUCCESS = 'SERVER_ROLE_FETCH_SUCCESS';
const SERVER_CREATE_ROLE_SUCCESS = 'SERVER_CREATE_ROLE_SUCCESS';
const SERVER_UPDATE_ROLE_SUCCESS = 'SERVER_UPDATE_ROLE_SUCCESS';
const SERVER_DELETE_ROLE_SUCCESS = 'SERVER_DELETE_ROLE_SUCCESS';


interface ServerRoleFetchSuccess {
    type: typeof SERVER_ROLE_FETCH_SUCCESS,
    server_id: string,
    roles: ServerRole[],
};

interface ServerCreateRoleSuccess {
    type: typeof SERVER_CREATE_ROLE_SUCCESS,
    server_id: string,
    role: ServerRole,
};

interface ServerUpdateRoleSuccess {
    type: typeof SERVER_UPDATE_ROLE_SUCCESS,
    server_id: string,
    role: ServerRole,
};

interface ServerDeleteRoleSuccess {
    type: typeof SERVER_DELETE_ROLE_SUCCESS,
    server_id: string,
    role_id: string,
};

export type ServersActionTypes = ServerCreateRoleSuccess | ServerUpdateRoleSuccess | ServerDeleteRoleSuccess | ServerRoleFetchSuccess;


interface ServerRoleFetchMessage {
    roles: ServerRole[],
};

interface ServerRoleCreateMessage {
    role: ServerRole,
};


function serverRoleFetchSuccess(server_id: string, roles: ServerRole[]): ServersActionTypes {
    return {
        type: SERVER_ROLE_FETCH_SUCCESS,
        server_id,
        roles,
    };
};

function serverRoleCreateSuccess(server_id: string, role: ServerRole): ServersActionTypes {
    return {
        type: SERVER_CREATE_ROLE_SUCCESS,
        server_id,
        role,
    };
};

function serverRoleUpdateSuccess(server_id: string, role: ServerRole): ServersActionTypes {
    return {
        type: SERVER_UPDATE_ROLE_SUCCESS,
        server_id,
        role,
    };
};


function serverRoleDeleteSuccess(server_id: string, role_id: string): ServersActionTypes {
    return {
        type: SERVER_DELETE_ROLE_SUCCESS,
        server_id,
        role_id,
    };
};

/*
    get "/roles/:server_id", RoleController, :index
    post "/roles/:server_id", RoleController, :create
    put "/roles/:server_id/:role_id", RoleController, :update
    delete "/roles/:server_id/:role_id", RoleController, :delete

    get "/user-roles/:server_id/:user_id", UserRoleController, :index
    post "/user-roles/:server_id/:user_id", UserRoleController, :create
    get "/user-permissions/:server_id/:user_id", UserRoleController, :show
*/


export function fetchServerRoles(server_id: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', `roles/${server_id}`)
        .then((response) => {
            return response.json() as Promise<ServerRoleFetchMessage>;
        })
        .then((data) => {
            console.log('fetchServerRoles:::', data);
            dispatch(serverRoleFetchSuccess(server_id, data.roles));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function createServerRole(server_id: string, name: string, color: string, permissions: (string | boolean)[]): ThunkAction<any, any, any, Action> {
    const data = {
        role: {
            name,
            permissions,
            color,
        },
    };

    return (dispatch) => {
        AuthREST('POST', `roles/${server_id}`, data)
        .then((response) => {
            return response.json() as Promise<ServerRoleCreateMessage>;
        })
        .then((data) => {
            console.log('serverRoleCreateSuccess', data);
            dispatch(serverRoleCreateSuccess(server_id, data.role));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function updateServerRole(server_id: string, role_id: string, name: string, color: string, permissions: (string | boolean)[]): ThunkAction<any, any, any, Action> {
    const _permissions = permissions as string[];
    const role = {
        server_id,
        role_id,
        name,
        permissions: _permissions,
        color,
    }
    const data = {
        role: {
            name,
            permissions,
            color,
        },
    };

    return (dispatch) => {
        AuthREST('PUT', `roles/${server_id}/${role_id}`, data)
        .then((response) => {
            console.log('updateServerRole::response::', response);
            dispatch(serverRoleUpdateSuccess(server_id, role));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function deleteServerRole(server_id: string, role_id: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('DELETE', `roles/${server_id}/${role_id}`)
        .then(() => {
            dispatch(serverRoleDeleteSuccess(server_id, role_id));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

const initialState: ServerRolesState = {
    roles: {},
};

export default function ServersRolesReducer(
    state = initialState,
    action: ServersActionTypes
): ServerRolesState {
    switch (action.type) {
        case SERVER_ROLE_FETCH_SUCCESS:
        return {
            ...state,
            roles: {
                ...state.roles,
                [action.server_id]: action.roles,
            },
        };
        case SERVER_CREATE_ROLE_SUCCESS:
            return {
                ...state,
                roles: {
                    ...state.roles,
                    [action.server_id]: state.roles[action.server_id]
                        ? [...state.roles[action.server_id], action.role]
                        : [action.role],
                },
            };
        case SERVER_DELETE_ROLE_SUCCESS:
            if (state.roles[action.server_id] == null) {
                return state;
            }

            const roles = state.roles[action.server_id];
            for (let index = 0; index < roles.length; index++) {
                const element = roles[index];
                if (element.role_id === action.role_id) {
                    roles.splice(index, 1);
                    break;
                }
            }

            return {
                ...state,
                roles: {
                    ...state.roles,
                    [action.server_id]: roles,
                },
            };
        default:
            return state;
    }
};