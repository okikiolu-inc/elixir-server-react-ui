import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import AuthREST from '../api/AuthRest';

interface Message {
    username: string,
    userhash: string,
    content: string,
    avatar: string,
    updated: string,
    message_id: string,
}

export interface MessagesState {
    messages: {
        [channel_id: string]: Message[],
    }
    end: {
        [channel_id: string]: boolean,
    }
}

const MESSAGES_FETCH = 'MESSAGES_FETCH';
const MESSAGES_FETCH_SUCCESS = 'MESSAGES_FETCH_SUCCESS';
const MESSAGES_PREVIOUS_FETCH_SUCCESS = 'MESSAGES_PREVIOUS_FETCH_SUCCESS';
const MESSAGE_ADD = 'MESSAGE_ADD';
const MESSAGE_UPDATE = 'MESSAGE_UPDATE'
const MESSAGE_DELETE = 'MESSAGE_DELETE';


interface MessageFetchSuccess {
    type: typeof MESSAGES_FETCH_SUCCESS,
    channel_id: string,
    messages: Message[],
};

interface PreviousMessageFetchSuccess {
    type: typeof MESSAGES_PREVIOUS_FETCH_SUCCESS,
    channel_id: string,
    messages: Message[],
};


interface MessageAdd {
    type: typeof MESSAGE_ADD,
    channel_id: string,
    message: Message,
};

export type MessagesActionTypes = MessageFetchSuccess | MessageAdd | PreviousMessageFetchSuccess;

interface MessagesResponse {
    messages: Message[],
};

function messagesFetchSuccess(channel_id: string, messages: Message[]): MessagesActionTypes {
    return {
        type: MESSAGES_FETCH_SUCCESS,
        channel_id,
        messages,
    };
};

function previousMessagesFetchSuccess(channel_id: string, messages: Message[]): MessagesActionTypes {
    return {
        type: MESSAGES_PREVIOUS_FETCH_SUCCESS,
        channel_id,
        messages,
    };
};

export function addMessage(channel_id: string, message: Message): MessagesActionTypes {
    return {
        type: MESSAGE_ADD,
        channel_id,
        message,
    };
};

export function fetchMessages(channelid: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', `messages/${channelid}`)
        .then((response) => {
            return response.json() as Promise<MessagesResponse>;
        })
        .then((data) => {
            dispatch(messagesFetchSuccess(channelid, data.messages.reverse()));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};

export function fetchPreviousMessages(channelid: string, messageid: string): ThunkAction<any, any, any, Action> {
    return (dispatch) => {
        AuthREST('GET', `messages/${channelid}/${messageid}`)
        .then((response) => {
            return response.json() as Promise<MessagesResponse>;
        })
        .then((data) => {
            console.log(`channelid: ${channelid}, messageid: ${messageid}`);
            console.log('fetchPreviousMessages::: data.messages', data.messages);
            dispatch(previousMessagesFetchSuccess(channelid, data.messages.reverse()));
        })
        .catch((data) => {
            console.error('data', data);
        });
    };
};


const initialState: MessagesState = {
    messages: {},
    end: {},
};

export default function MessageReducer(
    state = initialState,
    action: MessagesActionTypes
): MessagesState {
    switch (action.type) {
        case MESSAGES_FETCH_SUCCESS:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.channel_id]: state.messages[action.channel_id] != null ?
                        state.messages[action.channel_id].concat(action.messages) :
                        action.messages,
                },
                end: {
                    ...state.end,
                    [action.channel_id]: action.messages.length < 25
                },
            };
        case MESSAGES_PREVIOUS_FETCH_SUCCESS:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.channel_id]: state.messages[action.channel_id] != null ?
                        action.messages.concat(state.messages[action.channel_id]) :
                        action.messages,
                },
                end: {
                    ...state.end,
                    [action.channel_id]: action.messages.length < 25
                },
            };
        case MESSAGE_ADD:
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.channel_id]: state.messages[action.channel_id] != null ?
                        [...state.messages[action.channel_id], action.message] :
                        [action.message],
                },
            };
        default:
            return state;
    }
};