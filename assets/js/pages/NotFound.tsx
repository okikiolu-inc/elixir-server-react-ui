import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

const NotFound: React.FC<RouteComponentProps> = () => (
    <div className="w-11/12 lg:w-19/20 bg-gray-500 flex" />
);

export default NotFound;