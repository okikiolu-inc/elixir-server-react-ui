import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RouteComponentProps, Redirect, useParams } from 'react-router-dom';
import { serverJoinSuccess, ServerSuccessMessage } from '../reducers/Servers';
import AuthREST from '../api/AuthRest';


const mapDispatch = {
    serverJoinSuccess,
}


const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & RouteComponentProps & {
}

const JoinServer: React.FC<Props> = ({
    serverJoinSuccess,
}) => {

    const { serverID, inviteID } = useParams();
    const [inflight, setInflight] = useState(true);
    const [success, setSuccess] = useState(false);

    if (serverID == null || serverID.trim() === ''
        || inviteID == null || inviteID.trim() === '') {
        return (
            <Redirect
                to={{
                    pathname: "/@home",
                }}
            />
        );
    }

    const data = {
        server: {
            server_id: serverID,
            invite_id: inviteID,
        },
    };

    useEffect(() => {
        AuthREST('POST', 'server-join', data)
        .then((response) => {
            if (response.status >= 400) {
                return {
                    error: 'error',
                } as ServerSuccessMessage;
            }

            return response.json() as Promise<ServerSuccessMessage>;
        })
        .then((data) => {
            if (data.error) {
                setInflight(false);
            }
            else {
                serverJoinSuccess(data.server);
                setSuccess(true);
                setInflight(false);
            }
        })
        .catch(() => {
            setInflight(false);
        });
    }, []);

    return (
        inflight ? (
            <div className="w-11/12 lg:w-19/20 bg-gray-700 loader" />
        ) : (
            success ? (
                <Redirect
                    to={{
                        pathname: `/@server/${serverID}/`,
                    }}
                />
            ) : (
                <Redirect
                    to = {{
                        pathname: "/@home",
                    }}
                />
            )
        )
    );
}

export default connector(JoinServer);