import React, { useState } from 'react';
import { connect, ConnectedProps } from 'react-redux'
import {
    RouteComponentProps,
    useParams,
    Redirect,
} from 'react-router-dom';

import { ServersState } from '../reducers/Servers';
import Main from '../components/server_settings/Main';
import Invites from '../components/server_settings/Invites';
import Roles from '../components/server_settings/Roles';
import Delete from '../components/server_settings/Delete';
import Channels from '../components/server_settings/Channels';

interface RootState {
    Servers: ServersState,
};

const mapState = (state: RootState) => ({
    owned_servers: state.Servers.owned_servers,
});

const connector = connect(mapState);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = RouteComponentProps & PropsFromRedux & {
};

const ServerSettings: React.FC<Props> = ({
    owned_servers,
}) => {

    const [page, setPage] = useState('');

    const { serverID } = useParams();

    const routes = [
        {
            name: 'Overview',
            path: ''
        },
        {
            name: 'Channels',
            path: 'channels'
        },
        {
            name: 'Invites',
            path: 'invites'
        },
        {
            name: 'Roles',
            path: 'roles'
        },
        {
            name: 'Delete Server',
            path: 'delete'
        },
    ]

    if (owned_servers[serverID] == null) {
        return (
            <Redirect
                to={{
                    pathname: "/@home",
                }}
            />
        )
    }

    return (
        <div className="w-11/12 lg:w-19/20 bg-gray-500 flex">
            <div className="w-1/4 lg:w-1/6 bg-gray-900 flex flex-col shadow-xl">
                {routes.map((link, idx) => {
                    const isSelected = page === link.path;
                    return (
                        <div
                            key={`settings-link-${idx}`}
                            className={`py-1 m-1 pl-1 hover:bg-gray-800 cursor-pointer rounded-md flex items-center select-none justify-between ${isSelected ? 'bg-gray-600' : ''} ${isSelected ? 'text-gray-200' : 'text-gray-500'}`}
                            onClick={() => {
                                setPage(`${link.path}`);
                            }}
                        >
                            <div className="flex items-center">
                                <span className=" text-lg">{link.name}</span>
                            </div>
                        </div>
                    );
                })}
            </div>
            <div className="w-3/4 lg:w-5/6 bg-gray-500 flex">
                {page === '' && <Main serverID={serverID} />}
                {page === 'invites' && <Invites serverID={serverID} />}
                {page === 'roles' && <Roles serverID={serverID} />}
                {page === 'delete' && <Delete serverID={serverID} />}
                {page === 'channels' && <Channels serverID={serverID} />}
            </div>
        </div>
    );
};

export default connector(ServerSettings);
