import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux'
import { RouteComponentProps, useParams } from 'react-router-dom';
import ServerContentNav from '../components/server/ServerContentNav';
import ServerMain from '../components/server/ServerMain';
import ServerMembers from '../components/server/ServerMembers';
import { addMessage } from '../reducers/Messages';
import { onChannelReceive, joinChannel, SocketMessageResponse } from '../socket';


const mapDispatch = {
    addMessage,
};

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = RouteComponentProps & PropsFromRedux & {
};

const Home: React.FC<Props> = ({
    addMessage,
}) => {

    const { serverID, channelID } = useParams();

    useEffect(() => {
        if (serverID != null && serverID.trim() !== ''
            && channelID != null && channelID.trim() !== '') {
            joinChannel(
                serverID,
                channelID,
                () => {
                    onChannelReceive(
                        serverID,
                        channelID,
                        'message',
                        (message: SocketMessageResponse) => {
                            addMessage(message.channel_id, {
                                username: message.username,
                                content: message.content,
                                avatar: message.avatar,
                                updated: message.now_time,
                                message_id: message.message_id,
                                userhash: message.userhash,
                            });
                        },
                    );
                },
                (payload) => {
                    console.log("Unable to join", payload);
                }
            );
        }
    }, [serverID, channelID])

    return (
        <div className="w-11/12 lg:w-19/20 bg-gray-500 flex">
            <ServerContentNav />
            <ServerMain/>
            <ServerMembers />
        </div>
    );
};

export default connector(Home);