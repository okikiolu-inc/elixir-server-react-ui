import React from 'react';
import { useLocation } from 'react-router-dom';
import Login from '../components/auth/Login';
import Register from '../components/auth/Register';


const Auth: React.FC = () => {
    const location = useLocation();
    const pathname = location.pathname;

    return (
        <>
            {pathname === '/login' && <Login />}
            {pathname === '/register' && <Register />}
        </>
    );
};

export default Auth;