import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import UserContentNav from '../components/user/UserContentNav';
import UserMain from '../components/user/UserMain';

const Home: React.FC<RouteComponentProps> = () => (
    <div className="w-11/12 lg:w-19/20 bg-gray-500 flex">
        <UserContentNav />
        <UserMain />
    </div>
);

export default Home;