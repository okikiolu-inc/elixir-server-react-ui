defmodule HeliosServer.ServerTest do
  use HeliosServer.DataCase

  alias HeliosServer.Schema

  describe "servers" do
    alias HeliosServer.Schema.Server

    @valid_attrs %{owner_id: 12345, icon: "some icon", banner: "some banner", name: "some name"}
    @update_attrs %{owner_id: 678910, icon: "some updated icon", banner: "some updated banner", name: "some updated name"}
    @invalid_attrs %{owner_id: nil, name: nil}

    def server_fixture(attrs \\ %{}) do
      # def create_server(%{"name" => name, "owner_id" => owner_id}=attrs \\ %{})
      {:ok, server} = Schema.create_server(
        %{
          "name" => attrs.name,
          "owner_id" => attrs.owner_id,
        }
      )
      server
    end

    test "get_server/1 returns the server with given id" do
      server = server_fixture()
      assert Schema.get_server(server.server_id) == server
    end

    test "create_server/1 with valid data creates a server" do
      assert {:ok, %Server{} = server} = Schema.create_server(@valid_attrs)
      assert server.icon == "some icon"
      assert server.name == "some name"
    end

    test "create_server/1 with invalid data returns error message" do
      assert {:error, msg} = Schema.create_server(@invalid_attrs)
    end

    test "update_server/2 with valid data updates the server" do
      server = server_fixture()
      assert {:ok, %Server{} = server} = Schema.update_server(server, @update_attrs)
      assert server.icon == "some updated icon"
      assert server.name == "some updated name"
    end

    test "update_server/2 with invalid data returns error changeset" do
      server = server_fixture()
      assert {:error, msg} = Schema.update_server(server, @invalid_attrs)
      assert server == Schema.get_server(server.server_id)
    end

    test "delete_server/1 deletes the server" do
      server = server_fixture()
      assert {:ok, :success} = Schema.delete_server(server)
      assert {:ok, nil} = Schema.get_server(server.server_id)
    end

    test "join_server/2 adds a user the a server member list" do
      assert true
    end

    test "leave_server/2 removes a user the a server member list" do
      assert true
    end

    test "get_server_channels/1 get all the channels in a server" do
      assert true
    end
  end
end
