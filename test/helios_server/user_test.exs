defmodule HeliosServer.UserTest do
  use HeliosServer.DataCase

  alias HeliosServer.Schema

  describe "users" do
    alias HeliosServer.Schema.User

    @valid_attrs %{avatar: "some avatar", email: "some email", password: "some password", userhash: "some userhash", username: "some username"}
    @update_attrs %{avatar: "some updated avatar", email: "some updated email", password: "some updated password", userhash: "some updated userhash", username: "some updated username"}
    @invalid_attrs %{avatar: nil, email: nil, password: nil, userhash: nil, username: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Schema.create_user()

      user
    end

    test "get_user/1 returns the user with given id" do
      user = user_fixture()
      assert Schema.get_user(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Schema.create_user(@valid_attrs)
      assert user.avatar == "some avatar"
      assert user.email == "some email"
      assert user.password == "some password"
      assert user.userhash == "some userhash"
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, msg} = Schema.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Schema.update_user(user, @update_attrs)
      assert user.avatar == "some updated avatar"
      assert user.email == "some updated email"
      assert user.password == "some updated password"
      assert user.userhash == "some updated userhash"
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, msg} = Schema.update_user(user, @invalid_attrs)
      assert user == Schema.get_user(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Schema.delete_user(user)
      assert {:ok, nil} = Schema.get_user(user.id)
    end

    test "get_user_servers/1 gets all the servers a user belongs to" do
      assert true
    end

    test "get_by_email/1 gets user by their email address" do
      assert true
    end

    test "get_by_username_and_userhash/2 gets user by their usename and userhash" do
      assert true
    end
  end
end
