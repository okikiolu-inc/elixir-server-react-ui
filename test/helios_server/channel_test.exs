defmodule HeliosServer.ServerTest do
  use HeliosServer.DataCase

  alias HeliosServer.Schema

  describe "channels" do
    alias HeliosServer.Schema.Channel

    @valid_attrs %{channel_id: 12345, name: "some name"}
    @update_attrs %{channel_id: 12345, name: "some updated name"}
    @invalid_attrs %{channel_id: 12345, name: nil}

    def channel_fixture(attrs \\ %{}) do
      {:ok, channel} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Schema.create_channel()

      channel
    end

    test "get_channel/1 returns the channel with given id" do
      channel = channel_fixture()
      assert Schema.get_channel(channel.channel_id) == channel
    end

    test "create_channel/1 with valid data creates a channel" do
      assert {:ok, %Server{} = channel} = Schema.create_channel(@valid_attrs)
      assert channel.icon == "some icon"
      assert channel.name == "some name"
    end

    test "create_channel/1 with invalid data returns error message" do
      assert {:error, msg} = Schema.create_channel(@invalid_attrs)
    end

    test "update_channel/2 with valid data updates the channel" do
      channel = channel_fixture()
      assert {:ok, %Server{} = channel} = Schema.update_channel(channel, @update_attrs)
      assert channel.icon == "some updated icon"
      assert channel.name == "some updated name"
    end

    test "update_channel/2 with invalid data returns error changeset" do
      channel = channel_fixture()
      assert {:error, msg} = Schema.update_channel(channel, @invalid_attrs)
      assert channel == Schema.get_channel(channel.channel_id)
    end

    test "delete_channel/1 deletes the channel" do
      channel = channel_fixture()
      assert {:ok, :success} = Schema.delete_channel(channel)
      assert {:ok, nil} = Schema.get_channel(channel.channel_id)
    end
  end
end
