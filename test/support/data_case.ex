defmodule HeliosServer.DataCase do
  @moduledoc """
  This module defines the setup for tests requiring
  access to the application's data layer.

  You may define functions here to be used as helpers in
  your tests.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use HeliosServer.DataCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      alias HeliosServer.Repo

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import HeliosServer.DataCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(HeliosServer.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(HeliosServer.Repo, {:shared, self()})
    end

    :ok
  end

  @doc """
  A helper that transforms changeset errors into a map of messages.

      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)

  """
  def errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Regex.replace(~r"%{(\w+)}", message, fn _, key ->
        opts |> Keyword.get(String.to_existing_atom(key), key) |> to_string()
      end)
    end)
  end
end

# Pre populate Cassandra
# defmodule PrepopulateModule do
#   use Triton.Setup
#   import Triton.Query
#   require Schema.User
#   alias Schema.User

#   # create an admin user if it doesn't exist

#   setup do
#     User
#     |> insert(
#       user_id: @admin_user_id,
#       username: @admin_user_username,
#       display_name: @admin_user_display_name,
#       password: Bcrypt.hashpwsalt(@admin_user_password),
#       email: @admin_user_email,
#       created: @admin_user_created
#     ) |> if_not_exists
#   end
# end